//
//  UIImageResizing.m
//  JobsHereApp
//
//  Created by Apple on 29/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import "UIImageResizing.h"

@interface UIImageResizing ()

@end

@implementation UIImageResizing

@end
@implementation UIImage (Resize)

- (UIImage*)scaleToSize:(CGSize)size {
    UIGraphicsBeginImageContext(size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0.0, size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    CGContextDrawImage(context, CGRectMake(0.0f, 0.0f, size.width, size.height), self.CGImage);
    
    UIImage* scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return scaledImage;
}

@end



