//
//  UIImageResizing.h
//  JobsHereApp
//
//  Created by Apple on 29/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageResizing : UIImage

// Put this in UIImageResizing.h
@end

@interface UIImage (Resize)
- (UIImage*)scaleToSize:(CGSize)size;
@end


