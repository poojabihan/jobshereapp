#import "CardView.h"

@implementation CardView

- (void)layoutSubviews
{
	[super layoutSubviews];

	float cornerRadius = 5;
	int shadowOffsetWidth = 0;
	int shadowOffsetHeight = 3;
	float shadowOpacity = 0.5;
	UIColor* shadowColor = [UIColor blackColor];

	UIBezierPath* shadowPath = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:cornerRadius];

	self.backgroundColor = [UIColor whiteColor]; // TODO change color
	self.layer.cornerRadius = cornerRadius;
	self.layer.masksToBounds = false;
	self.layer.shadowColor = shadowColor.CGColor;
	self.layer.shadowOffset = CGSizeMake(shadowOffsetWidth, shadowOffsetHeight);
	self.layer.shadowOpacity = shadowOpacity;
	self.layer.shadowPath = shadowPath.CGPath;
}

@end
