//
//  JobActionPopUpViewController.h
//  JobsHereApp
//
//  Created by Apple on 28/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobActionPopUpViewController : UIViewController

@property (strong, nonatomic) NSString *strLocation;
@property (strong, nonatomic) NSString *latJobView;
@property (strong, nonatomic) NSString *longiJobView;
@property (strong, nonatomic) UINavigationController *nav;
@property (weak, nonatomic) IBOutlet UILabel *lblLocation;


@end
