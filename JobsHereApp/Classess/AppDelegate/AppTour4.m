//
//  AppTour4.m
//  JobsHereApp
//
//  Created by Apple on 25/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import "AppTour4.h"

@interface AppTour4 ()

@end

@implementation AppTour4

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.btnNext.layer.borderColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0].CGColor;
    self.btnNext.layer.borderWidth = 2;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
