//
//  AppDelegate.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)


#import "AppDelegate.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ViewController.h"
#import "TabBarViewController.h"
@import Firebase;
@import FirebaseMessaging;
#import "JCNotificationCenter.h"
#import "JCNotificationBannerPresenterSmokeStyle.h"
#import <AudioToolbox/AudioToolbox.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <linkedin-sdk/LISDK.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <Stripe/Stripe.h>
#import "AppTourViewController.h"
#import "VerifyAccountStep1ViewController.h"
@interface AppDelegate ()
{
    NSString * refreshedToken;
}
@end

@implementation AppDelegate
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([LISDKCallbackHandler shouldHandleUrl:url]) {
        return [LISDKCallbackHandler application:application openURL:url sourceApplication:sourceApplication annotation:annotation] || [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url                                                                                                                                                                           sourceApplication:sourceApplication                                                                                                                                                                                   annotation:annotation];
    }
        return [[FBSDKApplicationDelegate sharedInstance] application:application openURL:url                                                                                                                                                                           sourceApplication:sourceApplication                                                                                                                                                                                   annotation:annotation];

    return YES;
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
            options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                               annotation:options[UIApplicationOpenURLOptionsAnnotationKey]
                    ] || [LISDKCallbackHandler application:application openURL:url sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey] annotation:options[UIApplicationOpenURLOptionsAnnotationKey]];
    // Add any custom logic here.
    return handled;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     [GMSServices provideAPIKey:@"AIzaSyBUuMuzCUSW6N8CP5UkvijQgHODMLLqRjo"];
    
//    [Stripe setDefaultPublishableKey:@"pk_test_DInIGtLxTjj3lGi8v7LlnByV"];
    [Stripe setDefaultPublishableKey:@"pk_live_FRUToR3tRKPA8CxKoePsNmo3"];

    [self customappearance];
    NSString *LoginCheck = [[NSUserDefaults standardUserDefaults] valueForKey:@"Login"];
   
    UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//    AppTourViewController *login = (AppTourViewController*)[storyboard instantiateViewControllerWithIdentifier:@"AppTourViewController"];
//    UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
//    self.window.rootViewController=nav;
//    
//    return true;
    
    if ([LoginCheck isEqualToString:@"LoginSuccess"])
    {
        //home
        TabBarViewController *vc=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
        self.window.rootViewController=vc;
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"showAppTour"] == nil) {
            [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"showAppTour"];
            
            AppTourViewController *login = (AppTourViewController*)[storyboard instantiateViewControllerWithIdentifier:@"AppTourViewController"];
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
            self.window.rootViewController=nav;

        }
        else {
        //login
            ViewController *login = (ViewController*)[storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            UINavigationController *nav=[[UINavigationController alloc]initWithRootViewController:login];
            self.window.rootViewController=nav;
        }
    }
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(tokenRefreshCallBack:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    center.delegate = self;
    
    
    // [[UNUserNotificationCenter currentNotificationCenter] setDelegate:self];
    
    [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * error)
     {
         if (granted==NO) {
             NSLog(@"Didnt allowed");
             UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:@"Notification Services Disabled!"  message:@"Please open this app's settings enable Notifications to get important Updates!"  preferredStyle:UIAlertControllerStyleAlert];
             [alertController addAction:[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action)
                                         {
                                             @try
                                             {
                                                 NSLog(@"tapped Settings");
                                                 BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                                                 if (canOpenSettings)
                                                 {
                                                     NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                                                     
                                                     
                                                     [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                                                 }
                                             }
                                             @catch (NSException *exception)
                                             {
                                                 
                                             }
                                         }]];
             [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil]];
             UINavigationController *nvc = (UINavigationController *)[[application windows] objectAtIndex:0].rootViewController;
             
             UIViewController *vc;
             if ([nvc isKindOfClass:[TabBarViewController class]]) {
                 
                 UITabBarController *tb = (UITabBarController*)nvc;
                 vc = tb.selectedViewController;
             }
             else{
                 vc = nvc.visibleViewController;
             }
             
             alertController.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

//             UIViewController *vc = nvc.visibleViewController;
             dispatch_async(dispatch_get_main_queue(), ^{
                 [vc presentViewController:alertController animated:YES completion:nil];
             });
             
         }
         if(error)
         {
             NSLog( @"Push registration FAILED" );
             NSLog( @"ERROR: %@ - %@", error.localizedFailureReason, error.localizedDescription );
             NSLog( @"SUGGESTIONS: %@ - %@", error.localizedRecoveryOptions, error.localizedRecoverySuggestion );
         }
         else
         {
             dispatch_async(dispatch_get_main_queue(), ^{
                 
                 [[UIApplication sharedApplication] registerForRemoteNotifications];
             });
             // required to get the app to do anything at all about push notifications
             NSLog( @"Push registration success." );
         }
     }];
    
    [FIRApp configure];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:) name:kFIRInstanceIDTokenRefreshNotification object:nil];
    

    return [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    return YES;
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber=0)];
    //    UILocalNotification *notification=[[UILocalNotification alloc]init];
    //    notification.applicationIconBadgeNumber=-1;
    //    [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [[FIRMessaging messaging]setShouldEstablishDirectChannel:NO];
    NSLog(@"Disconnected From FCM");
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    UNMutableNotificationContent *content = [[UNMutableNotificationContent alloc] init];
    content.badge = [NSNumber numberWithInteger:([UIApplication sharedApplication].applicationIconBadgeNumber=0)];
    //UILocalNotification *notification=[[UILocalNotification alloc]init];
    //notification.applicationIconBadgeNumber=-1;
    //[[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    [self connectToFirebase];
    [FBSDKAppEvents activateApp];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error{
    
    NSLog(@"Failed to get token, error: %@", error.localizedDescription);
    
}


- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult result))handler {
    
    /*{
     "" = "";
     aps =     {
     alert =         {
     body = "Your email has been successfully verified.";
     title = "Email Verify";
     };
     badge = 1;
     sound = default;
     };
     badge = 1;
     "gcm.message_id" = "0:1548746762333095%0aab060b0aab060b";
     "gcm.notification." = "";
     "gcm.notification.NotificationView" = VerifyEmail;
     "google.c.a.e" = 1;
     message = "Email Verify";
     sound = default;
     text = "Your email has been successfully verified.";
     viewName = VerifyEmail;
     }*/
    
    NSLog(@"Message Id: %@",userInfo[@"gcm.message_id"]);
    NSLog(@"Message %@",userInfo);
    NSLog(@"Notification Delivered");
    
    if ([userInfo[@"viewName"] isEqualToString: @"VerifyEmail"]){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifiedEmailPushRecived"     object:self userInfo:userInfo];
    }
    // iOS 10 will handle notifications through other methods
    
    if( SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO( @"10.0" ) )
    {
        NSLog( @"iOS version >= 10. Let NotificationCenter handle this one." );
        
        // set a member variable to tell the new delegate that this is background
        return;
    }
    NSLog( @"HANDLE PUSH, didReceiveRemoteNotification: %@", userInfo );
    
    
}

-(void)playNotificationSound{
    //play sound
    SystemSoundID    pewPewSound;
    NSString *pewPewPath = [[NSBundle mainBundle]
                            pathForResource:@"sms_alert_aurora" ofType:@"caf"];
    NSURL *pewPewURL = [NSURL fileURLWithPath:pewPewPath];
    AudioServicesCreateSystemSoundID((__bridge CFURLRef)pewPewURL, & pewPewSound);
    AudioServicesPlaySystemSound(pewPewSound);
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
}
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
{
    NSLog( @"Handle push from foreground" );
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
    // custom code to handle push while app is in the foreground
    
    NSLog(@"%@", notification.request.content.userInfo);
    
    if ([notification.request.content.userInfo[@"viewName"] isEqualToString: @"VerifyEmail"]){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifiedEmailPushRecived"     object:self userInfo:nil];
    }
    
}


- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^_Nonnull __strong)(void))completionHandler
{
    
    NSLog(@"%@", response.notification.request.content.userInfo);
    
    if ([response.notification.request.content.userInfo[@"viewName"] isEqualToString: @"VerifyEmail"]){
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"VerifiedEmailPushRecived"     object:self userInfo:nil];
    }
}
#pragma mark : Firebase Delegate

-(void)tokenRefreshCallBack:(NSNotification *)notification{
    refreshedToken=[[FIRInstanceID instanceID]token];
    NSLog(@"InstanceId Token %@",refreshedToken);
    //Connect to FCM
    [self connectToFirebase];
    
}

-(void)connectToFirebase{
    
    NSError *error;
    
    [[FIRMessaging messaging]setShouldEstablishDirectChannel:YES];
    
    if (error!=nil) {
        
        NSLog(@"Unable to Connect to FCM %@",error.localizedDescription);
        
    }else{
        
        NSLog(@"Connected to FCM");
        
    };
}
- (void)tokenRefreshNotification:(NSNotification *)notification {
    NSLog(@"instanceId_notification=>%@",[notification object]);
    NSString * instanceID = [NSString stringWithFormat:@"%@",[notification object]];
    NSLog(@"String %@",instanceID);
    [self connectToFirebase];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"JobsHereApp"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                    */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}

#pragma mark - back button on navigation
-(void)customappearance
{
    //int imageSize = 20;
    UIImage *myImage = [UIImage imageNamed:@"backIcon"]; //set your backbutton imagename
    UIImage *backButtonImage = [myImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    // now use the new backButtomImage
    [[UINavigationBar appearance] setBackIndicatorImage:backButtonImage];
    [[UINavigationBar appearance] setBackIndicatorTransitionMaskImage:backButtonImage];
    
    [[UIBarButtonItem appearance] setBackButtonTitlePositionAdjustment:UIOffsetMake(-400.f, 0)
                                            forBarMetrics:UIBarMetricsDefault];
    
}

@end
