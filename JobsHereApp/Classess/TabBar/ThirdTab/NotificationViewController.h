//
//  NotificationViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tblNotifications;

@end
