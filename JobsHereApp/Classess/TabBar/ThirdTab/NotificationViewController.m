//
//  NotificationViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "NotificationViewController.h"
#import "Webservice.h"
//#import <MBProgressHUD.h>
#import "ChatListTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "RequestStatusView.h"
#import "JobsDetailsViewController.h"
#import "DGActivityIndicatorView.h"


@interface NotificationViewController (){
   // MBProgressHUD * HUD;
    NSMutableArray *arrNotifications;
    DGActivityIndicatorView *loader;

}

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.tblNotifications.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    [self callWebServiceForNotifications];
}

#pragma mark  WebService Method
-(void) callWebServiceForNotifications {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];

    NSString *str = [NSString stringWithFormat:@"%@api/getAllNotification",KBaseUrl];
    NSDictionary *dictParam = @{@"userId":loginUserID};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
      //  [HUD hideAnimated:YES];
        [loader stopAnimating];
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            arrNotifications = [response valueForKey:@"response"];
            [self.tblNotifications reloadData];

        }
    } failure:^(NSError *error) {
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
    }];
}

-(void) callWebServiceForGetJobByID:(NSString *)jobID {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    NSString *str = [NSString stringWithFormat:@"%@api/getJobById",KBaseUrl];
    NSDictionary *dictParam = @{@"jobId":jobID};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
      //  [HUD hideAnimated:YES];
        [loader stopAnimating];
        NSLog(@"response %@",response);
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            JobsDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
            JobVC.JobData = [response objectForKey:@"response"];
            [self.navigationController pushViewController:JobVC animated:YES];
        }
        else{
            [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        }
        
    } failure:^(NSError *error) {
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
    }];
}

-(void) callWebServiceForJobDetails:(NSString *)jobID {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];
    
    NSString *str = [NSString stringWithFormat:@"%@api/getUserJobDetails",KBaseUrl];
    NSDictionary *dictParam = @{@"userId":loginUserID,@"jobId":jobID};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
       // [HUD hideAnimated:YES];
        [loader stopAnimating];
        NSLog(@"response %@",response);
        if ([[response objectForKey:@"status"] boolValue] == 1) {

            RequestStatusView *requestStatusView = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestStatusView"];
            requestStatusView.strUserName = [NSString stringWithFormat:@"%@ %@",[response valueForKeyPath:@"response.fname"],[response valueForKeyPath:@"response.lname"]];
            requestStatusView.strEmail = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.email"]];
            requestStatusView.strAddress = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.address"]];
            requestStatusView.strRequestStatus = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.status"]];
            requestStatusView.strImagerData = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.profileImage"]];
            requestStatusView.strApplicantId = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.id"]];
            requestStatusView.strJobId = jobID;
            requestStatusView.strVerified = [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.isVerified"]];
            requestStatusView.nav = self.navigationController;
            requestStatusView.providesPresentationContextTransitionStyle = YES;
            requestStatusView.definesPresentationContext = YES;
            [requestStatusView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self.tabBarController presentViewController:requestStatusView animated:NO completion:nil];
        }
        
        
    } failure:^(NSError *error) {
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
    }];
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return arrNotifications.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    ChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatListTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    NSString *strPosted =  [[arrNotifications valueForKey:@"jobTitle"]objectAtIndex:indexPath.row];
    
    NSLog(@"%@", strPosted);
    
    cell.ProfileImage.layer.cornerRadius = 25;
    cell.ProfileImage.layer.masksToBounds=YES;
    cell.backgroundColor=[UIColor clearColor];
    
    
    cell.UserNameLabel.text = [NSString stringWithFormat:@"%@",[[arrNotifications valueForKey:@"title"]objectAtIndex:indexPath.row]];
    
    cell.LastMessageLabel.text = [NSString stringWithFormat:@"%@",[[arrNotifications valueForKey:@"notification"]objectAtIndex:indexPath.row]];
    
    [cell.ProfileImage setImage:[UIImage imageNamed:@"man"]];
    
    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ([[[arrNotifications objectAtIndex:indexPath.row] valueForKey:@"view"] isEqualToString:@"profile"]) {
        //getProfileDetail
        [self callWebServiceForJobDetails:[[arrNotifications objectAtIndex:indexPath.row] valueForKey:@"jobId"]];
    }
    if ([[[arrNotifications objectAtIndex:indexPath.row] valueForKey:@"view"] isEqualToString:@"jobDetails"]) {
        //jobdetail
        [self callWebServiceForGetJobByID:[[arrNotifications objectAtIndex:indexPath.row] valueForKey:@"jobId"]];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
