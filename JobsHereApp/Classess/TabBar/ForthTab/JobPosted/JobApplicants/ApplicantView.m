//
//  ApplicantView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "ApplicantView.h"
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "ApplicantTableViewCell.h"
#import "UIImageView+WebCache.h"
//#import <MBProgressHUD.h>
#import "RequestStatusView.h"
#import "DGActivityIndicatorView.h"


@interface ApplicantView ()
{
    NSMutableArray *ApplicantArrayDetails;
    //MBProgressHUD * HUD;
    NSString *strMarkCompleteStatus;
    DGActivityIndicatorView *loader;

}

@property (strong, nonatomic) IBOutlet UITableView *applicantJobsTableView;

@end

@implementation ApplicantView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.applicantJobsTableView setSeparatorColor:[UIColor colorWithHex:@"00b050"]];
    self.applicantJobsTableView.tableFooterView = [[UIView alloc] init];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.title = @"Applicants";
    [self callingWebServiceForGetApplicantsList];
    [self callingWebServiceForGetStatus];
}

- (IBAction)btnRefundPaymentAction:(id)sender {
    [self callingWebServiceForRefundPayment];
}

#pragma mark  Calling Api
-(void)callingWebServiceForRefundPayment{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSString *UserID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];

        NSDictionary *dictParam = @{@"userId":UserID, @"jobId":_strJobId, @"status":@"success"};
        
        NSString *submitRefund=[NSString stringWithFormat:@"%@api/requestRefundAmount",KBaseUrl];
        
        [Webservice requestPostUrl:submitRefund parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ApplicantArrayDetails = [response valueForKey:@"response"];
                
             //   [HUD hideAnimated:YES];
                [loader stopAnimating];
                [_applicantJobsTableView reloadData];
                
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                NSLog(@"response is null");
            }else{
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                
                
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                               //[HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                               
                               NSLog(@"Error %@",error);
                           }];
    }else{
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)callingWebServiceForGetApplicantsList{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSDictionary *dictParam = @{@"jobId":_strJobId};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getAllApplicant",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ApplicantArrayDetails = [response valueForKey:@"response"];
                
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

                [_applicantJobsTableView reloadData];
                
                
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

                NSLog(@"response is null");
            }else{
              //  [HUD hideAnimated:YES];
                [loader stopAnimating];

                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                              // [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)callingWebServiceForGetStatus{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSDictionary *dictParam = @{@"jobId":_strJobId};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getStatus",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                strMarkCompleteStatus = [response valueForKeyPath:@"result.job"];
                switch ([[response valueForKeyPath:@"result.refund"] integerValue]) {
                    case 0:
                        [self.btnRefund setTitle:@"Refund Payment" forState:UIControlStateNormal];
                        break;
                    case 1:
                        [self.btnRefund setTitle:@"Refund Requested" forState:UIControlStateNormal];
                        self.btnRefund.enabled = NO;
                        [self.btnRefund setUserInteractionEnabled:NO];
                        break;
                    case 2:
                        [self.btnRefund setTitle:@"Refunded" forState:UIControlStateNormal];
                        self.btnRefund.enabled = NO;
                        [self.btnRefund setUserInteractionEnabled:NO];
                        break;
                    default:
                        break;
                }
                
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

                NSLog(@"response is null");
            }else{
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

                
                
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                            //   [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                               
                               NSLog(@"Error %@",error);
                           }];
    }else{
                       [loader stopAnimating];
             //[HUD hideAnimated:YES];
        
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    return ApplicantArrayDetails.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    ApplicantTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ApplicantTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    cell.imgProfile.layer.cornerRadius = 20;
    cell.imgProfile.layer.masksToBounds=YES;
    
    cell.imgStatusIndication.layer.cornerRadius = 6;
    cell.imgStatusIndication.layer.masksToBounds=YES;
    cell.backgroundColor=[UIColor clearColor];
    
    
    cell.lblTitle.text = [NSString stringWithFormat:@"%@ %@",[[ApplicantArrayDetails valueForKey:@"fname"]objectAtIndex:indexPath.row],[[ApplicantArrayDetails valueForKey:@"lname"]objectAtIndex:indexPath.row]];
    
    cell.lblStatusTitle.text = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"status"]objectAtIndex:indexPath.row]];
    
    [cell.imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,[[ApplicantArrayDetails valueForKey:@"profileImage"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];
    
    if ([[NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"status"]objectAtIndex:indexPath.row]] isEqualToString:@"Pending"]) {
        
        [cell.imgStatusIndication setBackgroundColor:[UIColor colorWithHex:@"#f1a409"]];
    }
    else if ([[NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"status"]objectAtIndex:indexPath.row]] isEqualToString:@"Accepted"]) {
        
        [cell.imgStatusIndication setBackgroundColor:[UIColor colorWithHex:@"#029c0a"]];
    }
    else {
        [cell.imgStatusIndication setBackgroundColor:[UIColor colorWithHex:@"#ff060e"]];
        
    }
    
    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSString  *strJobId=[NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"id"]objectAtIndex:indexPath.row]];
    
    NSLog(@"Selected job Id is here %@",strJobId);
    
    RequestStatusView *requestStatusView = [self.storyboard instantiateViewControllerWithIdentifier:@"RequestStatusView"];
    
    requestStatusView.strMarkCompleteStatus = strMarkCompleteStatus;
    requestStatusView.strUserName = [NSString stringWithFormat:@"%@ %@",[[ApplicantArrayDetails valueForKey:@"fname"]objectAtIndex:indexPath.row],[[ApplicantArrayDetails valueForKey:@"lname"]objectAtIndex:indexPath.row]];
    
    requestStatusView.strEmail = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"email"]objectAtIndex:indexPath.row]];
    
    requestStatusView.strAddress = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"address"]objectAtIndex:indexPath.row]];
    
    requestStatusView.strRequestStatus = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"status"]objectAtIndex:indexPath.row]];
    
    requestStatusView.strImagerData = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"profileImage"]objectAtIndex:indexPath.row]];
    
    requestStatusView.strApplicantId = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"applicantId"]objectAtIndex:indexPath.row]];
    requestStatusView.nav = self.navigationController;
    requestStatusView.strVerified = [NSString stringWithFormat:@"%@",[[ApplicantArrayDetails valueForKey:@"isVerified"]objectAtIndex:indexPath.row]];

    requestStatusView.strJobId = _strJobId;
    requestStatusView.objApplicantView = self;
    
    requestStatusView.providesPresentationContextTransitionStyle = YES;
    requestStatusView.definesPresentationContext = YES;
    [requestStatusView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.tabBarController presentViewController:requestStatusView animated:NO completion:nil];
    
    
}



@end
