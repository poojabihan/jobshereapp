//
//  ApplicantView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplicantView : UIViewController

@property (strong, nonatomic) NSString *strJobId;
@property (strong, nonatomic) IBOutlet UIButton *btnRefund;

-(void)callingWebServiceForGetApplicantsList;
@end
