//
//  ApplicantTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ApplicantTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong, nonatomic) IBOutlet UILabel *lblStatusTitle;
@property (strong, nonatomic) IBOutlet UIImageView *imgStatusIndication;


@end
