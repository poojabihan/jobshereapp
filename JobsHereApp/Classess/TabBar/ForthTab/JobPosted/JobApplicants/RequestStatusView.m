//
//  RequestStatusView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "RequestStatusView.h"
#import "UIColor+Hexadecimal.h"
#import "UIImageView+WebCache.h"
#import "Reachability.h"
#import "Webservice.h"
#import "ApplicantTableViewCell.h"
#import "UIImageView+WebCache.h"
//#import <MBProgressHUD.h>
#import "MessageChatViewController.h"
#import "DGActivityIndicatorView.h"

@interface RequestStatusView ()
{
    NSMutableArray *ApplicantArrayDetails;
    //MBProgressHUD * HUD;
    DGActivityIndicatorView * loader;
    NSString *sendBtnClickTitle;
}
@property (strong, nonatomic) IBOutlet UIView *StatusView;

@end

@implementation RequestStatusView

- (void)viewDidLoad {
    [super viewDidLoad];
   
    if ([_strVerified isEqualToString:@"Yes"]) {
        _imgVerified.hidden = NO;
    }
    else{
        _imgVerified.hidden = YES;
    }
    
    /*Job Status
     0 -> Not completed, job is in progress
     1 -> Completed by user
     2 -> Payment initiated by admin
     
     Refund Status
     
     0 -> no refund request, user can request for refund
     1 -> refund request initiated by user
     2 -> refund released by admin*/
    
    switch ([_strMarkCompleteStatus integerValue]) {
        case 0:
            [self.btnMarkComplete setTitle:@"Mark Complete" forState:UIControlStateNormal];
            break;
        case 1:
            [self.btnMarkComplete setTitle:@"Job Completed" forState:UIControlStateNormal];
            self.btnMarkComplete.enabled = NO;
            [self.btnMarkComplete setUserInteractionEnabled:NO];
            break;
        case 2:
            [self.btnMarkComplete setTitle:@"Payment Released" forState:UIControlStateNormal];
            self.btnMarkComplete.enabled = NO;
            [self.btnMarkComplete setUserInteractionEnabled:NO];
            break;
        default:
            break;
    }
    
    
    _lblUserName.text = _strUserName;
    _lblName.text = _strUserName;
    _lblEmail.text = _strEmail;
    if([_strAddress isEqualToString:@""]){
        
        _lblAddress.hidden = YES;
        _imgAddress.hidden = YES;
        _topConstraintOfAcceptDeclineView.constant = -40;
        
        _heightConstraintOfStatusView.constant = _StatusView.frame.size.height - 110;
        
    }
    else{
    _lblAddress.text = _strAddress;
    }
    _imgProfile.layer.cornerRadius = _imgProfile.frame.size.height / 2;
    _imgProfile.layer.masksToBounds=YES;
    [_imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,_strImagerData]]placeholderImage:[UIImage imageNamed:@"man"]];
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    
    _StatusView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _StatusView.layer.shadowOffset = CGSizeMake(0, 0);
    _StatusView.layer.shadowOpacity = 1;
    _StatusView.layer.shadowRadius = 1.0;
    _StatusView.layer.masksToBounds = NO;
    
    _StatusView.layer.borderWidth = 1;
    _StatusView.layer.borderColor = [UIColor grayColor].CGColor;
    
    _btnAccept.layer.cornerRadius = _btnAccept.frame.size.height / 2;
    _btnAccept.layer.masksToBounds=YES;
    
    _btnDecline.layer.cornerRadius = _btnDecline.frame.size.height / 2;
    _btnDecline.layer.masksToBounds=YES;
    
    _btnDecline.layer.borderWidth = 2;
    _btnDecline.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;

    
    if([_strRequestStatus isEqualToString:@"Pending"]){
        
        _btnAccept.hidden = NO;
        _btnDecline.hidden = NO;
        _btnMarkComplete.hidden = YES;
      

    }
    else if([_strRequestStatus isEqualToString:@"Accepted"]){
        
        _btnAccept.hidden = YES;
        _btnDecline.hidden = NO;
        _btnMarkComplete.hidden = NO;

    
    _horizontalSpacingBetweenButtons.constant = -120 ;
    _WidthRelatedConstraintOfDeclineBtn.constant = 280;
    
    }
    
    else{
        
        _btnAccept.hidden = NO;
        _btnDecline.hidden = YES;
        _btnMarkComplete.hidden = YES;

        
        
 _WidthRelatedConstraintOfAcceotBtn.constant = 280;


    }
    // Do any additional setup after loading the view.
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimissView)];
    [self.bgView addGestureRecognizer:tap];
}
-(void)viewWillAppear:(BOOL)animated{
    
    sendBtnClickTitle = @"";

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dimissView {
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)btnChatAction:(id)sender {
    
    MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
    ChatVC.UserID = _strApplicantId;
    ChatVC.JobID = _strJobId;
    ChatVC.NavigationTitle = _strUserName;
    [_nav pushViewController:ChatVC animated:YES];
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}


- (IBAction)btnAcceptAction:(id)sender {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    sendBtnClickTitle = @"Accepted";
    
    [self callingWebServiceForGetApplicantsList ];
}
- (IBAction)btnDeclineAction:(id)sender {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    sendBtnClickTitle = @"Rejected";

    [self callingWebServiceForGetApplicantsList ];

}
- (IBAction)btnMarkCompleteAction:(id)sender {
    
    [self callWebServiceMarkComplete];
}


-(void)callWebServiceMarkComplete {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
//        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        HUD.bezelView.color = [UIColor blackColor];
//        HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//        HUD.contentColor = [UIColor whiteColor];
        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"];
        NSDictionary *dictParam = @{@"userId":UserId,@"jobId":_strJobId};
        NSLog(@"Dic Param - %@",dictParam);
        
        NSString *JobsPost=[NSString stringWithFormat:@"%@api/completeJob",KBaseUrl];
        
        [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
            
            if (([[response objectForKey:@"status"] boolValue] == 1)) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES];
                    [loader stopAnimating];
                    
                });
                
                UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:[response valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault  handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissViewControllerAnimated:NO completion:nil];
                }];
                [alertCont addAction:okAction];
                alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertCont animated:true completion:nil];
                });
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                  //  [HUD hideAnimated:YES];
                    [loader stopAnimating];

                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[HUD hideAnimated:YES];
                    [loader stopAnimating];

                });
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

            });
            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)callingWebServiceForGetApplicantsList{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSDictionary *dictParam = @{@"applicantId":_strApplicantId,@"jobId" :_strJobId,@"status": sendBtnClickTitle };
        
        NSLog(@"%@", dictParam);
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/changeJobStatus",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            //[HUD hideAnimated:YES];
            [loader stopAnimating];

            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ApplicantArrayDetails = [response valueForKey:@"response"];
                
                [self.objApplicantView callingWebServiceForGetApplicantsList];
                 [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
                [self dismissViewControllerAnimated:NO completion:nil];

                
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
            }else{
                
                
                
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                             //  [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}



@end
