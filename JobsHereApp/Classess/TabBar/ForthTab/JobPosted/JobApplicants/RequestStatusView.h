//
//  RequestStatusView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ApplicantView.h"

@interface RequestStatusView : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnMarkComplete;

@property (strong, nonatomic) NSString *strUserName;
@property (strong, nonatomic) NSString *strMarkCompleteStatus;

@property (strong, nonatomic) NSString *strEmail;
@property (strong, nonatomic) ApplicantView *objApplicantView;

@property (strong, nonatomic) NSString *strApplicantId;
@property (strong, nonatomic) NSString *strAddress;
@property (strong, nonatomic) NSString *strJobId;
@property (strong, nonatomic) NSString *strVerified;

@property (strong, nonatomic) NSString *strRequestStatus;

@property (strong, nonatomic) NSString *strImagerData;

@property (strong, nonatomic) IBOutlet UIImageView *imgAddress;
@property (strong, nonatomic) IBOutlet UIImageView *imgVerified;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *topConstraintOfAcceptDeclineView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintOfStatusView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *WidthRelatedConstraintOfAcceotBtn;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *WidthRelatedConstraintOfDeclineBtn;

@property (strong, nonatomic) IBOutlet UILabel *lblName;

@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;
@property (strong, nonatomic) IBOutlet UILabel *lblEmail;
@property (strong, nonatomic) IBOutlet UILabel *lblAddress;
@property (strong, nonatomic) IBOutlet UIButton *btnAccept;
@property (strong, nonatomic) IBOutlet UIButton *btnDecline;

@property (strong, nonatomic) IBOutlet UIView *acceptDeclineView;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) UINavigationController *nav;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *horizontalSpacingBetweenButtons;

@end
