//
//  JobPostedView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobPostedView.h"
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "JobPostedTableViewCell.h"
#import "UIImageView+WebCache.h"
//#import <MBProgressHUD.h>
#import "ApplicantView.h"
#import "DGActivityIndicatorView.h"


@interface JobPostedView ()
{
    NSMutableArray *PostedJobArrayDetails;
    DGActivityIndicatorView *loader;

     // MBProgressHUD * HUD;
}
@property (strong, nonatomic) IBOutlet UITableView *postedJobsTableView;

@end

@implementation JobPostedView

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
[self.postedJobsTableView setSeparatorColor:[UIColor colorWithHex:@"00b050"]];
    // Do any additional setup after loading the view.
    
    self.postedJobsTableView.tableFooterView = [UIView new];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.title = @"Posted Jobs list";

    dispatch_async(dispatch_get_main_queue(), ^{
        [self callingWebServiceForGetPostedJobsList];
    });
}

#pragma mark  Calling Api


-(void)callingWebServiceForGetPostedJobsList{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":LoginUserID};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getPostJob",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                PostedJobArrayDetails = [response valueForKey:@"response"];

              //  [HUD hideAnimated:YES];
                [loader stopAnimating];

                [_postedJobsTableView reloadData];
                
                NSLog(@"HEHHEHEHEHEHEH %@",PostedJobArrayDetails);
                
                for (int i = 0; i<PostedJobArrayDetails.count; i++) {
                    
                    NSLog(@"%@", PostedJobArrayDetails[i]);
                    
                }
                

            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                NSLog(@"response is null");
            }else{
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                          //     [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return PostedJobArrayDetails.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    JobPostedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobPostedTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    NSString *strPosted =  [[PostedJobArrayDetails valueForKey:@"jobTitle"]objectAtIndex:indexPath.row];
    
    NSLog(@"%@", strPosted);
    
    cell.imgProfile.layer.cornerRadius = 25;
    cell.imgProfile.layer.masksToBounds=YES;
    cell.backgroundColor=[UIColor clearColor];

    
    cell.lblUserName.text = [NSString stringWithFormat:@"%@",[[PostedJobArrayDetails valueForKey:@"jobTitle"]objectAtIndex:indexPath.row]];
    
    cell.lblDateNTime.text = [NSString stringWithFormat:@"%@",[[PostedJobArrayDetails valueForKey:@"createdAt"]objectAtIndex:indexPath.row]];
    
    [cell.imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PostedJobsUrl,[[PostedJobArrayDetails valueForKey:@"images"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];

    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

     NSString  *strJobId=[NSString stringWithFormat:@"%@",[[PostedJobArrayDetails valueForKey:@"id"]objectAtIndex:indexPath.row]];
    
    NSLog(@"Selected job Id is here %@",strJobId);

    ApplicantView *jobPostedView = [self.storyboard instantiateViewControllerWithIdentifier:@"ApplicantView"];
    jobPostedView.strJobId = strJobId;

    [self.navigationController pushViewController:jobPostedView animated:YES];
}


@end
