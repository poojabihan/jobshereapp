//
//  JobPostedTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobPostedTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;

@property (strong, nonatomic) IBOutlet UILabel *lblUserName;

@property (strong, nonatomic) IBOutlet UILabel *lblDateNTime;

@end
