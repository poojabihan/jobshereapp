//
//  ProfileViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.


#import "VerifyAccountStep1ViewController.h"
#import "ProfileViewController.h"
#import "ProfileTableViewCell.h"
#import "Webservice.h"
#import "Reachability.h"
//#import <MBProgressHUD.h>
#import "ViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "UIColor+Hexadecimal.h"
#import "UIImageView+WebCache.h"
#import "MessageChatViewController.h"
#import "VerifyAccountViewController.h"
#import "ChatListViewController.h"
#import "FeedbackView.h"
#import "UIColor+Hexadecimal.h"
#import "ShowAddressView.h"
#import "EditAddressView.h"
#import "JobPostedView.h"
#import "JobAppliedView.h"
#import "DGActivityIndicatorView.h"


@interface ProfileViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSString *strAddLine1, *strAddLine2,*strAddLine3,*strDOB;
    NSArray *MenuTitle;NSArray*MenuImage;NSString *imgString;UIImage *chosenImage;
    DGActivityIndicatorView *loader;

}
@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     [self.TableView setSeparatorColor:[UIColor colorWithHex:@"00b050"]];
    
    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:@"FName"];
    NSString *LastName = [[NSUserDefaults standardUserDefaults] valueForKey:@"LName"];
    NSString *Contact = [[NSUserDefaults standardUserDefaults] valueForKey:@"EmailID"];
    NSString *FullName = [NSString stringWithFormat:@"%@ %@",firstName,LastName];
    self.FullNameLabel.text = FullName;self.EmailLabel.text = Contact;
    
    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"Verified"] == nil || [[[NSUserDefaults standardUserDefaults] valueForKey:@"Verified"] isEqualToString:@"No"]) {
        MenuTitle = [NSArray arrayWithObjects:@"Message",@"Verify Your Account",@"Jobs Posted", @"Jobs Applied",@"Invite Friend",@"Give Us A Feedback",@"Bank Account Details",@"Logout", nil];
        MenuImage = [NSArray arrayWithObjects:@"Message",@"Verify",@"posted_job",@"applied_job",@"InviteFriend",@"FeedBack",@"Message",@"Message",nil];
    }
    else {
        MenuTitle = [NSArray arrayWithObjects:@"Message",@"Account Verified",@"Jobs Posted", @"Jobs Applied",@"Invite Friend",@"Give Us A Feedback",@"Bank Account Details",@"Logout", nil];
        MenuImage = [NSArray arrayWithObjects:@"Message",@"verify_green",@"posted_job",@"applied_job",@"InviteFriend",@"FeedBack",@"Message",@"Message",nil];
    }
    
    
    NSString *profileImage = [[NSUserDefaults standardUserDefaults] valueForKey:@"ProfileImage"];
    [self.ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,profileImage]]placeholderImage:[UIImage imageNamed:@"man"]];
    

}

#pragma mark  Hide navigation Bar

-(void)viewWillAppear:(BOOL)animated{
    
    [self callingWebServiceForGetPluseButtonData];
    [self callWebServiceForVerificationInfo];

    [self.navigationController setNavigationBarHidden:YES animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}

-(void)viewWillDisappear:(BOOL)animated{
    
    [self.TableView reloadData];
}

#pragma mark  Add Button for Header

- (IBAction)addButtonAction:(id)sender {
    
    if(![strAddLine1 isEqualToString:@""] && ![strAddLine2 isEqualToString:@""]) {
        
//        ShowAddressView *walkThru = [self.storyboard   instantiateViewControllerWithIdentifier:@"ShowAddressView"];
//        walkThru.strAddLine1 = strAddLine1;
//        walkThru.strAddLine2 = strAddLine2;
//        walkThru.strAddLine3 = strAddLine3;
//        walkThru.strDOB = strDOB;
//        walkThru.providesPresentationContextTransitionStyle = YES;
//        walkThru.definesPresentationContext = YES;
//        [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//        [self.tabBarController presentViewController:walkThru animated:NO completion:nil];

        EditAddressView *walkThru = [self.storyboard   instantiateViewControllerWithIdentifier:@"EditAddressView"];
        walkThru.objProfileView = self;
        walkThru.providesPresentationContextTransitionStyle = YES;
        walkThru.definesPresentationContext = YES;
        walkThru.isForEdit = NO;
        [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self.tabBarController presentViewController:walkThru animated:NO completion:nil];

    }
    else{
        EditAddressView *walkThru = [self.storyboard   instantiateViewControllerWithIdentifier:@"EditAddressView"];
        walkThru.objProfileView = self;
        walkThru.providesPresentationContextTransitionStyle = YES;
        walkThru.definesPresentationContext = YES;
        walkThru.isForEdit = YES;
        [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
        [self.tabBarController presentViewController:walkThru animated:NO completion:nil];
    }
}


#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [MenuTitle count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    ProfileTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"ProfileTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    tableView.separatorColor = [UIColor colorWithHex:@"00b050"];
    
    cell.MenuTitleText.text=[MenuTitle objectAtIndex:indexPath.row];
    cell.MenuIconImage.image=[UIImage imageNamed:[MenuImage objectAtIndex:indexPath.row]];
    
    if ([[MenuTitle objectAtIndex:indexPath.row] isEqualToString:@"Account Verified"]) {
        cell.MenuTitleText.textColor = [UIColor lightGrayColor];
    }
    else {
        cell.MenuTitleText.textColor = [UIColor blackColor];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 58;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.row) {
        case 0:
            [self MessageView];
            break;
        case 1:
            if ([[MenuTitle objectAtIndex:indexPath.row] isEqualToString:@"Verify Your Account"]) {
                [tableView deselectRowAtIndexPath:indexPath animated:YES];
                [self VerifyYourAccount];
            }
            break;
        case 2:
            [self JobPosted];
            break;
        case 3:
            [self JobApplied];
            break;
        case 4:
            [self InviteFriend];
            break;
        case 5:
            [self GiveUsAFeedback];
            break;
        case 6:
            [self performSegueWithIdentifier:@"toAccount" sender:self];
            break;
        case 7:
            [self LogoutClick];
            break;
        default:
            break;
    }
}

#pragma mark  Message View

-(void)MessageView{
    
    ChatListViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatListViewController"];
    [self.navigationController pushViewController:ChatVC animated:YES];
}

#pragma mark  Verify Your Account

-(void)VerifyYourAccount{
    
    VerifyAccountStep1ViewController *VerifyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyAccountStep1ViewController"];
    [self.navigationController pushViewController:VerifyVC animated:YES];
}

#pragma mark  Job Posted

-(void)JobPosted{
    
    JobPostedView *jobPostedView = [self.storyboard instantiateViewControllerWithIdentifier:@"JobPostedView"];
    
    [self.navigationController pushViewController:jobPostedView animated:YES];
}

#pragma mark  Job Applied

-(void)JobApplied{
    
    JobAppliedView *jobAppliedView = [self.storyboard instantiateViewControllerWithIdentifier:@"JobAppliedView"];
    
    [self.navigationController pushViewController:jobAppliedView animated:YES];
    
}


#pragma mark  Invite Friend

-(void)InviteFriend{
    
    NSString *shareWithStringContent = @"Hello jobs Here";
    NSArray *itemsToShare = @[shareWithStringContent];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc]initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,UIActivityTypePostToTencentWeibo];
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

#pragma mark  Give Us A Feedback

-(void)GiveUsAFeedback{
    
    FeedbackView *walkThru = [self.storyboard   instantiateViewControllerWithIdentifier:@"FeedbackView"];
    walkThru.providesPresentationContextTransitionStyle = YES;
    walkThru.definesPresentationContext = YES;
    [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self.tabBarController presentViewController:walkThru animated:NO completion:nil];
}

#pragma mark  Logout

-(void)LogoutClick{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert    message:@"Are you sure want to logout?" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * logout = [UIAlertAction actionWithTitle:@"Yes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":UserId};
        
        NSString *loginURL=[NSString stringWithFormat:@"%@api/userLogout",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                NSLog(@"Logout Response %@",response);
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"EmailID"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"FName"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LName"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"UserID"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Login"];
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Verified"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                ViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
                VC.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:VC animated:NO];
                
            }
            
        } failure:^(NSError *error) {
            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        }];
        
    }];
    
    UIAlertAction *dismiss = [UIAlertAction actionWithTitle:@"No" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    [alert addAction:logout];
    [alert addAction:dismiss];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alert animated:YES completion:nil];
    });
}

- (IBAction)ProfileButton:(id)sender {
    
    UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
        
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
        {
            
            [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
        }
        else
        {
            [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"OK"];
        }
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }]];
    
    [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
        {
            UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
            photoPicker.delegate = self;
            photoPicker.allowsEditing = NO;
            photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:photoPicker animated:YES completion:NULL];
        }
        
    }]];
    
    [self presentViewController:actionSheet animated:YES completion:nil];
    
}

#pragma mark  Show Camera

-(void)showcamera
{
    
    [self.tabBarController.tabBar setHidden:YES];
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];
    }
    else
    {
        [self camDenied];
    }
    
}

#pragma mark  Pop Camera

-(void)popCamera{
    
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}

#pragma mark  Cam Denied

- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark  Image Picker Select Image

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.tabBarController.tabBar setHidden:NO];
    
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    
    self.ProfileImage.image = chosenImage;
    
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.ProfileImage.image, nil, nil, nil);
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
        [self ProfileUploadingWebservices];
    });
}
#pragma mark  Error Alert

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Profile Uploading Web Service
-(void) callWebServiceForVerificationInfo {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];
    
    NSString *str = [NSString stringWithFormat:@"%@api/getVerifiedInformation",KBaseUrl];
    
    NSDictionary *dictParam = @{@"userId":loginUserID};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        NSLog(@"response %@",response);
        
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            if ([[response valueForKeyPath:@"result.email"] isEqualToString:@"Yes"] && [[response valueForKeyPath:@"result.is_verified"] isEqualToString:@"Yes"]) {
                MenuTitle = [NSArray arrayWithObjects:@"Message",@"Account Verified",@"Jobs Posted", @"Jobs Applied",@"Invite Friend",@"Give Us A Feedback",@"Bank Account Details",@"Logout", nil];
                MenuImage = [NSArray arrayWithObjects:@"Message",@"verify_green",@"posted_job",@"applied_job",@"InviteFriend",@"FeedBack",@"Message",@"Message",nil];
                [self.TableView reloadData];
                [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"Verified"];
            }
        }
    } failure:^(NSError *error) {
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        //NSLog(error);
    }];
}
-(void)ProfileUploadingWebservices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if([self image:self.ProfileImage.image isEqualTo:[UIImage imageNamed:@"blank_icon.png"]]==YES){
            
            imgString=@"";
            
        }else{
            
            NSData *imageData = UIImageJPEGRepresentation(self.ProfileImage.image, 0.2);
            imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            
            NSString *imgName = [_ProfileImage image].accessibilityIdentifier;
            NSLog(@"%@",imgName);
            
        }
        
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"];
        
//        MBProgressHUD  *HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        HUD.bezelView.color = [UIColor colorWithHex:@"43C3F9"];
//        HUD.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
//        HUD.contentColor = [UIColor whiteColor];
//        HUD.label.text = NSLocalizedString(@"Uploading...", @"HUD loading title");
        
        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        
        
        
        NSDictionary *dictParam = @{@"userId":UserId,@"profileImage":imgString};
        
        NSString *loginURL=[NSString stringWithFormat:@"%@api/updateProfile",KBaseUrl];
        
        [Webservice requestPostUrl:loginURL parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"Get response %@",response);
            
            [[NSUserDefaults standardUserDefaults]setValue:[[response valueForKey:@"result"]valueForKey:@"profileImage"] forKey:@"ProfileImage"];
            
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                   // [HUD hideAnimated:YES afterDelay:1];
                    [self.ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@", [[response valueForKey:@"result"]valueForKey:@"profileImage"]]]placeholderImage:[UIImage imageNamed:@"man"]];
                    [self alertWithTitle:kAppNameAlert message:@"Image uploaded successfully." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    [loader stopAnimating];
                });
                
            }else if ([[response objectForKey:@"error"]objectForKey:@"check_user"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[HUD hideAnimated:YES afterDelay:1];
                    [loader stopAnimating];

                });
                [self alertWithTitle:kAppNameAlert message:@"User name or password is incorrect" actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES afterDelay:1];
                    [loader stopAnimating];

                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                NSLog(@"response is null");
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES afterDelay:1];
                    [loader stopAnimating];

                });
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES ];
                [loader stopAnimating];

            });
            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:@"The Internet connection appears to be offline." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    
}

#pragma mark  Img1 = Imge2

- (BOOL)image:(UIImage *)image1 isEqualTo:(UIImage *)image2
{
    NSData *data1 = UIImagePNGRepresentation(image1);
    NSData *data2 = UIImagePNGRepresentation(image2);
    if ([data1 isEqualToData:data2]) {
        return YES;
    }else{
        return NO;
    }
}

#pragma mark  Remove Stored Cache

- (void)removeAllStoredCredentials
{
    // Delete any cached URLrequests!
    NSURLCache *sharedCache = [NSURLCache sharedURLCache];
    [sharedCache removeAllCachedResponses];
    
    // Also delete all stored cookies!
    NSHTTPCookieStorage *cookieStorage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray *cookies = [cookieStorage cookies];
    id cookie;
    for (cookie in cookies) {
        [cookieStorage deleteCookie:cookie];
    }
    
    NSDictionary *credentialsDict = [[NSURLCredentialStorage sharedCredentialStorage] allCredentials];
    if ([credentialsDict count] > 0) {
        // the credentialsDict has NSURLProtectionSpace objs as keys and dicts of userName => NSURLCredential
        NSEnumerator *protectionSpaceEnumerator = [credentialsDict keyEnumerator];
        id urlProtectionSpace;
        // iterate over all NSURLProtectionSpaces
        while (urlProtectionSpace = [protectionSpaceEnumerator nextObject]) {
            NSEnumerator *userNameEnumerator = [[credentialsDict objectForKey:urlProtectionSpace] keyEnumerator];
            id userName;
            // iterate over all usernames for this protectionspace, which are the keys for the actual NSURLCredentials
            while (userName = [userNameEnumerator nextObject]) {
                NSURLCredential *cred = [[credentialsDict objectForKey:urlProtectionSpace] objectForKey:userName];
                NSLog(@"credentials to be removed: %@", cred);
                [[NSURLCredentialStorage sharedCredentialStorage] removeCredential:cred forProtectionSpace:urlProtectionSpace];
            }
        }
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}
#pragma mark  Custom Methods for Calling WebService [Pluse Button]

-(void)callingWebServiceForGetPluseButtonData{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":LoginUserID};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getUserProfile",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
    
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
                
                strAddLine1 = [[response objectForKey:@"result"] valueForKey:@"line1"];
                strAddLine2 = [[response objectForKey:@"result"] valueForKey:@"line2"];
                strAddLine3 = [[response objectForKey:@"result"] valueForKey:@"line3"];
                strDOB = [[response objectForKey:@"result"] valueForKey:@"dateOfBirth"];
                
                [_ProfileImage setImageWithURL:[NSURL URLWithString:[[response objectForKey:@"result"] valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"man"]];
                
                NSLog(@"here is the add data LINE1%@ LINE2 %@",strAddLine1,strAddLine2);
                
                
            }else if (response==NULL) {
                

                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }
        } failure:^(NSError *error) {

            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        }];
        
        
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    
    
}


@end
