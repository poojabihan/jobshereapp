//
//  BankAccountDetailsViewController.m
//  JobsHereApp
//
//  Created by kdstudio on 24/10/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "BankAccountDetailsViewController.h"
#import "Reachability.h"
//#import <MBProgressHUD.h>
#import "Webservice.h"
#import "DGActivityIndicatorView.h"

@interface BankAccountDetailsViewController (){
    //MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

    
}
@property (weak, nonatomic) IBOutlet UILabel *lblUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtAccountNo;
@property (weak, nonatomic) IBOutlet UITextField *txtSwiftCode;

@property (weak, nonatomic) IBOutlet UITextField *txtNamw;
@end

@implementation BankAccountDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSString *firstName = [[NSUserDefaults standardUserDefaults] valueForKey:@"FName"];
    NSString *LastName = [[NSUserDefaults standardUserDefaults] valueForKey:@"LName"];
    NSString *FullName = [NSString stringWithFormat:@"%@ %@",firstName,LastName];

    _lblUsername.text = FullName;
    
    [self callWebServiceGetBank];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callWebServiceAddBank {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
//        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        HUD.bezelView.color = [UIColor blackColor];
//        HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//        HUD.contentColor = [UIColor whiteColor];
        
        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        
        
        NSString *UserID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];        NSDictionary *dictParam = @{@"userId":UserID,@"account_name":_txtNamw.text,@"account_number":_txtAccountNo.text,@"account_swift":_txtSwiftCode.text};
        NSLog(@"Dic Param - %@",dictParam);
        
        NSString *JobsPost=[NSString stringWithFormat:@"%@api/addBankAccount",KBaseUrl];
        
        [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
            
            if ([[response objectForKey:@"status"] boolValue] == 1) {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [loader stopAnimating];
                    //[HUD hideAnimated:YES];
                });
                
                UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Bank details added successfully." preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                    [self dismissViewControllerAnimated:NO completion:nil];
                }];
                [alertCont addAction:okAction];
                alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alertCont animated:true completion:nil];
                });
                
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[HUD hideAnimated:YES];
                    [loader stopAnimating];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES];
                    [loader stopAnimating];
                });
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES];
                [loader stopAnimating];
                
            });
            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)callWebServiceGetBank {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
//        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        HUD.bezelView.color = [UIColor blackColor];
//        HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//        HUD.contentColor = [UIColor whiteColor];
        
        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        
        
        NSString *UserID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];        NSDictionary *dictParam = @{@"userId":UserID};
        NSLog(@"Dic Param - %@",dictParam);
        
        NSString *JobsPost=[NSString stringWithFormat:@"%@api/getBankAccount",KBaseUrl];
        
        [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
            
            if (([[response objectForKey:@"status"] boolValue] == 1)) {
                
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES];
                    [loader stopAnimating];
                });
                
                _txtNamw.text = ![[response valueForKeyPath:@"result.account_name"] isKindOfClass:[NSNull class]] ? [response valueForKeyPath:@"result.account_name"] : @"";
                _txtAccountNo.text = ![[response valueForKeyPath:@"result.account_number"] isKindOfClass:[NSNull class]] ? [response valueForKeyPath:@"result.account_number"] : @"";
                _txtSwiftCode.text = ![[response valueForKeyPath:@"result.account_swift"] isKindOfClass:[NSNull class]] ? [response valueForKeyPath:@"result.account_swift"] : @"";

                
            }else if (response==NULL) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [loader stopAnimating];

                    //[HUD hideAnimated:YES];
                });
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [loader stopAnimating];

                    // [HUD hideAnimated:YES];
                });
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }
        } failure:^(NSError *error) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [loader stopAnimating];
// [HUD hideAnimated:YES];
            });
            NSLog(@"Error %@",error);
            [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
        }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

- (IBAction)btnConfirmAction:(id)sender {
    
    if ([_txtNamw.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter account name."];
    }
    else if ([_txtAccountNo.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter account number."];
    }
    else if ([_txtSwiftCode.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter swift code."];
    }
    else
        [self callWebServiceAddBank];
}

- (IBAction)btnCancelAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
