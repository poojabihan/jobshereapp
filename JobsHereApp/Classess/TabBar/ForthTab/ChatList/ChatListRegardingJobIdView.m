//
//  ChatListRegardingJobIdView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 22/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "ChatListRegardingJobIdView.h"
#import "MessageChatViewController.h"
//#import <MBProgressHUD.h>
#import "Webservice.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "ChatListRegardingJobIdTableViewCell.h"
#import "DGActivityIndicatorView.h"

@interface ChatListRegardingJobIdView ()
{
    //MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;
    UIRefreshControl *refreshController;NSMutableArray *ChatData;
}
@property (strong, nonatomic) IBOutlet UITableView *chatListTableView;
@end

@implementation ChatListRegardingJobIdView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    ChatData = [[NSMutableArray alloc] init];
    self.chatListTableView.backgroundColor = [UIColor clearColor];;
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
//
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    // Do any additional setup after loading the view.
    // Do any additional setup after loading the view.
    self.chatListTableView.tableFooterView = [UIView new];

}

-(void)viewWillAppear:(BOOL)animated{
    
    [self ChatListLoadingWebservices ];
    
    self.title =@"Chats";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)ChatListLoadingWebservices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUser = [standardUserDefaults stringForKey:@"UserID"];
        NSDictionary *dictParam = @{ @"userId":LoginUser ,@"jobId" : _strJobId };
        NSString *GetJobs=[NSString stringWithFormat:@"%@api/getChat",KBaseUrl];
        
        [Webservice requestPostUrl:GetJobs parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"%@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ChatData = [response objectForKey:@"response"];
                
                if ([ChatData count ]==0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        _chatListTableView.hidden=YES;
                       // [HUD hideAnimated:YES];
                        [loader stopAnimating];
                        [refreshController endRefreshing];
                    });
                }
                else
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.chatListTableView reloadData];
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];

                        [refreshController endRefreshing];
                    });
                }
            }
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                [refreshController endRefreshing];
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ChatData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    
    ChatListRegardingJobIdTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatListRegardingJobIdTableViewCell"];
    
    cell.backgroundColor=[UIColor clearColor];
    
    cell.imgProfile.layer.cornerRadius=cell.imgProfile.frame.size.height/2;
    
    cell.imgProfile.layer.masksToBounds=YES;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(cell == nil){
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatListTableViewCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.lblUserName.text=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"name"]objectAtIndex:indexPath.row]];
    
    
    [cell.imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,[[ChatData valueForKey:@"profileImage"]objectAtIndex:indexPath.row]]] placeholderImage:[UIImage imageNamed:@"man"]];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    
        MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
    
        ChatVC.JobID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"job_id"]objectAtIndex:indexPath.row]];
    
        ChatVC.UserID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"sendFrom"]objectAtIndex:indexPath.row]];
    
        ChatVC.NavigationTitle=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"name"]objectAtIndex:indexPath.row]];
    
       [self.navigationController pushViewController:ChatVC animated:YES];
    
}

@end
