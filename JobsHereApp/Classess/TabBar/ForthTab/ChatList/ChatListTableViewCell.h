//
//  ChatListTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 01/02/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *ProfileImage;

@property (strong, nonatomic) IBOutlet UILabel *UserNameLabel;
@property (strong, nonatomic) IBOutlet UILabel *LastMessageLabel;




@end
