//
//  ChatListRegardingJobIdView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 22/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListRegardingJobIdView : UIViewController

@property (strong, nonatomic) NSString *strJobId;

@end
