//
//  ChatListViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 01/02/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "ChatListViewController.h"
#import "ChatListTableViewCell.h"
#import "MessageChatViewController.h"
//#import <MBProgressHUD.h>
#import "Webservice.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
#import "ChatListRegardingJobIdView.h"
#import "DGActivityIndicatorView.h"

@interface ChatListViewController ()
{
    //MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;

    UIRefreshControl *refreshController;NSMutableArray *ChatData;
}
@end

@implementation ChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    ChatData = [[NSMutableArray alloc] init];
    self.TableView.backgroundColor = [UIColor clearColor];;
    
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor=[UIColor darkGrayColor];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing Data...."];
    [self.TableView addSubview:refreshController];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    // Do any additional setup after loading the view.
    self.TableView.tableFooterView = [UIView new];
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.title =@"All Chats";

    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self ChatListLoadingWebservices];
    });
}

#pragma mark  Refresh Activity

-(void)handleRefresh:(id)sender{
    
    [self ChatListLoadingWebservices];
    
}

-(void)ChatListLoadingWebservices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
    
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUser = [standardUserDefaults stringForKey:@"UserID"];
        NSDictionary *dictParam = @{ @"userId":LoginUser };
        NSString *GetJobs=[NSString stringWithFormat:@"%@api/getAllChat",KBaseUrl];
        
        [Webservice requestPostUrl:GetJobs parameters:dictParam success:^(NSDictionary *response) {
            
            NSLog(@"%@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                ChatData = [response objectForKey:@"response"];
               
                if ([ChatData count ]==0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                         _TableView.hidden=YES;
                       // [HUD hideAnimated:YES];
                        [loader stopAnimating];
                        [refreshController endRefreshing];
                    });
                }
                else
                    {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.TableView reloadData];
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];

                        [refreshController endRefreshing];
                    });
                }
            }
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
             //   [HUD hideAnimated:YES];
                [loader stopAnimating];

                [refreshController endRefreshing];
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

            });
            NSLog(@"Error %@",error);
        }];
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [ChatData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    
    ChatListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatListTableViewCell"];
    
    cell.backgroundColor=[UIColor clearColor];
    
    cell.ProfileImage.layer.cornerRadius=cell.ProfileImage.frame.size.height/2;
    
    cell.ProfileImage.layer.masksToBounds=YES;
    
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    
    if(cell == nil){
        
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"ChatListTableViewCell" owner:self options:nil];
        
        cell = [nib objectAtIndex:0];
        
    }
    
    cell.UserNameLabel.text=[NSString stringWithFormat:@"%@ - %@",[[ChatData valueForKey:@"userName"]objectAtIndex:indexPath.row],[[ChatData valueForKey:@"jobTitle"]objectAtIndex:indexPath.row]];
    
    
    [cell.ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,[[ChatData valueForKey:@"profileImage"]objectAtIndex:indexPath.row]]] placeholderImage:[UIImage imageNamed:@"man"]];
    
    return cell;
    
}

-(CGFloat)tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 54;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
    
    ChatVC.JobID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"job_id"]objectAtIndex:indexPath.row]];
    
    ChatVC.UserID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"sendFrom"]objectAtIndex:indexPath.row]];
    
    ChatVC.NavigationTitle=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"userName"]objectAtIndex:indexPath.row]];
    
    [self.navigationController pushViewController:ChatVC animated:YES];
    
    
   /* -ChatListRegardingJobIdView * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChatListRegardingJobIdView"];
    
      ChatVC.strJobId=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"job_id"]objectAtIndex:indexPath.row]];
    
      [self.navigationController pushViewController:ChatVC animated:YES];*/
    
    //////****************************////////////////
    
//    MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
//
//    ChatVC.JobID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"job_id"]objectAtIndex:indexPath.row]];
//
//    ChatVC.UserID=[NSString stringWithFormat:@"%@",[[ChatData valueForKey:@"sendFrom"]objectAtIndex:indexPath.row]];
//
//    ChatVC.NavigationTitle=[NSString stringWithFormat:@"%@ %@",[[ChatData valueForKey:@"fname"]objectAtIndex:indexPath.row],[[ChatData valueForKey:@"lname"]objectAtIndex:indexPath.row]];
//
//   [self.navigationController pushViewController:ChatVC animated:YES];
    
}

@end
