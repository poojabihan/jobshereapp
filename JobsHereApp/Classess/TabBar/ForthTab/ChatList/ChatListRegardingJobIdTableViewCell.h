//
//  ChatListRegardingJobIdTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 22/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatListRegardingJobIdTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UIImageView *imgProfile;
@property (strong, nonatomic) IBOutlet UILabel *lblUserName;

@end
