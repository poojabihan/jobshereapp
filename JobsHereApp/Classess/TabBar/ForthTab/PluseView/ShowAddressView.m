//
//  ShowAddressView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "ShowAddressView.h"
//#import <SVProgressHUD.h>
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "EditAddressView.h"
//#import <MBProgressHUD.h>
#import "DGActivityIndicatorView.h"

@interface ShowAddressView ()
{
  //  MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;

    
}
@property (strong, nonatomic) IBOutlet UIView *ShowAddressDetailView;


@end

@implementation ShowAddressView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    _ShowAddressDetailView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _ShowAddressDetailView.layer.shadowOffset = CGSizeMake(0, 0);
    _ShowAddressDetailView.layer.shadowOpacity = 1;
    _ShowAddressDetailView.layer.shadowRadius = 1.0;
    _ShowAddressDetailView.layer.masksToBounds = NO;
    
    self.ShowAddressDetailView.layer.borderWidth = 1;
    self.ShowAddressDetailView.layer.borderColor = [UIColor grayColor].CGColor;
    // Do any additional setup after loading the view.
    
    _lblAddLine1.text = _strAddLine1;
    _lblAddLine2.text = _strAddLine2;
    _lblAddLine3.text = _strAddLine3;
    _lblAddDOB.text = _strDOB;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimissView)];
    [self.bgView addGestureRecognizer:tap];
}

- (void) dimissView {
    [self dismissViewControllerAnimated:NO completion:nil];
}

-(void)viewWillAppear:(BOOL)animated{
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
//
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
     _lblAddLine1.text = @"";
     _lblAddLine2.text = @"";
     _lblAddLine3.text = @"";
    _lblAddDOB.text = @"";
    [self callingWebServiceForGetPluseButtonData];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)callingWebServiceForGetPluseButtonData{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":LoginUserID};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getUserProfile",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
          //  [HUD hideAnimated:YES];
            [loader stopAnimating];
            NSLog(@"Response %@",response);
           
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
                
                NSString *strAddLine1 = [[response objectForKey:@"result"] valueForKey:@"line1"];
                NSString *strAddLine2 = [[response objectForKey:@"result"] valueForKey:@"line2"];
                NSString *strAddLine3 = [[response objectForKey:@"result"] valueForKey:@"line3"];
                
                NSLog(@"here is the add data LINE1%@ LINE2 %@",strAddLine1,strAddLine2);
                
                _lblAddLine1.text = strAddLine1;
                _lblAddLine2.text = strAddLine2;
                _lblAddLine3.text = strAddLine3;
                _lblAddDOB.text = [[response objectForKey:@"result"] valueForKey:@"dateOfBirth"];;
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
            }else{
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                             //  [HUD hideAnimated:YES];
                               [loader stopAnimating];
                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

            NSLog(@"Error %@",error);
        }];
    }else{
     //   [HUD hideAnimated:YES];
        [loader stopAnimating];
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Action
- (IBAction)btnEditAction:(id)sender {
    
    EditAddressView *walkThru = [self.storyboard instantiateViewControllerWithIdentifier:@"EditAddressView"];
    walkThru.strAddLine1 = _lblAddLine1.text;
    walkThru.strAddLine2 = _lblAddLine2.text;
    walkThru.strAddLine3 = _lblAddLine3.text;
    walkThru.strDOB = _lblAddDOB.text;
    walkThru.providesPresentationContextTransitionStyle = YES;
    walkThru.definesPresentationContext = YES;
    [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:walkThru animated:NO completion:nil];
}

- (IBAction)ActionToDismissTheView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    
}
#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}


@end
