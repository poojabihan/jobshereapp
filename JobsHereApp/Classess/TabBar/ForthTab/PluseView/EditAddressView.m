//
//  EditAddressView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "EditAddressView.h"
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "ProfileViewController.h"
//#import <MBProgressHUD.h>
#import "UIColor+Hexadecimal.h"
#import "DGActivityIndicatorView.h"

@interface EditAddressView () 
{
    UIDatePicker *datePicker;
    NSString *formattedDate;
   // MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

}
@property (nonatomic, retain) UIToolbar* toolBar;
@property (strong, nonatomic) IBOutlet UITextField *textFieldAddLine1;
@property (strong, nonatomic) IBOutlet UITextField *textFieldAddLine2;
@property (strong, nonatomic) IBOutlet UITextField *textFieldAddLine3;
@property (strong, nonatomic) IBOutlet UITextField *BirthdateTextfield;
@property (strong, nonatomic) IBOutlet UIView *editAddressDetailView;

//ForShowPopup
@property (strong, nonatomic) IBOutlet UIView *ShowAddressDetailView;

@end

@implementation EditAddressView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimissView)];
    [self.bgView addGestureRecognizer:tap];
    

    
    if (_isForEdit) {
        _editAddressDetailView.hidden = NO;
        _ShowAddressDetailView.hidden = YES;
        
        _editAddressDetailView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        _editAddressDetailView.layer.shadowOffset = CGSizeMake(0, 0);
        _editAddressDetailView.layer.shadowOpacity = 1;
        _editAddressDetailView.layer.shadowRadius = 1.0;
        _editAddressDetailView.layer.masksToBounds = NO;
        
        _editAddressDetailView.layer.borderWidth = 1;
        _editAddressDetailView.layer.borderColor = [UIColor grayColor].CGColor;
        
        self.BirthdateTextfield.delegate = self;
        
        datePicker = [[UIDatePicker alloc]init];
        [datePicker setDate:[NSDate date]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"dd-MMM-yyyy";
        
        [datePicker setDatePickerMode:UIDatePickerModeDate];
        [datePicker setMaximumDate: [NSDate date]];
        
        self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(updateTextField:)];
        NSArray* barItems = [NSArray arrayWithObjects:doneButton, nil];
        [self.toolBar setItems:barItems animated:YES];
        
        self.BirthdateTextfield.inputAccessoryView = self.toolBar;
        
        [self.BirthdateTextfield setInputView:datePicker];
        
        _textFieldAddLine1.text = _strAddLine1;
        _textFieldAddLine2.text = _strAddLine2;
        _textFieldAddLine3.text = _strAddLine3;
        _BirthdateTextfield.text = _strDOB;


    }
    else{
        _editAddressDetailView.hidden = YES;
        _ShowAddressDetailView.hidden = NO;
        

    }
    
    //For show popup
    _ShowAddressDetailView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _ShowAddressDetailView.layer.shadowOffset = CGSizeMake(0, 0);
    _ShowAddressDetailView.layer.shadowOpacity = 1;
    _ShowAddressDetailView.layer.shadowRadius = 1.0;
    _ShowAddressDetailView.layer.masksToBounds = NO;
    
    self.ShowAddressDetailView.layer.borderWidth = 1;
    self.ShowAddressDetailView.layer.borderColor = [UIColor grayColor].CGColor;
    // Do any additional setup after loading the view.
    
    _lblAddLine1.text = _strAddLine1;
    _lblAddLine2.text = _strAddLine2;
    _lblAddLine3.text = _strAddLine3;
    _lblAddDOB.text = _strDOB;


}

-(void)viewWillAppear:(BOOL)animated {
    
    if (!_isForEdit) {
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        
    _lblAddLine1.text = @"";
    _lblAddLine2.text = @"";
    _lblAddLine3.text = @"";
    _lblAddDOB.text = @"";
    [self callingWebServiceForGetPluseButtonDataShow];
    }

}
- (void) dimissView {
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  Button Actions
- (IBAction)btnEditAction:(id)sender {
    
    _editAddressDetailView.hidden = NO;
    _ShowAddressDetailView.hidden = YES;
    
//    EditAddressView *walkThru = [self.storyboard instantiateViewControllerWithIdentifier:@"EditAddressView"];
//    walkThru.strAddLine1 = _lblAddLine1.text;
//    walkThru.strAddLine2 = _lblAddLine2.text;
//    walkThru.strAddLine3 = _lblAddLine3.text;
//    walkThru.strDOB = _lblAddDOB.text;
//    walkThru.providesPresentationContextTransitionStyle = YES;
//    walkThru.definesPresentationContext = YES;
//    [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
//    [self presentViewController:walkThru animated:NO completion:nil];
    
    
    
    _editAddressDetailView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _editAddressDetailView.layer.shadowOffset = CGSizeMake(0, 0);
    _editAddressDetailView.layer.shadowOpacity = 1;
    _editAddressDetailView.layer.shadowRadius = 1.0;
    _editAddressDetailView.layer.masksToBounds = NO;
    
    _editAddressDetailView.layer.borderWidth = 1;
    _editAddressDetailView.layer.borderColor = [UIColor grayColor].CGColor;
    
    self.BirthdateTextfield.delegate = self;
    
    datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MMM-yyyy";
    
    [datePicker setDatePickerMode:UIDatePickerModeDate];
    [datePicker setMaximumDate: [NSDate date]];
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(updateTextField:)];
    NSArray* barItems = [NSArray arrayWithObjects:doneButton, nil];
    [self.toolBar setItems:barItems animated:YES];
    
    self.BirthdateTextfield.inputAccessoryView = self.toolBar;
    
    [self.BirthdateTextfield setInputView:datePicker];
    
    _textFieldAddLine1.text = _lblAddLine1.text;
    _textFieldAddLine2.text = _lblAddLine2.text;
    _textFieldAddLine3.text = _lblAddLine3.text;
    _BirthdateTextfield.text = _lblAddDOB.text;

}

- (IBAction)actionSave:(id)sender {
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    [self callingWebServiceForGetPluseButtonData];
}

#pragma mark  Custom Functions
-(void)updateTextField:(id)sender {
    
    UIDatePicker *picker = (UIDatePicker*)self.BirthdateTextfield.inputView;
    self.BirthdateTextfield.text = [self formatDate:picker.date];
    
    [self.BirthdateTextfield resignFirstResponder];
}

- (NSString *)formatDate:(NSDate *)date {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterShortStyle];
    //2018-05-24
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    formattedDate = [dateFormatter stringFromDate:date];
    return formattedDate;
}


#pragma mark  UITextfield Delegates


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldBeginEditing");
    
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    
    return YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    NSLog(@"textFieldShouldEndEditing");
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1;
    border.borderColor = [UIColor whiteColor].CGColor;
    border.frame = CGRectMake(0, textField.frame.size.height - borderWidth, textField.frame.size.width, textField.frame.size.height);
    border.borderWidth = borderWidth;
    [textField.layer addSublayer:border];
    textField.layer.masksToBounds = YES;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return NO;
}

#pragma mark  Api Calling
-(void)callingWebServiceForGetPluseButtonDataShow{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":LoginUserID};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getUserProfile",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
           // [HUD hideAnimated:YES];
            [loader stopAnimating];
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                
                NSString *strAddLine1 = [[response objectForKey:@"result"] valueForKey:@"line1"];
                NSString *strAddLine2 = [[response objectForKey:@"result"] valueForKey:@"line2"];
                NSString *strAddLine3 = [[response objectForKey:@"result"] valueForKey:@"line3"];
                
                NSLog(@"here is the add data LINE1%@ LINE2 %@",strAddLine1,strAddLine2);
                
                _lblAddLine1.text = strAddLine1;
                _lblAddLine2.text = strAddLine2;
                _lblAddLine3.text = strAddLine3;
                _lblAddDOB.text = [[response objectForKey:@"result"] valueForKey:@"dateOfBirth"];;
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
            }else{
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                              // [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:kAppNameAlert message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}


-(void)callingWebServiceForGetPluseButtonData{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
    
      //  [HUD hideAnimated:YES];
        [loader stopAnimating];

        if([_textFieldAddLine1.text isEqualToString:@""] || [_textFieldAddLine2.text isEqualToString:@""] || [_textFieldAddLine3.text isEqualToString:@""] ){
            
            [self validationErrorAlert:@"Please Enter Complete Address"];
            
        }
        else if([_BirthdateTextfield.text isEqualToString:@""]){
            [self validationErrorAlert:@"Please Enter DOB"];
        }
        else {
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam =  @{@"userId":LoginUserID,
          @"line1":_textFieldAddLine1.text,
          @"line2":_textFieldAddLine2.text,
          @"line3":_textFieldAddLine3.text,
          @"dateOfBirth":_BirthdateTextfield.text};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/updateProfile",KBaseUrl];
        
        NSLog(@"%@", dictParam);
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
           // [HUD hideAnimated:YES];
            [loader stopAnimating];

            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                [self validationErrorAlert:@"Profile Update Successfully"];
                [self.objProfileView callingWebServiceForGetPluseButtonData];
                [self dimissView];
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
                NSLog(@"response is null");
                
            }else{
                
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }
        }
                           failure:^(NSError *error) {
                             //  [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
        }
        
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    
    
}


#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}



@end
