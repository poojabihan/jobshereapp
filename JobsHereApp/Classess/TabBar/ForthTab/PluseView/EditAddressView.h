//
//  EditAddressView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProfileViewController.h"
#import "ShowAddressView.h"

@interface EditAddressView : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) NSString *strAddLine1;
@property (strong, nonatomic) NSString *strAddLine2;
@property (strong, nonatomic) NSString *strAddLine3;
@property (strong, nonatomic) NSString *strDOB;

@property (strong, nonatomic) ProfileViewController *objProfileView;

@property (strong, nonatomic) IBOutlet UIView *bgView;


//ForShowPopUp
@property (strong, nonatomic) IBOutlet UILabel *lblAddLine1;
@property (strong, nonatomic) IBOutlet UILabel *lblAddLine2;
@property (strong, nonatomic) IBOutlet UILabel *lblAddLine3;
@property (strong, nonatomic) IBOutlet UILabel *lblAddDOB;
//@property (strong, nonatomic) NSString *strAddLine1;
//@property (strong, nonatomic) NSString *strAddLine2;
//@property (strong, nonatomic) NSString *strAddLine3;
//@property (strong, nonatomic) NSString *strDOB;
//@property (strong, nonatomic) IBOutlet UIView *bgView;

@property BOOL isForEdit;

@end
