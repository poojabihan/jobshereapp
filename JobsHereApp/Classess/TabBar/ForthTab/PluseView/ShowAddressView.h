//
//  ShowAddressView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShowAddressView : UIViewController

@property (strong, nonatomic) IBOutlet UILabel *lblAddLine1;

@property (strong, nonatomic) IBOutlet UILabel *lblAddLine2;

@property (strong, nonatomic) IBOutlet UILabel *lblAddLine3;

@property (strong, nonatomic) IBOutlet UILabel *lblAddDOB;

@property (strong, nonatomic) NSString *strAddLine1;
@property (strong, nonatomic) NSString *strAddLine2;
@property (strong, nonatomic) NSString *strAddLine3;
@property (strong, nonatomic) NSString *strDOB;

@property (strong, nonatomic) IBOutlet UIView *bgView;


@end
