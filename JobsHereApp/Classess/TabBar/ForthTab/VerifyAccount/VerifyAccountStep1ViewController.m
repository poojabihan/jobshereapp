//
//  VerifyAccountStep1ViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "VerifyAccountStep1ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <linkedin-sdk/LISDK.h>
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "VerifyAccountViewController.h"
//#import <MBProgressHUD.h>
#import <linkedin-sdk/LISDK.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#include "UIImageResizing.h"
#import "DGActivityIndicatorView.h"


@interface VerifyAccountStep1ViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>{
   // MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;

    UIImage *chosenImage;
}
@property (nonatomic, retain) UIToolbar* toolBar;
@property (weak, nonatomic) IBOutlet UILabel *lblGovID;
@property (weak, nonatomic) IBOutlet UILabel *lblLineGovId;

@end

@implementation VerifyAccountStep1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
//    self.emailView.layer.borderWidth = 1;
//    self.emailView.layer.borderColor = [UIColor colorWithHex:@"4CBBE8"].CGColor;

    //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceived:) name:@"VerifiedEmailPushRecived" object:nil];

   
    
    self.phoneView.layer.borderWidth = 1;
    self.phoneView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;

    self.governmentView.layer.borderWidth = 1;
    self.governmentView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;

    _txtEmail.text = [[NSUserDefaults standardUserDefaults] valueForKey:@"EmailID"];
    
    [self callWebServiceForVerificationInfo];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    NSString *linkedInConnect = [[NSUserDefaults standardUserDefaults] valueForKey:@"LinkedInConnect"];
    NSString *fbConnect = [[NSUserDefaults standardUserDefaults] valueForKey:@"FBConnect"];

    if ([linkedInConnect isEqualToString:@"Yes"]) {
        [_btnLinkedInConnect setTitle:@"Connected" forState:UIControlStateNormal];
    }
    
    if ([fbConnect isEqualToString:@"Yes"]) {
        [_btnFBConnect setTitle:@"Connected" forState:UIControlStateNormal];
    }
    
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(updateTextField:)];
    NSArray* barItems = [NSArray arrayWithObjects:doneButton, nil];
    [self.toolBar setItems:barItems animated:YES];
    _txtPhoneNo.inputAccessoryView = _toolBar;

}



- (void) notificationReceivedEmail:(NSNotification *) notification
{
    
    [self callWebServiceForVerificationInfo];
//    if ([[notification name] isEqualToString:@"VerifiedEmailPushRecived"])
//    {    NSLog (@"Successfully received the test notification!");
//
//
//    }
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationReceivedEmail:) name:@"VerifiedEmailPushRecived" object:nil];

    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.title = @"Verify Your Account";

}
-(void)updateTextField:(id)sender {
    [_txtPhoneNo resignFirstResponder];
}


#pragma mark  Custom Method
-(void) facebookConnect
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in");
             
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name"}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error) {
                      NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
                      [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"FBConnect"];
                      [_btnFBConnect setTitle:@"Connected" forState:UIControlStateNormal];
                  }
                  else{
                    //  [HUD hideAnimated:YES];
                      [loader stopAnimating];
                      
                  }
              }];
         }
     }];
}

- (void) linkedIn
{
    NSArray *permissions = [NSArray arrayWithObjects:LISDK_BASIC_PROFILE_PERMISSION, nil];
    
    [LISDKSessionManager createSessionWithAuth:permissions state:nil showGoToAppStoreDialog:YES successBlock:^(NSString *returnState){
        NSLog(@"%s","success called!");
        [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"LinkedInConnect"];
        [_btnLinkedInConnect setTitle:@"Connected" forState:UIControlStateNormal];

        LISDKSession *session = [[LISDKSessionManager sharedInstance] session];
        NSLog(@"Session  : %@", session.description);
        
        [[LISDKAPIHelper sharedInstance] getRequest:@"https://api.linkedin.com/v1/people/~"
                                            success:^(LISDKAPIResponse *response) {
                                                
                                                NSData* data = [response.data dataUsingEncoding:NSUTF8StringEncoding];
                                                NSDictionary *dictResponse = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                                                
                                                NSString *authUsername = [NSString stringWithFormat: @"%@ %@", [dictResponse valueForKey: @"firstName"], [dictResponse valueForKey: @"lastName"]];
                                                NSLog(@"Authenticated user name  : %@", authUsername);

                                                
                                            } error:^(LISDKAPIError *apiError) {
                                                NSLog(@"Error  : %@", apiError);
                                            }];
    } errorBlock:^(NSError *error) {
        NSLog(@"Error called  : %@", error);
    }];
}

#pragma mark  IBAction Method
- (IBAction) btnLinkedInAction:(id)sender {
    [self linkedIn];
}

- (IBAction) btnFacebookAction:(id)sender {
    [self facebookConnect];
}

- (IBAction) btnEmailVerifyAction:(id)sender {

    if((([self.txtEmail.text isEqualToString:@""]))){
        
        [self validationErrorAlert:@"Please fill e-mail address"];
        
    }
    else if(![self NSStringIsValidEmail:self.txtEmail.text]){
        
        [self validationErrorAlert:@"Invalid e-mail address!"];
        
    }
    else{
//        HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//        HUD.bezelView.color = [UIColor blackColor];
//        HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//        HUD.contentColor = [UIColor whiteColor];

        loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
        loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
        
        [self.view addSubview:loader];
        [loader startAnimating];
        
        [self callWebServiceForVerifyEmail];
    }
}

- (IBAction) btnGovIDVerifyAction:(id)sender {
    
//    VerifyAccountViewController *VerifyVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyAccountViewController"];
//    [self.navigationController pushViewController:VerifyVC animated:YES];

        if ([self.btnFileUpload.titleLabel.text isEqual:@"Upload Image"]) {
            
            NSLog(@"Upload image web services");
            
//            HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//            HUD.bezelView.color = [UIColor blackColor];
//            HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//            HUD.contentColor = [UIColor whiteColor];
            
            loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
            loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
            
            [self.view addSubview:loader];
            [loader startAnimating];
            
            [self callWebServiceToUploadImage];
            
            
        }else{
            
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary"        preferredStyle:UIAlertControllerStyleActionSheet];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                {
                    [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
                }
                else
                {
                    [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"OK"];
                }
                
                [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
            
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
                if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
                {
                    UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
                    photoPicker.delegate = self;
                    photoPicker.allowsEditing = NO;
                    photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                    [self presentViewController:photoPicker animated:YES completion:NULL];
                }
                
            }]];
            
            [self presentViewController:actionSheet animated:YES completion:nil];
        }
    
}

#pragma mark  WebService Method
-(void) callWebServiceToUploadImage {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];
    
    NSString *str = [NSString stringWithFormat:@"%@api/varifyAccount",KBaseUrl];
    
    UIImage* smallImage = [_imgId.image scaleToSize:CGSizeMake(100.0f,100.0f)];

    
    //NSDictionary *dictParam = @{@"userId":loginUserID, @"verifiedId":[self encodeToBase64String: [UIImage imageNamed: @"step1.png"]] };
    
    NSDictionary *dictParam = @{@"userId":loginUserID, @"verifiedId": [self encodeToBase64String: smallImage]};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
        NSLog(@"%@",response);
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            [self validationErrorAlert:[response valueForKeyPath:@"message"]];
            
            [self callWebServiceForVerificationInfo];
//            [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
//            [self dismissViewControllerAnimated:NO completion:nil];
        }
    } failure:^(NSError *error) {
        
       // [HUD hideAnimated:YES];
        [loader stopAnimating];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }];
}

-(void) callWebServiceForVerifyEmail {
    
    NSString *str = [NSString stringWithFormat:@"%@api/verifyEmail",KBaseUrl];
    NSDictionary *dictParam = @{@"email":_txtEmail.text};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
        
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                        message:@"Verification link has been sent on your mail"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

            [self presentViewController:alert animated:YES completion:nil];
        }
    } failure:^(NSError *error) {
        
       // [HUD hideAnimated:YES];
        [loader stopAnimating];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
        
    }];
    
}

-(void) callWebServiceForVerificationInfo {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];

    NSString *str = [NSString stringWithFormat:@"%@api/getVerifiedInformation",KBaseUrl];
    
    NSDictionary *dictParam = @{@"userId":loginUserID};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        NSLog(@"response %@",response);
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            if (![[response valueForKeyPath:@"result.email"] isEqualToString:@"Yes"]) {
                _heightConstraintVerify.constant = 40;
            }
            else {
                _heightConstraintVerify.constant = 0;
            }
            
            if ([[response valueForKeyPath:@"result.goverment"] isEqualToString:@"Yes"]) {
                _txtGovID.text = @"Image Uploaded";
                _heightConstraintForImage.constant = 0;
                _heightForUploadButton.constant = 0;
                _verticalSpaceSelectImageButton.constant = 70;
                [_lblGovID setHidden:NO];
                [_lblLineGovId setHidden:NO];
                [_txtGovID setHidden:NO];
            }
            else {
                [_lblGovID setHidden:YES];
                [_lblLineGovId setHidden:YES];
                [_txtGovID setHidden:YES];
                _heightConstraintForImage.constant = 150;
                _heightForUploadButton.constant = 40;
            }
            
            if ([[response valueForKeyPath:@"result.is_verified"] isEqualToString:@"Yes"]) {
                _txtGovID.text = @"Verified";
                _heightConstraintForImage.constant = 0;
                _heightForUploadButton.constant = 0;
                _verticalSpaceSelectImageButton.constant = 70;
                [_lblGovID setHidden:NO];
                [_lblLineGovId setHidden:NO];
                [_txtGovID setHidden:NO];
            }
        }
    } failure:^(NSError *error) {
      //  [HUD hideAnimated:YES];
        [loader stopAnimating];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        //NSLog(error);
    }];
}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

#pragma mark  Alert Prompt Method
-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Image Picker Select Image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.tabBarController.tabBar setHidden:NO];
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    self.imgId.image = chosenImage;
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.imgId.image, nil, nil, nil);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.btnFileUpload setTitle:@"Upload Image" forState:UIControlStateNormal];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}



-(void)showcamera
{
    
    [self.tabBarController.tabBar setHidden:YES];
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];
    }
    else
    {
        [self camDenied];
    }
}
#pragma mark  Pop Camera
-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:cameraPicker animated:YES completion:nil];
}
#pragma mark  Cam Denied
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
        }
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
