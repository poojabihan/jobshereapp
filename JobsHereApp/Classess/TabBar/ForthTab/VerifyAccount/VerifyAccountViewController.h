//
//  VerifyAccountViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 29/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VerifyAccountViewController : UIViewController



@property (strong, nonatomic) IBOutlet UIImageView *ImageView;
@property (strong, nonatomic) IBOutlet UIButton *SelectButtonInstance;
- (IBAction)SelectAndUploadImage:(id)sender;

@end
