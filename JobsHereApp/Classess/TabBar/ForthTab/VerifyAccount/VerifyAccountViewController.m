//
//  VerifyAccountViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 29/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "VerifyAccountViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "Webservice.h"
#import <MBProgressHUD.h>


@interface VerifyAccountViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
{
    NSString *imgString;UIImage *chosenImage;
    MBProgressHUD * HUD;
}
@end

@implementation VerifyAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark  Show navigation Bar

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    [self.tabBarController setHidesBottomBarWhenPushed:YES];
    
    self.title = @"Upload Image";
}

- (IBAction)SelectAndUploadImage:(id)sender {

    
    if ([self.SelectButtonInstance.titleLabel.text isEqual:@"Upload Image"]) {
      
        NSLog(@"Upload image web services");
        
        HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.bezelView.color = [UIColor blackColor];
        HUD.bezelView.style = MBProgressHUDModeIndeterminate;
        HUD.contentColor = [UIColor whiteColor];
        
        [self callWebServiceToUploadImage];
        
      
    }else{
            
            UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary"        preferredStyle:UIAlertControllerStyleActionSheet];
        
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
                    [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
            {
                [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
            }
            else
            {
                [self errorAlertWithTitle:@"Camera not found" message:@"This Device has no Camera" actionTitle:@"OK"];
            }
            
            [self dismissViewControllerAnimated:YES completion:^{
                }];
            }]];
        
            [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
            {
                UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
                photoPicker.delegate = self;
                photoPicker.allowsEditing = NO;
                photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:photoPicker animated:YES completion:NULL];
            }
                
        }]];
        
        [self presentViewController:actionSheet animated:YES completion:nil];
    }
}

#pragma mark  Show Camera

-(void)showcamera
{
    
    [self.tabBarController.tabBar setHidden:YES];
    
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];
    }
    else
    {
        [self camDenied];
    }
}
#pragma mark  Pop Camera
-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    [self presentViewController:cameraPicker animated:YES completion:nil];
}
#pragma mark  Cam Denied
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
        }
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}
#pragma mark  Image Picker Select Image
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    [self.tabBarController.tabBar setHidden:NO];
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    self.ImageView.image = chosenImage;
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(self.ImageView.image, nil, nil, nil);
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.SelectButtonInstance setTitle:@"Upload Image" forState:UIControlStateNormal];
        [self dismissViewControllerAnimated:YES completion:nil];
    });
}

- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

#pragma mark  Error Alert
-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}

#pragma mark  WebService Method
-(void) callWebServiceToUploadImage {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];

    NSString *str = [NSString stringWithFormat:@"%@api/varifyAccount",KBaseUrl];
    NSDictionary *dictParam = @{@"userId":loginUserID, @"verifiedId":[self encodeToBase64String:chosenImage] };
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        [HUD hideAnimated:YES];

        NSLog(@"%@",response);
        if ([[response objectForKey:@"status"] boolValue] == 1) {
            
            [self validationErrorAlert:[response valueForKeyPath:@"message"]];

             [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
            [self dismissViewControllerAnimated:NO completion:nil];


        }
    } failure:^(NSError *error) {
        
        [HUD hideAnimated:YES];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }];
    
}

#pragma mark  Alert Prompt Method
-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}


@end
