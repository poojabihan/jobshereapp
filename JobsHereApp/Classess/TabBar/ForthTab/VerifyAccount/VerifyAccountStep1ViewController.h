//
//  VerifyAccountStep1ViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 18/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//



#import <UIKit/UIKit.h>

@interface VerifyAccountStep1ViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIButton *btnVerifyEmail;
@property (strong, nonatomic) IBOutlet UITextField *txtEmail;
@property (strong, nonatomic) IBOutlet UILabel *txtGovID;
@property (strong, nonatomic) IBOutlet UITextField *txtPhoneNo;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpaceSelectImageButton;

@property (strong, nonatomic) IBOutlet UIView *emailView;
@property (strong, nonatomic) IBOutlet UIView *phoneView;
@property (strong, nonatomic) IBOutlet UIView *governmentView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintVerify;
@property (weak, nonatomic) IBOutlet UIButton *btnFileUpload;

@property (strong, nonatomic) IBOutlet UIButton *btnFBConnect;
@property (strong, nonatomic) IBOutlet UIButton *btnLinkedInConnect;
@property (weak, nonatomic) IBOutlet UIImageView *imgId;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintForImage;
@property (weak, nonatomic) IBOutlet UILabel *lblHeightForGvtStatus;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *heightForUploadButton;

@end
