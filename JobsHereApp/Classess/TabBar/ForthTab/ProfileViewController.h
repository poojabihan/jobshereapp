//
//  ProfileViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *TableView;

@property (strong, nonatomic) IBOutlet UILabel *FullNameLabel;

@property (strong, nonatomic) IBOutlet UILabel *EmailLabel;

@property (strong, nonatomic) IBOutlet UIImageView *ProfileImage;
- (IBAction)ProfileButton:(id)sender;
-(void)callingWebServiceForGetPluseButtonData;


@end
