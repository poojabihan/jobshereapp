//
//  MessageChatViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 29/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "MessageChatViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+Hexadecimal.h"
#import "UIImageView+WebCache.h"
#import "DGActivityIndicatorView.h"      

@interface MessageChatViewController ()
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubble;
@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubble;
@property (strong, nonatomic) JSQMessagesAvatarImage *incomingAvatar;
@property (strong, nonatomic) JSQMessagesAvatarImage *outgoingAvatar;
@property (strong, nonatomic) NSMutableArray * dictArray,*temp;

@property (atomic, assign) BOOL canceled;

@end

@implementation MessageChatViewController
{
    NSDictionary * countDictionary;
    NSTimer* timer;
    NSArray *resultArray;
   // MBProgressHUD *HUD ;
    DGActivityIndicatorView *loader;

    UIImage *chosenImage;
}
- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    // Do any additional setup after loading the view.
    NSLog(@"change it id view did load %@",self.UserID);
    NSLog(@"Job Id %@",self.JobID);
    self.tabBarController.tabBar.hidden = YES;
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationItem setTitle:self.NavigationTitle];
    [self.inputToolbar.contentView.textView setKeyboardType:UIKeyboardTypeASCIICapable];
    self.inputToolbar.contentView.leftBarButtonItem.hidden = YES;
    
    //self.view.backgroundColor = [UIColor.grayColor];
    
    self.view.backgroundColor = [UIColor colorWithHex:@"3ab44b"];
    
    //self.inputToolbar.contentView.leftBarButtonItem = nil;
    //AppDelegate *app=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    
    // ① Own senderId, senderDisplayName The setting
    
    self.senderId = @"User";
    self.senderDisplayName = @"User";
    // ② MessageBubble (Balloon background) The setting
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [JSQMessagesBubbleImageFactory new];
    
    //self.incomingBubble = [bubbleFactory  incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubble=[bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0]];
    
    //self.outgoingBubble = [bubbleFactory  outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
    
    self.outgoingBubble = [bubbleFactory outgoingMessagesBubbleImageWithColor: [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0]/*[UIColor blueColor]*/];
    
    //set initial names of user
//    self.incomingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"PW" backgroundColor:[UIColor colorWithHex:@"3ab44b"] textColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14.0f] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//
//    self.outgoingAvatar=[JSQMessagesAvatarImageFactory avatarImageWithUserInitials:@"DA" backgroundColor:[UIColor colorWithHex:@"3ab44b"] textColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14.0f] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    // ③ Set an avatar image
    
    //  self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"BannerImage"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    //  self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"Twitter"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
    
    // ④ Initialize an array of message data
        self.messages = [NSMutableArray array];
        [self loadEarlierMessages];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
}
#pragma mark  Show navigation Bar
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:(BOOL)animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:(BOOL)animated];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
    }
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        timer = [NSTimer scheduledTimerWithTimeInterval:5 target: self selector: @selector(loadEarlierMessages) userInfo:nil repeats: YES];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    self.tabBarController.tabBar.hidden = NO;
    [super viewWillDisappear:animated];
    // Enable iOS 7 back gesture
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = YES;
        self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    [timer invalidate];
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    return NO;
}

#pragma mark GetMessages Method

-(void)loadEarlierMessages
{
    self.temp = [NSMutableArray array];
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if(internetStatus != NotReachable)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            //Do background work
            self.dictArray = [[NSMutableArray alloc]init];
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            //NSString *emailIdString = [standardUserDefaults stringForKey:@"EmailID"];
            NSString *LoginUser = [standardUserDefaults stringForKey:@"UserID"];
            
            /*New*/
            NSDictionary *dictParam = @{@"userId":LoginUser,@"chatFrom":self.UserID,@"jobId":_JobID};

            //NSDictionary *dictParam = @{@"userId":LoginUser,@"chatFrom":self.UserID,@"jobId":_JobID};
            
            self.URLString=[NSString stringWithFormat:@"%@api/getSingleChat",KBaseUrl];
            
            [Webservice requestPostUrl:self.URLString parameters:dictParam success:^(NSDictionary *response) {
                NSLog(@"Response :%@",response);
                
               // [HUD hideAnimated:YES];
                [loader stopAnimating];
                if ([response objectForKey:@"response"]) {

                    self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"man"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
                    self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"man"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];

//                    if ([[response objectForKey:@"sendfromImage"] isEqualToString:@""]) {
//                        self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"man"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//                    }
//                    else{
//
//                        NSString *imgURLString = [NSString stringWithFormat:@"%@%@",PostedJobsUrl,[response objectForKey:@"sendfromImage"]];
//                        NSURL *url = [NSURL URLWithString:imgURLString];
//                        NSData *data = [NSData dataWithContentsOfURL:url];
//                        UIImage *dataLoadedImg = [UIImage imageWithData:data];
//
//                        JSQMessagesAvatarImage *img = [JSQMessagesAvatarImageFactory avatarImageWithImage:dataLoadedImg diameter:20.0f];
//                        self.outgoingAvatar = img;
//
////                        self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage image] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//                    }
//
//                    if ([[response objectForKey:@"sendtoName"] isEqualToString:@""]) {
//                        self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"man"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//                    }
                    

                        
//    [cell.imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PostedJobsUrl,[[appliedJobArrayDetails valueForKey:@"images"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"Backgroundimage"]];

//                    self.incomingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"BannerImage"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
//                    self.outgoingAvatar = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"Twitter"] diameter:kJSQMessagesCollectionViewAvatarSizeDefault];

                    
                    resultArray = [response objectForKey:@"response"];
                    NSLog(@"Number of elements in array = %lu", (unsigned long)[resultArray count]);
                    [self.temp removeAllObjects];
                    for (NSDictionary* msg in resultArray) {
                        [self.temp addObject:[self JSQMessageFromData:msg]];
                    }
                    if (self.messages.count<self.temp.count) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            //[self.messages removeAllObjects];
                            NSLog(@"self.messages count %lu and temp count %lu",(unsigned long)self.messages.count,(unsigned long)self.temp.count);
                            self.messages=self.temp;
                            [self.collectionView reloadData];
                            [self finishReceivingMessageAnimated:YES];
                        });
                    }
                }
                else{
                }
            } failure:^(NSError *error) {
                NSLog(@"Error: %@", error);
                [self errorAlertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK"];
               // [HUD hideAnimated:YES];
                [loader stopAnimating];
            }];
        });
    }else{
        
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:kInternetOff preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"Dissmiss" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alert addAction:ok];
        alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alert animated:true completion:nil];
        });
        
    }
    
}

- (JSQMessage *) JSQMessageFromData:(NSDictionary*)msg {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    
    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    
    NSDate *date =[dateFormat dateFromString:[NSString stringWithFormat:@"%@",[msg objectForKey:@"createdAt"]]];
    
    return [[JSQMessage alloc]initWithSenderId:[msg objectForKey:@"user_type"] senderDisplayName:[msg objectForKey:@"user_type"] date:date text:[msg objectForKey:@"message"]];
    
}

#pragma mark - Send Message

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    if(internetStatus != NotReachable)
    {
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        
        NSString *emailIdString = [standardUserDefaults stringForKey:@"EmailID"];
        NSLog(@"email id %@",emailIdString);
        
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        NSLog(@"email id %@",LoginUserID);
        
        NSDictionary *dictParam = @{@"sendTo":self.UserID,@"email_id":emailIdString,@"message":text,@"post_type":@"msg",@"sendFrom":LoginUserID,@"job_id":self.JobID};
        
        NSLog(@"Params %@",dictParam);

        self.URLString=[NSString stringWithFormat:@"%@api/addMessage",KBaseUrl];
        
        [Webservice requestPostUrl:self.URLString parameters:dictParam success:^(NSDictionary *response) {
            NSLog(@"Response : %@",response);
            NSMutableDictionary *Ms = [[NSMutableDictionary alloc] init];
            Ms = [[response valueForKey:@"response"] objectAtIndex:0];
            if (response == NULL) {
                [self errorAlertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK"];
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    //User
                    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                    [dateFormat setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                    //[dateFormat setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                    //NSDate *date = [dateFormat dateFromString:[NSString stringWithFormat:@"%@",[Ms objectForKey:@"created_date"]]];
                    [JSQSystemSoundPlayer jsq_playMessageSentSound];
                    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:[Ms objectForKey:@"user_type"] senderDisplayName:[Ms objectForKey:@"user_type"] date:date  text:[Ms objectForKey:@"message"]];
                    NSLog(@"Message %@",message);
                    [self.messages addObject:message];
                    [self finishSendingMessageAnimated:YES];
                });
            }
        }failure:^(NSError *error) {
            NSLog(@"Error: %@", error);
            [self errorAlertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK"];
        }];
    }else{
        [self errorAlertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK"];
    }
}
#pragma mark - JSQMessagesCollectionViewDataSource
- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
    
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
    
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messages objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

-(NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
}
// ④Return a message data that refer to each item
- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messages objectAtIndex:indexPath.item];
}
// ② Of each item MessageBubble (background) return it
- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingBubble;
    }
    return self.incomingBubble;
}
// ③ Return the avatar image of each item
- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messages objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return self.outgoingAvatar;
    }
    return self.incomingAvatar;
}


-(UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    JSQMessage *msg = [self.messages objectAtIndex:indexPath.item];
    
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            
            cell.textView.textColor = [UIColor whiteColor];
        }
        else {
            
            cell.textView.textColor = [UIColor blackColor];
        }
    }
    
   
    return cell;
}
#pragma mark - UICollectionViewDataSource

// ④It returns the total number of items
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.messages.count;
}


#pragma mark - sendingImage methods

- (void)didPressAccessoryButton:(UIButton *)sender
{
    
//    Reachability *reachability = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
//    if(internetStatus != NotReachable)
//    {
//        [self.inputToolbar.contentView.textView resignFirstResponder];
//
//        UIAlertController *actionSheet = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Please, Pick Photo from Camera or PhotoLibrary" preferredStyle:UIAlertControllerStyleActionSheet];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action) {
//            [self.inputToolbar.contentView.textView becomeFirstResponder];
//            // Cancel button tappped.
//            [self dismissViewControllerAnimated:YES completion:^{
//            }];
//        }]];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
//            {
//
//                [self performSelector:@selector(showcamera) withObject:nil afterDelay:0.0];
//
//            }
//
//            else
//
//            {
//
//                [self errorAlertWithTitle:@"Camera Not Found" message:@"This Device has no Camera" actionTitle:@"Dismiss"];
//
//            }
//            // Distructive button tapped.
//            [self dismissViewControllerAnimated:YES completion:^{
//            }];
//        }]];
//
//        [actionSheet addAction:[UIAlertAction actionWithTitle:@"Photo Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
//
//            // OK button tapped.
//            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
//            {
//
//                UIImagePickerController *photoPicker = [[UIImagePickerController alloc] init];
//                photoPicker.delegate = self;
//                photoPicker.allowsEditing = NO;
//                photoPicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
//
//                [self presentViewController:photoPicker animated:YES completion:NULL];
//            }
//
//        }]];
//        [self presentViewController:actionSheet animated:YES completion:nil];
//    }
//    else
//    {
//        [self errorAlertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"Dismiss"];
//    }
}
-(void)showcamera
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if(authStatus == AVAuthorizationStatusAuthorized)
    {
        [self popCamera];
    }
    else if(authStatus == AVAuthorizationStatusNotDetermined)
    {
        NSLog(@"%@", @"Camera access not determined. Ask for permission.");
        
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted)
         {
             if(granted)
             {
                 NSLog(@"Granted access to %@", AVMediaTypeVideo);
                 [self popCamera];
             }
             else
             {
                 NSLog(@"Not granted access to %@", AVMediaTypeVideo);
                 [self camDenied];
             }
         }];
    }
    else if (authStatus == AVAuthorizationStatusRestricted)
    {
        [self errorAlertWithTitle:kAppNameAlert message:@"You've been restricted from using the camera on this device. Without camera access this feature won't work. Please contact the device owner so they can give you access." actionTitle:@"OK"];
    }
    else
    {
        [self camDenied];
    }
    
}
-(void)popCamera{
    UIImagePickerController * cameraPicker = [[UIImagePickerController alloc] init];
    
    cameraPicker.delegate = self;
    cameraPicker.allowsEditing = NO;
    cameraPicker.modalPresentationStyle = UIModalPresentationCurrentContext;
    cameraPicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:cameraPicker animated:YES completion:nil];
    
}
- (void)camDenied
{
    NSLog(@"%@", @"Denied camera access");
    
    UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Error" message:@"It looks like your privacy settings are preventing us from accessing your camera to take Photos. You can fix this by doing the following:\n\n1. Touch the Settings button below to open the Settings of this app.\n\n2. Turn the Camera on.\n\n3. Open this app and try again." preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        @try
        {
            NSLog(@"tapped Settings");
            BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
            if (canOpenSettings)
            {
                NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                [[UIApplication sharedApplication] openURL:url options:@{} completionHandler:nil];
            }
        }
        @catch (NSException *exception)
        {
            
        }
        
    }];
    UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
    
    [alert addAction:settingsAction];
    [alert addAction:cancelAction];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    chosenImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
  
    if (picker.sourceType == UIImagePickerControllerSourceTypeCamera)
    {
        UIImageWriteToSavedPhotosAlbum(chosenImage, nil, nil, nil);
    }
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self dismissViewControllerAnimated:YES completion:nil];
    });
    
}
- (void)doSomeWorkWithProgress {
    self.canceled = NO;
    // This just increases the progress indicator in a loop.
    float progress = 0.0f;
    while (progress < 1.0f) {
        if (self.canceled) break;
        progress += 0.01f;
        dispatch_async(dispatch_get_main_queue(), ^{
            // Instead we could have also passed a reference to the HUD
            // to the HUD to myProgressTask as a method parameter.
            
            //puchna hai...
            [MBProgressHUD HUDForView:self.navigationController.view].progress = progress;
        });
        usleep(50000);
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Navigation Segue

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Bubble tap");
}

-(void)errorAlertWithTitle:(NSString *)titleName message:(NSString *)message actionTitle:(NSString *)actionName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:UIAlertActionStyleCancel handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
}

@end
