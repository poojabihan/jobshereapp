//
//  MessageChatViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 29/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

#import <JSQMessagesViewController/JSQMessagesViewController.h>
#import <JSQMessagesViewController/JSQMessages.h>
#import "Reachability.h"
#import "Webservice.h"


@interface MessageChatViewController : JSQMessagesViewController<JSQMessagesCollectionViewDataSource,JSQMessagesCollectionViewDelegateFlowLayout,UITextViewDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate>


@property NSString *UserID;
@property NSString *JobID;
@property NSString *NavigationTitle;
@property NSString * URLString;



@end
