//
//  JobAppliedView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 21/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobAppliedView.h"
#import "JobAppliedTableViewCell.h"
#import "Webservice.h"
#import "UIColor+Hexadecimal.h"
#import "Reachability.h"
#import "UIImageView+WebCache.h"
//#import <MBProgressHUD.h>
#import "JobsDetailsViewController.h"
#import "DGActivityIndicatorView.h"


@interface JobAppliedView ()
    
    {
        NSMutableArray *appliedJobArrayDetails;
        DGActivityIndicatorView *loader;

       // MBProgressHUD * HUD;
    }
@property (strong, nonatomic) IBOutlet UITableView *appliedJobsTableView;

@end

@implementation JobAppliedView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.appliedJobsTableView setSeparatorColor:[UIColor colorWithHex:@"00b050"]];
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    // Do any additional setup after loading the view.
    
    [self.appliedJobsTableView setTableFooterView:[UIView new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.title = @"Applied Jobs";

    dispatch_async(dispatch_get_main_queue(), ^{
        [self callingWebServiceForGetAppliedJobsList];
    });
}

#pragma mark  Calling Api


-(void)callingWebServiceForGetAppliedJobsList{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        
        NSDictionary *dictParam = @{@"userId":LoginUserID};
        
        NSString *submitFeedback=[NSString stringWithFormat:@"%@api/getAppliedJob",KBaseUrl];
        
        [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
            
            NSLog(@"Response %@",response);
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                appliedJobArrayDetails = [response valueForKey:@"response"];
                
              //  [HUD hideAnimated:YES];
                [loader stopAnimating];
                [_appliedJobsTableView reloadData];
                
                NSLog(@"HEHHEHEHEHEHEH %@",appliedJobArrayDetails);
                
                for (int i = 0; i<appliedJobArrayDetails.count; i++) {
                    
                    NSLog(@"%@", appliedJobArrayDetails[i]);
                    
                }
                
                
            }else if (response==NULL) {
                
                [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
               // [HUD hideAnimated:YES];
                [loader stopAnimating];
                NSLog(@"response is null");
            }else{
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            }
        }
                           failure:^(NSError *error) {
                             //  [HUD hideAnimated:YES];
                               [loader stopAnimating];

                               [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                               NSLog(@"Error %@",error);
                           }];
    }else{
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
   
    return appliedJobArrayDetails.count;
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    JobAppliedTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"JobAppliedTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    cell.imgProfile.layer.cornerRadius = cell.imgProfile.frame.size.height / 2;
    cell.imgProfile.layer.masksToBounds=YES;
    cell.backgroundColor=[UIColor clearColor];
    
    
    cell.lblUserName.text = [NSString stringWithFormat:@"%@",[[appliedJobArrayDetails valueForKey:@"jobTitle"]objectAtIndex:indexPath.row]];

    cell.lblDateNTime.text = [NSString stringWithFormat:@"%@",[[appliedJobArrayDetails valueForKey:@"appliedDate"]objectAtIndex:indexPath.row]];

    [cell.imgProfile setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",PostedJobsUrl,[[appliedJobArrayDetails valueForKey:@"images"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];
    cell.lblStatus.text = [NSString stringWithFormat:@"%@",[[appliedJobArrayDetails valueForKey:@"jobStatus"]objectAtIndex:indexPath.row]];

    if ([[NSString stringWithFormat:@"%@",[[appliedJobArrayDetails valueForKey:@"jobStatus"]objectAtIndex:indexPath.row]] isEqualToString:@"Pending"]) {
        
        [cell.imgStatus setBackgroundColor:[UIColor colorWithHex:@"#f1a409"]];
    }
    else if ([[NSString stringWithFormat:@"%@",[[appliedJobArrayDetails valueForKey:@"jobStatus"]objectAtIndex:indexPath.row]] isEqualToString:@"Accepted"]) {
        
        [cell.imgStatus setBackgroundColor:[UIColor colorWithHex:@"#029c0a"]];
    }
    else {
        [cell.imgStatus setBackgroundColor:[UIColor colorWithHex:@"#ff060e"]];
    }
    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    JobsDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
    JobVC.isFromJobApplied = YES;
    JobVC.JobData = [appliedJobArrayDetails objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:JobVC animated:YES];

}

@end
