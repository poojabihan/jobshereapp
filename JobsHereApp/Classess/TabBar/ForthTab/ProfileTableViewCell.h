//
//  ProfileTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 19/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UIImageView *MenuIconImage;

@property (strong, nonatomic) IBOutlet UILabel *MenuTitleText;

@end
