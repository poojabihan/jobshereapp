//
//  FeedbackView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FeedbackView : UIViewController
@property (strong, nonatomic) IBOutlet UITextView *feedbackTextView;

@property (strong, nonatomic) IBOutlet UIView *bgView;

@end
