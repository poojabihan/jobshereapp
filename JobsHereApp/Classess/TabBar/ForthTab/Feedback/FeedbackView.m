//
//  FeedbackView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "FeedbackView.h"
#import "UIColor+Hexadecimal.h"
//#import <SVProgressHUD.h>
#import "Reachability.h"
#import "Webservice.h"
#import "DGActivityIndicatorView.h"

@interface FeedbackView ()
{
    NSUserDefaults *Store;
    DGActivityIndicatorView *loader;

}

@property (strong, nonatomic) IBOutlet UIView *feedbackView;

@end

@implementation FeedbackView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    
    _feedbackView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _feedbackView.layer.shadowOffset = CGSizeMake(0, 0);
    _feedbackView.layer.shadowOpacity = 1;
    _feedbackView.layer.shadowRadius = 1.0;
    _feedbackView.layer.masksToBounds = NO;
    
    self.feedbackTextView.layer.borderWidth = 2;
    self.feedbackTextView.layer.borderColor = [UIColor grayColor].CGColor;
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimissView)];
    [self.bgView addGestureRecognizer:tap];
}

- (void) dimissView {
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark  Action
- (IBAction)cancelButtonAction:(id)sender {
    
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (IBAction)sendButtonAction:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if([self.feedbackTextView.text isEqualToString:@""] ){
            
            [self validationErrorAlert:@"Please enter message"];
            
        }
        else if(self.feedbackTextView.text.length < 1){
            
            [self validationErrorAlert:@"Please enter proper message"];
            
        }
        else{
            
//            [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
//            [SVProgressHUD show];
            //[SVProgressHUD showWithStatus:@"Loading"];
            
            loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
            loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
            
            [self.view addSubview:loader];
            [loader startAnimating];
            
            NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
            NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
            
            NSDictionary *dictParam = @{@"userId":LoginUserID,@"message": self.feedbackTextView.text};
            
            NSString *submitFeedback=[NSString stringWithFormat:@"%@api/submitFeedback",KBaseUrl];
            
            [Webservice requestPostUrl:submitFeedback parameters:dictParam success:^(NSDictionary *response){
                
                NSLog(@"Response %@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    [loader stopAnimating];

                    [self validationErrorAlert:@"Feedback submitted successfully."];
                    
                    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
                    [self dismissViewControllerAnimated:NO completion:nil];

                    
                }else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                      //  [SVProgressHUD dismiss];
                        [loader stopAnimating];
                        
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[SVProgressHUD dismiss];
                        [loader stopAnimating];

                    });
                    [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[SVProgressHUD dismiss];
                    [loader stopAnimating];

                });
                NSLog(@"Error %@",error);
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

            }];
            
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    
}

#pragma mark  Alert Prompt Method
-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}
#pragma mark  TextView Delegate

- (BOOL)textView:(UITextView *)txtView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if( [text rangeOfCharacterFromSet:[NSCharacterSet newlineCharacterSet]].location == NSNotFound ) {
        return YES;
    }
    
    [txtView resignFirstResponder];
    return NO;
}



@end
