//
//  JobViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobViewController.h"

@interface JobViewController ()
@property (strong, nonatomic) IBOutlet UILabel *iLikeItLbl;

@end

@implementation JobViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
   
    _iLikeItLbl.layer.cornerRadius = 22;

    _iLikeItLbl.layer.borderColor =[[UIColor colorWithRed:0.0f/255.0f green:176.0f/255.0f blue:80.0f/255.0f alpha:1.0] CGColor];

    _iLikeItLbl.layer.borderWidth = 1;
    
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
}


@end
