//
//  DoJobConfirmView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 16/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "DoJobConfirmView.h"

@interface DoJobConfirmView ()
@property (strong, nonatomic) IBOutlet UIView *goodLuckView;
@property (strong, nonatomic) IBOutlet UIView *bgView;

@end

@implementation DoJobConfirmView

- (void)viewDidLoad {
    [super viewDidLoad];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    _goodLuckView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _goodLuckView.layer.shadowOffset = CGSizeMake(0, 0);
    _goodLuckView.layer.shadowOpacity = 1;
    _goodLuckView.layer.shadowRadius = 1.0;
    _goodLuckView.layer.masksToBounds = NO;
    
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dimissView)];
//    [self.bgView addGestureRecognizer:tap];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnActionCancel:(id)sender {
    
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void) dimissView {
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];

}

@end
