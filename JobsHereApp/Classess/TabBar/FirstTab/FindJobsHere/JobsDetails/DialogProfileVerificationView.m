//
//  DialogProfileVerificationView.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "DialogProfileVerificationView.h"

@interface DialogProfileVerificationView ()

@property (strong, nonatomic) IBOutlet UIView *profileVerificationView;
@property (strong, nonatomic) IBOutlet UIButton *btnLinkProfileOutlet;

@end

@implementation DialogProfileVerificationView

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    _profileVerificationView.layer.shadowColor = [UIColor lightGrayColor].CGColor;
    _profileVerificationView.layer.shadowOffset = CGSizeMake(0, 0);
    _profileVerificationView.layer.shadowOpacity = 1;
    _profileVerificationView.layer.shadowRadius = 1.0;
    _profileVerificationView.layer.masksToBounds = NO;
    // Do any additional setup after loading the view.
}
- (IBAction)ActionToDismissTheView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil
     ];
    
}
- (IBAction)btnDismiss:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil
     ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)ActionToVerifyProfile:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];
    [_objJobs navigateToProfile];
}


@end
