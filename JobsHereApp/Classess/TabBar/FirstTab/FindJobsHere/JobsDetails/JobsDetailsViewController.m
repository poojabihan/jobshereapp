//
//  JobsDetailsViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 22/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobsDetailsViewController.h"
#import "UIColor+Hexadecimal.h"
#import "DoJobConfirmView.h"
#import "JobsImageViewCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "MessageChatViewController.h"
#import "AppDelegate.h"
#import "DialogProfileVerificationView.h"
#import "VerifyAccountStep1ViewController.h"

@interface JobsDetailsViewController ()
{
    NSMutableArray *JobImageArray;
    NSString *LoginUserID;
    NSString *jobID;
    NSMutableArray *responseArray;
    NSString *userID;

    
}

@end

@implementation JobsDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"--%@",self.JobData);
    self.JobDetails.layer.borderWidth=1;
    self.JobDetails.layer.borderColor=[UIColor colorWithHex:@"00b050"].CGColor;
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.Pricelbl.text=[NSString stringWithFormat:@"£%@",[self.JobData valueForKey:@"cost"]];
        self.JobTitle.text=[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"jobTitle"]];
        self.JobDescription.text=[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"jobDescription"]];
        
        jobID = [self.JobData valueForKey:@"id"] == nil ? [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"jobId"]] : [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"id"]];
        userID = [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"userId"]];
        
        NSLog(@"here is the jobId%@", jobID);
        
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        LoginUserID = [standardUserDefaults stringForKey:@"UserID"];
        if ([LoginUserID isEqualToString:[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"userId"]]]) {
            [self.AsqQuestionButtonInstance setHidden:YES];
            [self.DoJobsHereButtonInstance setHidden:YES];
        }
        JobImageArray = [self.JobData valueForKey:@"image"] == nil ? [[self.JobData valueForKey:@"images"] componentsSeparatedByString:@","] : [self.JobData valueForKey:@"image"];
        [self.CollectionView reloadData];
        
        [self webserviceCallDoJobNotificationStatus];

    });
    
    

}
-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.title = @"Job Detail";
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    if (self.isFromJobApplied) {
        _heightConstraintDoJobHereButton.constant = 0.0;
        [self callWebServiceJobDetail];
        _btnAskForJobs.userInteractionEnabled = NO;
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

#pragma mark  Collection View Delegate
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [JobImageArray count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"JobsImageViewCollectionViewCell";
    JobsImageViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.backgroundColor=[UIColor whiteColor];
    [cell.jobimage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",JobImageBaseUrl,[JobImageArray  objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];
    return cell;
}

- (IBAction)AskQuetionButton:(id)sender {
    
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *accountVerified = [standardUserDefaults stringForKey:@"isVerified"];
    
    if ([accountVerified isEqualToString:@"No"]) {
        
        [self callWebServiceProfileDetail];
    }
    else{
        MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
        ChatVC.UserID = userID;//[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"userId"]];
        ChatVC.JobID = [self.JobData valueForKey:@"id"] == nil ? [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"jobId"]] : [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"id"]];
        
        ChatVC.NavigationTitle = [self.JobData valueForKey:@"fname"] == nil ? [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"postedName"]] : [NSString stringWithFormat:@"%@ %@",[self.JobData valueForKey:@"fname"],[self.JobData valueForKey:@"lname"]];
        [self.navigationController pushViewController:ChatVC animated:YES];
    }

}
- (IBAction)DoJobButton:(id)sender {
    
    [self webserviceCall];
}

-(void) callWebServiceProfileDetail {
    
    NSString *str = [NSString stringWithFormat:@"%@api/getUserProfile",KBaseUrl];
    
    NSDictionary *dictParam = @{@"userId":LoginUserID};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        if (([[response objectForKey:@"status"] boolValue] == 1)) {
            
            
            if ([[response valueForKeyPath:@"result.isVerified"] isEqualToString:@"No"]) {
                
                if ([[response valueForKeyPath:@"result.isDoc"] isEqualToString:@"No"]) {
                    //open Profile View
                    DialogProfileVerificationView *walkThru = [self.storyboard instantiateViewControllerWithIdentifier:@"DialogProfileVerificationView"];
                    walkThru.providesPresentationContextTransitionStyle = YES;
                    walkThru.definesPresentationContext = YES;
                    walkThru.objJobs = self;
                    [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
                    [self.tabBarController presentViewController:walkThru animated:NO completion:nil];
                }
                else{
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                                   message:@"Your account verification is in progress."
                                                                            preferredStyle:UIAlertControllerStyleAlert];

                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {}];

                    [alert addAction:defaultAction];
                    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                    [self presentViewController:alert animated:YES completion:nil];

                }
            }
            else{
                MessageChatViewController * ChatVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MessageChatViewController"];
                ChatVC.UserID = userID;
                ChatVC.JobID = [self.JobData valueForKey:@"id"] == nil ? [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"jobId"]] : [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"id"]];//[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"id"]];
                [self.navigationController pushViewController:ChatVC animated:YES];
            }
        }
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

    }];
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


-(void) callWebServiceJobDetail {
    
    NSString *str = [NSString stringWithFormat:@"%@api/getJobById",KBaseUrl];
    
    NSDictionary *dictParam = @{@"jobId":[self.JobData valueForKeyPath:@"jobId"]};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        NSLog(@"response+++ %@",response);

        if (([[response objectForKey:@"status"] boolValue] == 1)) {
            //[NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"userId"]]
            userID = [self.JobData valueForKey:@"userId"] == nil ? [NSString stringWithFormat:@"%@",[response valueForKeyPath:@"response.userId"]] : [NSString stringWithFormat:@"%@",[self.JobData valueForKey:@"userId"]];
            _btnAskForJobs.userInteractionEnabled = YES;

        }
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

    }];
    
}

-(void) webserviceCall {
    
    NSString *str = [NSString stringWithFormat:@"%@api/doJobNotification",KBaseUrl];
    
    NSDictionary *dictParam = @{@"userId":LoginUserID,@"jobId":jobID};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
            DoJobConfirmView *walkThru = [self.storyboard   instantiateViewControllerWithIdentifier:@"DoJobConfirmView"];
            walkThru.providesPresentationContextTransitionStyle = YES;
            walkThru.definesPresentationContext = YES;
            [walkThru setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            
            [self.tabBarController presentViewController:walkThru animated:NO completion:nil];
            
            [self webserviceCallDoJobNotificationStatus];
            
        }
        
        else{
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Message"
                                                                           message:@"You have already applied for this job"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

            [self presentViewController:alert animated:YES completion:nil];
            
        }
        
        
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

    }];
    
}

-(void) webserviceCallDoJobNotificationStatus {
    
    NSString *str = [NSString stringWithFormat:@"%@api/doJobNotificationStatus",KBaseUrl];
    
    NSDictionary *dictParam = @{@"userId":LoginUserID,@"jobId":jobID};
    
    NSLog(@"strMessage %@",dictParam);
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
      //  if (([[response objectForKey:@"status"] boolValue] == 1)) {
            
            if ([[response valueForKeyPath:@"response.isjobapplied"] isEqualToString:@"1"]) {
                //applied
                [self.DoJobsHereButtonInstance setTitle:@"Job Applied" forState:UIControlStateNormal];
                [self.DoJobsHereButtonInstance setUserInteractionEnabled:false];
                
            }
        /*}
        
        else{
            
            UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Message"
                                                                           message:@"You have already applied for this job"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                  handler:^(UIAlertAction * action) {}];
            
            [alert addAction:defaultAction];
            [self presentViewController:alert animated:YES completion:nil];
            
        }*/
        
        
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        
    }];
    
}

- (void)navigateToProfile {
    VerifyAccountStep1ViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerifyAccountStep1ViewController"];
    [self.navigationController pushViewController:JobVC animated:YES];
}

@end
