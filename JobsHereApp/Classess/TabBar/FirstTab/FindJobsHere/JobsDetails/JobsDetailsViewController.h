//
//  JobsDetailsViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 22/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *JobDetails;
@property (strong, nonatomic) IBOutlet UIView *ImageView;
@property (strong, nonatomic) IBOutlet UILabel *JobDescription;
@property (strong, nonatomic) IBOutlet UILabel *JobTitle;
@property (strong, nonatomic) IBOutlet UILabel *Pricelbl;
@property (strong, nonatomic) IBOutlet UIButton *btnAskForJobs;



@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;


@property (strong, nonatomic) NSMutableArray *JobData;
@property BOOL isFromJobApplied;

- (IBAction)AskQuetionButton:(id)sender;
- (IBAction)DoJobButton:(id)sender;
- (void) navigateToProfile;

@property (strong, nonatomic) IBOutlet UIButton *AsqQuestionButtonInstance;

@property (strong, nonatomic) IBOutlet UIButton *DoJobsHereButtonInstance;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraintDoJobHereButton;



@end
