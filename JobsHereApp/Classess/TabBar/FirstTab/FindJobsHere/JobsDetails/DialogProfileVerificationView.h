//
//  DialogProfileVerificationView.h
//  JobsHereApp
//
//  Created by Alok Mishra on 18/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobsDetailsViewController.h"

@interface DialogProfileVerificationView : UIViewController
@property (strong, nonatomic) JobsDetailsViewController *objJobs;

@end
