//
//  JobsImageViewCollectionViewCell.m
//  JobsHereApp
//
//  Created by Alok Mishra on 23/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobsImageViewCollectionViewCell.h"
#import "EXPhotoViewer.h"

@implementation JobsImageViewCollectionViewCell

- (IBAction)imgTapAction:(id)sender {
    [EXPhotoViewer showImageFrom:self.jobimage];
}

@end
