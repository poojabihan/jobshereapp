//
//  JobsImageViewCollectionViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 23/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JobsImageViewCollectionViewCell : UICollectionViewCell
@property (strong, nonatomic) IBOutlet UIImageView *jobimage;

@end
