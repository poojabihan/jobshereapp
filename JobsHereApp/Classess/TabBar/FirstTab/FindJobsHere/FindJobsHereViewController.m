//
//  FindJobsHereViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "FindJobsHereViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Webservice.h"
#import "Reachability.h"
//#import <MBProgressHUD.h>
#import <CoreLocation/CoreLocation.h>
#import "JobsDetailsViewController.h"
#import "customMapView.h"
#import "UIColor+Hexadecimal.h"
#import "jobsTableViewCell.h"
#import "MultiJobsViewController.h"
#import "DGActivityIndicatorView.h"

@interface FindJobsHereViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate>
{
    GMSMapView *_mapView;CLLocationManager *_manager;GMSMarker *_locationMarker;
    BOOL _firstLocationUpdate;
   // MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

    CLLocationManager *locationManager;
    NSString *latitude;NSString *longitude;GMSMarker *multipleMarker;NSMutableArray *JobArray;GMSMarker *marker;GMSMarker *_fadedMarker;
    NSMutableDictionary *selectedDict;
    NSMutableArray *multiJobsArray;
}
@property CLLocationCoordinate2D location;
@end

@implementation FindJobsHereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    JobArray = [[NSMutableArray alloc] init];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                            longitude:151.2086
                                                                 zoom:12];
    _mapView = [GMSMapView mapWithFrame:CGRectZero camera:camera];
    _mapView.settings.compassButton = YES;
    _mapView.settings.myLocationButton = YES;
    marker = [[GMSMarker alloc] init];
    _mapView.delegate=self;
    multipleMarker = [[GMSMarker alloc] init];
    // Listen to the myLocation property of GMSMapView.
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    self.view = _mapView;
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
    
#pragma mark - update current location
    
    locationManager = [CLLocationManager new];
    locationManager.delegate = self;
    locationManager.distanceFilter = 80.0;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice]systemVersion] intValue] >= 8)
    {
        if ([locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [locationManager requestWhenInUseAuthorization];
        }
    }
    [locationManager startUpdatingLocation];
    
    if (_showListView) {
        self.btnListView.hidden = NO;
        UIImage* image3 = [UIImage imageNamed:@"Listview"];
        CGRect frameimg = CGRectMake(15,5, 25,25);
        
        UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [someButton addTarget:self action:@selector(btnBackAction)
             forControlEvents:UIControlEventTouchUpInside];
        [someButton setShowsTouchWhenHighlighted:YES];
        
        UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
        self.navigationItem.rightBarButtonItem =mailbutton;

    }
    else {
        self.btnListView.hidden = YES;
    }
    
}

- (void)btnBackAction {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)dealloc {
    [_mapView removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}

- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
    }
}

-(void)locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *location = [locations lastObject];
    
    longitude = [[NSNumber numberWithDouble: location.coordinate.longitude] stringValue];
    latitude = [[NSNumber numberWithDouble: location.coordinate.latitude] stringValue];
    
    if (_locationMarker == nil) {
        _locationMarker = [[GMSMarker alloc] init];
        _locationMarker.position = location.coordinate;
        
        // Animated walker images derived from an www.angryanimator.com tutorial.
        // See: http://www.angryanimator.com/word/2010/11/26/tutorial-2-walk-cycle/
        
        NSArray *frames = @[[UIImage imageNamed:@"step1"],
                            [UIImage imageNamed:@"step2"],
                            [UIImage imageNamed:@"step3"],
                            [UIImage imageNamed:@"step4"],
                            [UIImage imageNamed:@"step5"],
                            [UIImage imageNamed:@"step6"],
                            [UIImage imageNamed:@"step7"],
                            [UIImage imageNamed:@"step8"]];
        
        _locationMarker.icon = [UIImage animatedImageWithImages:frames duration:0.8];
        _locationMarker.groundAnchor = CGPointMake(0.5f, 0.97f); // Taking into account walker's shadow
        _locationMarker.map = _mapView;
    } else {
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _locationMarker.position = location.coordinate;
        [CATransaction commit];
    }
    
    [self JobsNearYouWebServices];
    
    [locationManager stopUpdatingLocation];
    
    GMSCameraUpdate *move = [GMSCameraUpdate setTarget:location.coordinate zoom:12];
    [_mapView animateWithCameraUpdate:move];
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [alertCont addAction:okAction];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    [self fadeMarker:nil];
    
    marker.position = coordinate;
    marker.map=_mapView;
    _location=coordinate;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [((GMSMapView*)mapView) setSelectedMarker:marker];
    }];
    
    longitude = [[NSNumber numberWithDouble:coordinate.longitude] stringValue];
    latitude = [[NSNumber numberWithDouble:coordinate.latitude] stringValue];
   
    [self JobsNearYouWebServices];
}

-(void)JobsNearYouWebServices{
    
    dispatch_async(dispatch_get_main_queue(), ^{
       
        [_mapView clear];
        [JobArray removeAllObjects];
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        marker.position = CLLocationCoordinate2DMake([latitude floatValue], [longitude floatValue]);
        bounds = [bounds includingCoordinate:marker.position];
        marker.map=_mapView;
        
    });
    NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString *loginUserID = [standardUserDefaults stringForKey:@"UserID"];
    
    NSDictionary *dictParam = @{@"latitude":latitude,@"longitude":longitude, @"userId":loginUserID};
    
    NSString *GetJobs=[NSString stringWithFormat:@"%@api/getJobs",KBaseUrl];
    
    [Webservice requestPostUrl:GetJobs parameters:dictParam success:^(NSDictionary *response) {
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
       
            JobArray = [response mutableCopy];

            for (int i=0; i<[[[JobArray valueForKey:@"jobs"]valueForKey:@"latitude"] count]; i++) {
            
                double latField =[[[[JobArray valueForKey:@"jobs"]valueForKey:@"latitude"]objectAtIndex:i] doubleValue];
                double longField=[[[[JobArray valueForKey:@"jobs"] valueForKey:@"longitude"]objectAtIndex:i] doubleValue];
                
                multipleMarker = [[GMSMarker alloc] init];
                
                _location.latitude = latField;
                _location.longitude = longField;
                

                multipleMarker.position = CLLocationCoordinate2DMake(self.location.latitude, self.location.longitude);
//                multipleMarker.icon=[UIImage imageNamed:@"JobPins"];

                NSString *strPrice = @"";
                NSString *Title = @"";
                NSString *strDate = @"";
                customMapView *customView = [[[NSBundle mainBundle] loadNibNamed:@"customMapView" owner:self options:nil] firstObject];

                if ([[[[JobArray valueForKey:@"jobs"]valueForKey:@"count"]objectAtIndex:i] intValue] > 1) {
                    strPrice = [NSString stringWithFormat:@"%@ jobs",[[[JobArray valueForKey:@"jobs"]valueForKey:@"count"]objectAtIndex:i]];
                    Title = @"";
                    strDate = @"";
                    customView.lblPrize.textColor = customView.lblTitle.textColor;
                }
                else{
                    Title = [NSString stringWithFormat:@"%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"jobTitle"]objectAtIndex:i]];
                    strPrice = [NSString stringWithFormat:@"£%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"cost"]objectAtIndex:i]];
                    strDate = [NSString stringWithFormat:@"%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"postedDate"]objectAtIndex:i]];

                }
                
                


                customView.lblTitle.text = Title;
                customView.lblPrize.text = strPrice;
                customView.lblDate.text = strDate;
                
                multipleMarker.iconView = customView;
//                multipleMarker.icon = [self imageWithView:customView];
                
                NSLog(@"here is the Title for each marker %@", Title);
                
                //NSString *Message = [NSString stringWithFormat:@"%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"location"]objectAtIndex:i]];
                
                //multipleMarker.title = Title;
                
                
//                multipleMarker.appearAnimation = kGMSMarkerAnimationPop;
                multipleMarker.map = _mapView;
                
            }
         }
    } failure:^(NSError *error) {
        NSLog(@"Error %@",error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

    }];
}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {
    
//    [self fadeMarker:marker];
    
    for (int j=0; j<[[[JobArray valueForKey:@"jobs"]valueForKey:@"latitude"]count]; j++)
    {
        
        NSLog(@"job array count %lu",[[[JobArray valueForKey:@"jobs"]valueForKey:@"latitude"]count]);
        NSLog(@"j avlue %d",j);
        
       
        if ([[[[JobArray valueForKey:@"jobs"] valueForKey:@"latitude"] objectAtIndex:j] doubleValue] == marker.position.latitude && [[[[JobArray valueForKey:@"jobs"] valueForKey:@"longitude"] objectAtIndex:j] doubleValue]==marker.position.longitude) {
            
            if ([[[[JobArray valueForKey:@"jobs"] objectAtIndex:j] valueForKeyPath:@"count"] intValue] > 1) {
//            if ([[[[JobArray valueForKey:@"jobs"] valueForKey:@"count"] objectAtIndex:j] isEqualToString:@"1"]) {
                

                [self callWebServiceForGetJobsByLocation:[[[JobArray valueForKey:@"jobs"] valueForKey:@"latitude"] objectAtIndex:j] longitude:[[[JobArray valueForKey:@"jobs"] valueForKey:@"longitude"] objectAtIndex:j]];
            }
            else{
                NSString *Title = [NSString stringWithFormat:@"%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"jobTitle"]objectAtIndex:j]];
                NSString *Message = [NSString stringWithFormat:@"%@",[[[JobArray valueForKey:@"jobs"]valueForKey:@"location"]objectAtIndex:j]];
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:Title message:Message preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *ok =[UIAlertAction actionWithTitle:@"More Info" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    JobsDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
                    JobVC.JobData=[[JobArray valueForKey:@"jobs"] objectAtIndex:j];
                    [self.navigationController pushViewController:JobVC animated:YES];
                }];
                UIAlertAction *cancle = [UIAlertAction actionWithTitle:@"Other Jobs" style:UIAlertActionStyleCancel handler:nil];
                [alert addAction:ok];
                [alert addAction:cancle];
                alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                dispatch_async(dispatch_get_main_queue(), ^{
                    [self presentViewController:alert animated:YES completion:nil];
                });
            }
            
        }else{
         
            NSLog(@"No Match");
            
        }
    }
    return YES;
    
}

- (void)fadeMarker:(GMSMarker *)marker {
    _fadedMarker.opacity = 1.0f;  // reset previous faded marker
    // Fade this new marker.
    _fadedMarker = marker;
    _fadedMarker.opacity = 0.5f;
    
}

-(void) navigateToDetailView:(NSMutableArray *)arr{
    JobsDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
    JobVC.JobData = arr;//[[JobArray valueForKey:@"jobs"] objectAtIndex:j];
    [self.navigationController pushViewController:JobVC animated:YES];
}

#pragma mark  WebService Method
-(void) callWebServiceForGetJobsByLocation:(NSString*)latitude longitude:(NSString *)longitude{
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    NSString *str = [NSString stringWithFormat:@"%@api/getJobsByLocation",KBaseUrl];
    NSDictionary *dictParam = @{@"latitude":latitude,@"longitude":longitude};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
        NSLog(@"Response %@",response);
        
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
            MultiJobsViewController *requestStatusView = [self.storyboard instantiateViewControllerWithIdentifier:@"MultiJobsViewController"];
            requestStatusView.multiJobsArray = [response valueForKey:@"jobs"];
            requestStatusView.objFindJobs = self;
            requestStatusView.providesPresentationContextTransitionStyle = YES;
            requestStatusView.definesPresentationContext = YES;
            [requestStatusView setModalPresentationStyle:UIModalPresentationOverCurrentContext];
            [self.tabBarController presentViewController:requestStatusView animated:NO completion:nil];
            


        }
        else if (response==NULL) {
            
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
            NSLog(@"response is null");
            
        }else{
            
            [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
        }
    } failure:^(NSError *error) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
        
    }];
}



@end
