//
//  MultiJobsViewController.h
//  JobsHereApp
//
//  Created by kdstudio on 24/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FindJobsHereViewController.h"

@interface MultiJobsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tblMultiJobs;
//@property (weak, nonatomic) IBOutlet UIView *multiJobView;
@property (strong, nonatomic) NSMutableArray *multiJobsArray;
@property (strong, nonatomic) IBOutlet UIView *bgView;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *heightConstraint;
@property (strong, nonatomic) FindJobsHereViewController *objFindJobs;

@end
