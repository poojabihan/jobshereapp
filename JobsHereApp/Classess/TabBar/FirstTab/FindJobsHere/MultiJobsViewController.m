//
//  MultiJobsViewController.m
//  JobsHereApp
//
//  Created by kdstudio on 24/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "MultiJobsViewController.h"
#import "jobsTableViewCell.h"
#import "UIColor+Hexadecimal.h"
#import "JobsDetailsViewController.h"

@interface MultiJobsViewController ()<UIGestureRecognizerDelegate>

@end

@implementation MultiJobsViewController
@synthesize multiJobsArray;

- (void)viewDidLoad {
    [super viewDidLoad];

    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    
    [self.tblMultiJobs setSeparatorColor:[UIColor colorWithHex:@"00b050"]];
    self.tblMultiJobs.tableFooterView = [UIView new];
    
    if ([multiJobsArray count] > 4) {
        
        self.heightConstraint.constant = (4 * 140) + 40;
        
    }
    else {
        self.heightConstraint.constant = ([multiJobsArray count] * 140) + 40;
    }
}

- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if ([touch.view isDescendantOfView:_tblMultiJobs]) {
        
        // Don't let selections of auto-complete entries fire the
        // gesture recognizer
        return NO;
    }
    
    return YES;
}


#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return multiJobsArray.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    jobsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"jobsTableViewCell"];
    cell.backgroundColor=[UIColor clearColor];
    
    cell.lblTitle.text = [NSString stringWithFormat:@"%@",[[multiJobsArray valueForKey:@"jobTitle"] objectAtIndex:indexPath.row]];
    cell.lblDate.text = [NSString stringWithFormat:@"%@",[[multiJobsArray valueForKey:@"postedDate"]objectAtIndex:indexPath.row]];
    cell.lblCost.text = [NSString stringWithFormat:@"£%@",[[multiJobsArray valueForKey:@"cost"]objectAtIndex:indexPath.row]];
    cell.lblAddress.text = [NSString stringWithFormat:@"%@",[[multiJobsArray valueForKey:@"location"]objectAtIndex:indexPath.row]];
    
    return cell;
}

#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 140;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.objFindJobs navigateToDetailView:[multiJobsArray objectAtIndex:indexPath.row]];
    [self dismissView:nil];
    
}



@end
