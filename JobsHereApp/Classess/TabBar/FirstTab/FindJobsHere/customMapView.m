//
//  customMapView.m
//  JobsHereApp
//
//  Created by kdstudio on 17/05/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "customMapView.h"

@implementation customMapView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib {
    
    [super awakeFromNib];
    
    self.borderView.layer.borderWidth = 2;
    self.borderView.layer.borderColor = [UIColor grayColor].CGColor;

}
@end
