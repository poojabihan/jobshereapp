//
//  JobDetailsViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 19/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import <MessageUI/MessageUI.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <Photos/Photos.h>
#import <Stripe/Stripe.h>

typedef NS_ENUM(NSInteger, STPBackendResult) {
    STPBackendResultSuccess,
    STPBackendResultFailure,
};

typedef void (^STPSourceSubmissionHandler)(STPBackendResult status, NSError *error);
typedef void (^STPPaymentIntentCreationHandler)(STPBackendResult status, NSString *clientSecret, NSError *error);


@protocol StripeControllerDelegate <NSObject>

- (void)stripeViewController:(UIViewController *)controller didFinishWithMessage:(NSString *)message;
- (void)stripeViewController:(UIViewController *)controller didFinishWithError:(NSError *)error;
- (void)createBackendChargeWithSource:(NSString *)sourceID completion:(STPSourceSubmissionHandler)completion;
- (void)createBackendPaymentIntentWithAmount:(NSNumber *)amount completion:(STPPaymentIntentCreationHandler)completion;

@end

@interface JobDetailsViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *PayForJobPDayView;
@property (strong, nonatomic) IBOutlet UIView *JobTitleView;
@property (strong, nonatomic) IBOutlet UIView *JobDetialsView;
@property (strong, nonatomic) IBOutlet UIView *ImageView;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *PayForPDayTextfield;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *JobTitleTextfield;
@property (strong, nonatomic) IBOutlet UITextView *JobDescruptionTextView;
@property (strong, nonatomic) IBOutlet UICollectionView *CollectionView;

@property (strong, nonatomic) NSString *latJobView;
@property (strong, nonatomic) NSString *longiJobView;
@property (strong, nonatomic) NSString *locationJobView;


- (IBAction)SelectImage:(id)sender;
- (IBAction)SubmitData:(id)sender;
-(void)SubmitPost:(NSString *)transactionId strAmt:(NSString *)strAmt;
@end
