//
//  JobDetailsViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 19/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobDetailsViewController.h"
#import "UIColor+Hexadecimal.h"
#import "ELCImagePickerController.h"
#import "ELCAssetTablePicker.h"
#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ImageViewCollectionViewCell.h"
#import "Reachability.h"
#import "Webservice.h"
#import "DGActivityIndicatorView.h"
//#import <MBProgressHUD.h>
#import "JobConfirmPaymenttViewController.h"
#import "QBImagePicker.h"
#import "QBImagePickerController.h"

@interface JobDetailsViewController ()<UITextViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ELCImagePickerControllerDelegate, StripeControllerDelegate, QBImagePickerControllerDelegate>
{
    NSMutableArray *imagesArray;
   // MBProgressHUD *HUD;
    NSMutableArray *servicesArray;
    NSString *image1;
    NSString *image2;
    NSString *image3;
    DGActivityIndicatorView *loader;

}
@property (nonatomic, strong) PHPhotoLibrary *specialLibrary;
@property (nonatomic, retain) UIToolbar* toolBar;

@end

@implementation JobDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    imagesArray = [[NSMutableArray alloc] init];servicesArray = [[NSMutableArray alloc] init];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.PayForJobPDayView.layer.borderWidth = 1;
    self.PayForJobPDayView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    self.JobTitleView.layer.borderWidth = 1;
    self.JobTitleView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    self.JobDetialsView.layer.borderWidth = 1;
    self.JobDetialsView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    self.ImageView.layer.borderWidth = 1;
    self.ImageView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    self.JobDescruptionTextView.delegate=self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
 
    self.toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(updateTextField:)];
    NSArray* barItems = [NSArray arrayWithObjects:doneButton, nil];
    [self.toolBar setItems:barItems animated:YES];
    _PayForPDayTextfield.inputAccessoryView = _toolBar;

}

-(void)viewWillAppear:(BOOL)animated{
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
}

-(void)updateTextField:(id)sender {
    [_PayForPDayTextfield resignFirstResponder];
}


#pragma mark  Keyboard Delegates

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}

- (void)keyboardWasShown:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark  UITextfield Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        
        [nextResponder becomeFirstResponder];
        
    }else{
        
        [textField resignFirstResponder];
        
    }
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

#pragma mark  TextView Delegates

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
   
    self.JobDescruptionTextView.backgroundColor = [UIColor whiteColor];
    
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Multiple image selected

- (IBAction)SelectImage:(id)sender {
    
    [servicesArray removeAllObjects];[imagesArray removeAllObjects];
    
    /*ELCImagePickerController *elcPicker = [[ELCImagePickerController alloc] initImagePicker];
    
    elcPicker.maximumImagesCount = 3; //Set the maximum number of images to select to 100
    elcPicker.returnsOriginalImage = YES; //Only return the fullScreenImage, not the fullResolutionImage
    elcPicker.returnsImage = YES; //Return UIimage if YES. If NO, only return asset location information
    elcPicker.onOrder = YES; //For multiple image selection, display and return order of selected images
    elcPicker.mediaTypes = @[(NSString *)kUTTypeImage, (NSString *)kUTTypeMovie]; //Supports image and movie types
    
    elcPicker.imagePickerDelegate = self;
    
    [self presentViewController:elcPicker animated:YES completion:nil];*/
    
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    imagePickerController.delegate = self;
    imagePickerController.mediaType = QBImagePickerMediaTypeAny;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.maximumNumberOfSelection = 3;
    
    [self presentViewController:imagePickerController animated:YES completion:NULL];

    
}

- (void)elcImagePickerController:(ELCImagePickerController *)picker didFinishPickingMediaWithInfo:(NSArray *)info
{
    imagesArray = [NSMutableArray arrayWithCapacity:[info count]];
    for (NSDictionary *dict in info) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:@"UIImagePickerControllerOriginalImage"];
                NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
                NSString *imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                [servicesArray addObject:imgString];
                [imagesArray addObject:image];
                [_CollectionView reloadData];
            }else{
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        }
    }
     [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)elcImagePickerControllerDidCancel:(ELCImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark  Collection View Delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return imagesArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *identifier = @"ImageViewCollectionViewCell";
    
    ImageViewCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.JobsIMage.image = [imagesArray objectAtIndex:indexPath.row];
    
    return cell;
}

- (IBAction)SubmitData:(id)sender {
    if ([self.PayForPDayTextfield.text isEqualToString:@""]) {
        
        [self validationErrorAlert:@"Please enter Job Rate"];
        
    }else if ([self.JobTitleTextfield.text isEqualToString:@""]){
        
        [self validationErrorAlert:@"Please enter Job Title"];
        
    }else if ([self.JobDescruptionTextView.text isEqualToString:@""]){
        
        [self validationErrorAlert:@"Please enter Job Description"];
        
    }else if (servicesArray.count == 0){
        
        [self validationErrorAlert:@"Please select at list one image"];
        
    }else{

    [self performSegueWithIdentifier:@"toConfirmPayment" sender:nil];
//    CardViewController *cardVC = [CardViewController new];
//    cardVC.delegate = self;
//    [self.navigationController pushViewController:cardVC animated:YES];
    return;
    }
   
    
}


///**
// Ask the example backend to create a PaymentIntent with the specified amount.
//
// The implementation of this function is not interesting or relevant to using PaymentIntents. The
// method signature is the most interesting part: you need some way to ask *your* backend to create
// a PaymentIntent with the correct properties, and then it needs to pass the client secret back.
//
// @param amount Amount to charge the customer
// @param completion completion block called with status of backend call & the client secret if successful.
// */
//- (void)createBackendPaymentIntentWithAmount:(NSNumber *)amount completion:(STPPaymentIntentCreationHandler)completion {
//    if (!BackendBaseURL) {
//        NSError *error = [NSError errorWithDomain:StripeDomain
//                                             code:STPInvalidRequestError
//                                         userInfo:@{NSLocalizedDescriptionKey: @"You must set a backend base URL in Constants.m to create a charge."}];
//        completion(STPBackendResultFailure, nil, error);
//        return;
//    }
//
//    // This asks the backend to create a PaymentIntent for us, which can then be passed to the Stripe SDK to confirm
//    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
//
//    NSString *urlString = [BackendBaseURL stringByAppendingPathComponent:@"create_intent"];
//    NSURL *url = [NSURL URLWithString:urlString];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
//    request.HTTPMethod = @"POST";
//    NSString *postBody = [NSString stringWithFormat:@"amount=%@", amount];
//    NSData *data = [postBody dataUsingEncoding:NSUTF8StringEncoding];
//
//    NSURLSessionUploadTask *uploadTask = [session uploadTaskWithRequest:request
//                                                               fromData:data
//                                                      completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//                                                          NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *)response;
//                                                          if (!error && httpResponse.statusCode != 200) {
//                                                              error = [NSError errorWithDomain:StripeDomain
//                                                                                          code:STPInvalidRequestError
//                                                                                      userInfo:@{NSLocalizedDescriptionKey: @"There was an error connecting to your payment backend."}];
//                                                          }
//                                                          if (error || data == nil) {
//                                                              completion(STPBackendResultFailure, nil, error);
//                                                          }
//                                                          else {
//                                                              NSError *jsonError = nil;
//                                                              id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
//
//                                                              if (json &&
//                                                                  [json isKindOfClass:[NSDictionary class]] &&
//                                                                  [json[@"secret"] isKindOfClass:[NSString class]]) {
//                                                                  completion(STPBackendResultSuccess, json[@"secret"], nil);
//                                                              }
//                                                              else {
//                                                                  completion(STPBackendResultFailure, nil, jsonError);
//                                                              }
//                                                          }
//                                                      }];
//
//    [uploadTask resume];
//}

#pragma mark - StripeViewControllerDelegate

- (void)stripeViewController:(UIViewController *)controller didFinishWithMessage:(NSString *)message {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:action];
        alertController.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

        [controller presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)stripeViewController:(UIViewController *)controller didFinishWithError:(NSError *)error {
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[error localizedDescription] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(__unused UIAlertAction *action) {
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [alertController addAction:action];
        alertController.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

        [controller presentViewController:alertController animated:YES completion:nil];
    });
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"toConfirmPayment"]) {

        JobConfirmPaymenttViewController *vc = [segue destinationViewController];
        vc.strJobTitle = self.JobTitleTextfield.text;
        vc.strJobDesc = self.JobDescruptionTextView.text;
        vc.strAmount = self.PayForPDayTextfield.text;
        vc.objJobsDetail = self;

    }
}

-(void)SubmitPost:(NSString *)transactionId strAmt:(NSString *)strAmt{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if ([self.PayForPDayTextfield.text isEqualToString:@""]) {
            
            [self validationErrorAlert:@"Please enter Job Rate"];
            
        }else if ([self.JobTitleTextfield.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter Job Title"];
            
        }else if ([self.JobDescruptionTextView.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter Job Description"];
            
        }else if (servicesArray.count == 0){
            
            [self validationErrorAlert:@"Please select at list one image"];
            
        }else{
            
            NSString *UserID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
            NSLog(@"-----%lu",(unsigned long)servicesArray.count);
            if (servicesArray.count==1) {
                image1 = [servicesArray objectAtIndex:0];image2=@"";image3=@"";
            }else if (servicesArray.count == 2){
                image1 = [servicesArray objectAtIndex:0];image2 = [servicesArray objectAtIndex:1];image3=@"";
            }else if (servicesArray.count == 3){
                image1 = [servicesArray objectAtIndex:0];image2 = [servicesArray objectAtIndex:1];image3 = [servicesArray objectAtIndex:2];
            }else{
                image1=@"";image2=@"";image3=@"";
            }
            
            NSString *JobMessage = [self.JobDescruptionTextView text];
            
            JobMessage = [JobMessage stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            
            loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
            loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
            
            [self.view addSubview:loader];
            [loader startAnimating];
            
//            HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//            HUD.bezelView.color = [UIColor blackColor];
//            HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//            HUD.contentColor = [UIColor whiteColor];
            
            NSString *strLong = @"";
            if (self.longiJobView != nil) {
                strLong = self.longiJobView;
            }
            
            NSString *strLat = @"";
            if (self.latJobView != nil) {
                strLat = self.latJobView;
            }
            
            NSDictionary *dictParam = @{@"userId":UserID,@"jobTitle":self.JobTitleTextfield.text,@"jobDescription":JobMessage,@"cost":self.PayForPDayTextfield.text,@"latitude":strLat,@"longitude":strLong,@"location":self.locationJobView,@"image1":image1,@"image2":image2,@"image3":image3,@"transactionId":transactionId};
            
            NSDictionary *dictParamToPrint = @{@"userId":UserID,@"jobTitle":self.JobTitleTextfield.text,@"jobDescription":JobMessage,@"cost":self.PayForPDayTextfield.text,@"latitude":strLat,@"longitude":strLong,@"location":self.locationJobView,@"transactionId":transactionId};

            NSLog(@"Dic Param - %@",dictParamToPrint);
            
            NSString *JobsPost=[NSString stringWithFormat:@"%@api/jobPost",KBaseUrl];
            
            [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
                                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [HUD hideAnimated:YES];
                        [loader stopAnimating];
                    });
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:kAppNameAlert message:@"Job posted successfully!" preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *Done =[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
                        [self.navigationController popViewControllerAnimated:YES];
                    }];
                    [alert addAction:Done];
                    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alert animated:YES completion:nil];
                    });
                }else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];
                    });
                    
//                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                    [self callWebServiceForRefundPayment:transactionId amount:strAmt];

                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];

                    });
                    [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    [self callWebServiceForRefundPayment:transactionId amount:strAmt];
                    
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[HUD hideAnimated:YES];
                    [loader stopAnimating];

                });
                NSLog(@"Error %@",error);
//                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                [self callWebServiceForRefundPayment:transactionId amount:strAmt];
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

-(void)callWebServiceForRefundPayment:(NSString *)transactionId amount:(NSString *)amt{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        
            NSString *UserID = [[NSUserDefaults standardUserDefaults] valueForKey:@"UserID"];
            NSLog(@"-----%lu",(unsigned long)servicesArray.count);
        
        
            NSDictionary *dictParam = @{@"userId":UserID,
                                        @"transactionId":transactionId,
                                        @"amount":amt,
                                        @"currencyCode":@"GBP"};
            
        
            NSLog(@"Dic Param - %@",dictParam);
            
            NSString *JobsPost=[NSString stringWithFormat:@"%@api/addFailedTransaction",KBaseUrl];
            
            [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // [HUD hideAnimated:YES];
                        [loader stopAnimating];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Your refund has been initiated." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                }else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[HUD hideAnimated:YES];
                        [loader stopAnimating];
                        
                    });
                    [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[HUD hideAnimated:YES];
                    [loader stopAnimating];
                    
                });
                NSLog(@"Error %@",error);
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }];
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    NSLog(@"Selected assets:");
    NSLog(@"%@", assets);
    
    imagesArray = [NSMutableArray arrayWithCapacity:[assets count]];
    /*for (NSDictionary *dict in assets) {
        if ([dict objectForKey:UIImagePickerControllerMediaType] == ALAssetTypePhoto){
            if ([dict objectForKey:UIImagePickerControllerOriginalImage]){
                UIImage* image=[dict objectForKey:@"UIImagePickerControllerOriginalImage"];
                NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
                NSString *imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
                [servicesArray addObject:imgString];
                [imagesArray addObject:image];
                [_CollectionView reloadData];
            }else{
                NSLog(@"UIImagePickerControllerReferenceURL = %@", dict);
            }
        }
    }*/

    PHImageRequestOptions *options = [[PHImageRequestOptions alloc] init];
    options.deliveryMode = PHImageRequestOptionsDeliveryModeHighQualityFormat; //I only want the highest possible quality
    options.synchronous = NO;
    options.networkAccessAllowed = YES;
    options.progressHandler = ^(double progress, NSError *error, BOOL *stop, NSDictionary *info) {
        NSLog(@"%f", progress); //follow progress + update progress bar
    };

    for (PHAsset *asset in assets) {
        // Do something with the asset
        
        [[PHImageManager defaultManager] requestImageForAsset:asset targetSize:self.view.frame.size contentMode:PHImageContentModeAspectFill options:options resultHandler:^(UIImage *image, NSDictionary *info) {
            NSLog(@"reponse %@", info);
            NSLog(@"got image %f %f", image.size.width, image.size.height);
            
            if (image == nil) {
                [self validationErrorAlert:@"There seems to be some problem with this image. Please select another image."];
                return;
            }
            
            NSData *imageData = UIImageJPEGRepresentation(image, 0.5);
            NSString *imgString = [imageData base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
            [servicesArray addObject:imgString];
            [imagesArray addObject:image];
            [_CollectionView reloadData];

        }];
    }

    


    
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController
{
    NSLog(@"Canceled.");
    
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end
