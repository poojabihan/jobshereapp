//
//  JobConfirmPaymenttViewController.m
//  JobsHereApp
//
//  Created by kdstudio on 23/10/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "JobConfirmPaymenttViewController.h"
#import <Stripe/Stripe.h>
#import "Reachability.h"
#import "DGActivityIndicatorView.h"
//#import <MBProgressHUD.h>
#import "Webservice.h"

@interface JobConfirmPaymenttViewController (){
    //MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

}
@property (weak, nonatomic) IBOutlet UILabel *lblJobTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblJobDescription;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UITextField *txtCardNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtCvv;
@property (weak, nonatomic) IBOutlet UITextField *txtExpYear;
@property (weak, nonatomic) IBOutlet UITextField *txtExpMonth;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;


@end

@implementation JobConfirmPaymenttViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _lblJobTitle.text = self.strJobTitle;
    _lblJobDescription.text = self.strJobDesc;
    _lblAmount.text = [NSString stringWithFormat:@"£ %@",self.strAmount];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)stripePayment {
    
    STPCardParams *cardParams = [[STPCardParams alloc] init];
    cardParams.number = _txtCardNumber.text;
    cardParams.expMonth = [_txtExpMonth.text integerValue];
    cardParams.expYear = [_txtExpYear.text integerValue];
    cardParams.cvc = _txtCvv.text;
    
    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken * _Nullable token, NSError * _Nullable error) {
        if (token == nil || error != nil) {
            // Present error to user...
            [loader stopAnimating];

            [self validationErrorAlert:error.localizedDescription];
            //[HUD hideAnimated:YES];
            
            return;
        }
        [self callWebServicePayment:token.tokenId];
    }];
    
    
    
    /*do {
     try stripCard.validateReturningError()
     
     STPAPIClient.shared().createToken(with: stripCard) { (token, error) in
     print(token?.tokenId ?? "no token", error ?? "no error")
     
     NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
     
     if error != nil {
     self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: error?.localizedDescription)
     }
     else {
     self.didFinishPayment!((token?.tokenId)!)
     self.dismiss(animated: false, completion: nil)
     }
     }
     
     } catch {
     NVActivityIndicatorPresenter.sharedInstance.stopAnimating(nil)
     print("Error info: \(error)")
     self.panel.showNotify(withStatus: .failed, inView: self.appDelegate.window!, title: error.localizedDescription)
     
     }*/
    
//    [[STPAPIClient sharedClient] createTokenWithCard:cardParams completion:^(STPToken *token, NSError *error) {
//        if (token == nil || error != nil) {
//            // Present error to user...
//            [self validationErrorAlert:error.localizedDescription];
//            return;
//        }
//        [self callWebServicePayment:token.tokenId];
////        [self submitTokenToBackend:token completion:^(NSError *error) {
////            if (error) {
////                // Present error to user...
////            }
////            else {
////                // Continue with payment...
////            }
////        }];
//    }];
}

-(void)callWebServicePayment:(NSString *)strToken {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        _btnConfirm.userInteractionEnabled = NO;
        
        
        NSString *UserId =[[NSUserDefaults standardUserDefaults]valueForKey:@"UserID"];
        NSDictionary *dictParam = @{@"stripeToken":strToken,
                                    @"amount":_strAmount,
                                    @"currency":@"GBP",
                                    @"description":_strJobDesc,
                                    @"userId": UserId
                                    };
            NSLog(@"Dic Param - %@",dictParam);
            
            NSString *JobsPost=[NSString stringWithFormat:@"%@api/initiatePayment",KBaseUrl];
            
            [Webservice requestPostUrl:JobsPost parameters:dictParam success:^(NSDictionary *response){
                
//                NSLog(response);

                if (([[response objectForKey:@"status"] boolValue] == 1)) {
                    
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                      //  [HUD hideAnimated:YES];
                        [loader stopAnimating];

                    });
                    
                    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:kAppNameAlert message:[response valueForKey:@"message"] preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        [self.objJobsDetail SubmitPost:[response valueForKeyPath:@"transactionId"] strAmt:_strAmount];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }];
                    [alertCont addAction:okAction];
                    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self presentViewController:alertCont animated:true completion:nil];
                    });
                    
                    
                }else if (response==NULL) {
                    _btnConfirm.userInteractionEnabled = YES;
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [HUD hideAnimated:YES];
                        [loader stopAnimating];
                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    _btnConfirm.userInteractionEnabled = YES;

                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [HUD hideAnimated:YES];
                        [loader stopAnimating];

                    });
                    [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                }
            } failure:^(NSError *error) {
                _btnConfirm.userInteractionEnabled = YES;

                dispatch_async(dispatch_get_main_queue(), ^{
                   // [HUD hideAnimated:YES];
                    [loader stopAnimating];

                });
                NSLog(@"Error %@",error);
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }];
        
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

    }
}

- (IBAction)btnConfirmAction:(id)sender {
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    if ([_txtCardNumber.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter card number."];
        //[HUD hideAnimated:YES];
        [loader stopAnimating];
    }
    else if ([_txtExpMonth.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter expiry month."];
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

    }
    else if ([_txtExpYear.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter expiry year."];
       // [HUD hideAnimated:YES];
        [loader stopAnimating];

    }
    else if ([_txtCvv.text isEqualToString:@""]) {
        [self validationErrorAlert:@"Please enter CVV."];
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

    }
    else {
        [self stripePayment];
    }
}

- (IBAction)btnCancelAction:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];

    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
