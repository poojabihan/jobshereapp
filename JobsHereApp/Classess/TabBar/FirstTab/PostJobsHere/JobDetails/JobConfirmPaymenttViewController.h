//
//  JobConfirmPaymenttViewController.h
//  JobsHereApp
//
//  Created by kdstudio on 23/10/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobDetailsViewController.h"

@interface JobConfirmPaymenttViewController : UIViewController

@property (strong, nonatomic) NSString *strJobTitle;
@property (strong, nonatomic) NSString *strJobDesc;
@property (strong, nonatomic) NSString *strAmount;
@property (strong, nonatomic) NSString *strJobID;
@property (strong, nonatomic) JobDetailsViewController *objJobsDetail;

@end
