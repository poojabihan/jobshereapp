//
//  PostJobsHereViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "PostJobsHereViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "JobDetailsViewController.h"
#import "JobActionPopUpViewController.h"
@interface PostJobsHereViewController ()<GMSMapViewDelegate,CLLocationManagerDelegate>
{
    GMSMapView *_mapView;CLLocationManager *_manager;GMSMarker *_locationMarker;
    BOOL _firstLocationUpdate;GMSMarker *marker;
    
}
@property CLLocationCoordinate2D location,location1;
@property (nonatomic, retain) UIToolbar* toolBar;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *containerForMapView;
@property (weak, nonatomic) IBOutlet UILabel *lblHeader;


@end

@implementation PostJobsHereViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:-33.868
                                                            longitude:151.2086
                                                                 zoom:12];
    _mapView = [GMSMapView mapWithFrame:CGRectMake(_containerForMapView.frame.origin.x, _containerForMapView.frame.origin.y, _containerForMapView.frame.size.width, _containerForMapView.frame.size.height) camera:camera];
    _mapView.settings.compassButton = YES;
    _mapView.settings.myLocationButton = YES;
    marker = [[GMSMarker alloc] init];
    _mapView.delegate=self;
    // Listen to the myLocation property of GMSMapView.
    
    
    [_mapView addObserver:self
               forKeyPath:@"myLocation"
                  options:NSKeyValueObservingOptionNew
                  context:NULL];
    
    [self.view addSubview:_headerView];
    _containerForMapView = _mapView;

    [self.view addSubview:_containerForMapView];

    
    
    // Ask for My Location data after the map has already been added to the UI.
    dispatch_async(dispatch_get_main_queue(), ^{
        _mapView.myLocationEnabled = YES;
    });
#pragma mark - update current location
    
    _manager = [[CLLocationManager alloc] init];
    if ([_manager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_manager requestWhenInUseAuthorization];
    }
    _manager.delegate = self;
    _manager.desiredAccuracy = kCLLocationAccuracyBest;
    _manager.distanceFilter = 5.0f;
    [_manager startUpdatingLocation];
    
    _mapView.mapType = kGMSTypeNormal;
    
    
}



-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [_mapView clear];
    [_mapView layoutIfNeeded];
}

- (void)dealloc {
    [_mapView removeObserver:self
                  forKeyPath:@"myLocation"
                     context:NULL];
}
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context {
    if (!_firstLocationUpdate) {
        // If the first location update has not yet been recieved, then jump to that
        // location.
        _firstLocationUpdate = YES;
        CLLocation *location = [change objectForKey:NSKeyValueChangeNewKey];
        _mapView.camera = [GMSCameraPosition cameraWithTarget:location.coordinate
                                                         zoom:14];
    }
}

-(void)locationManager:(CLLocationManager*)manager didUpdateLocations:(NSArray *)locations{
    
    CLLocation *location = [locations lastObject];
    
    if (_locationMarker == nil) {
        _locationMarker = [[GMSMarker alloc] init];
        _locationMarker.position = location.coordinate;
        
        // Animated walker images derived from an www.angryanimator.com tutorial.
        // See: http://www.angryanimator.com/word/2010/11/26/tutorial-2-walk-cycle/
        
      /*  NSArray *frames = @[[UIImage imageNamed:@"step1"],
                            [UIImage imageNamed:@"step2"],
                            [UIImage imageNamed:@"step3"],
                            [UIImage imageNamed:@"step4"],
                            [UIImage imageNamed:@"step5"],
                            [UIImage imageNamed:@"step6"],
                            [UIImage imageNamed:@"step7"],
                            [UIImage imageNamed:@"step8"]];
        
        _locationMarker.icon = [UIImage animatedImageWithImages:frames duration:0.8];*/
        _locationMarker.groundAnchor = CGPointMake(0.5f, 0.97f); // Taking into account walker's shadow
        _locationMarker.map = _mapView;
    } else {
        [CATransaction begin];
        [CATransaction setAnimationDuration:2.0];
        _locationMarker.position = location.coordinate;
        [CATransaction commit];
    }
    GMSCameraUpdate *move = [GMSCameraUpdate setTarget:location.coordinate zoom:12];
    [_mapView animateWithCameraUpdate:move];
}


-(void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate{
    
    NSLog(@"Tap at (%g,%g)", coordinate.latitude, coordinate.longitude);
    marker.position = coordinate;
    marker.map=_mapView;
    _location1=coordinate;
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        [((GMSMapView*)self.containerForMapView) setSelectedMarker:marker];
    }];
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude]; //insert your coordinates
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  if (placemark) {
                      
                      NSLog(@"placemark %@",placemark);
                      //String to hold address
                      NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
                      NSLog(@"addressDictionary %@", placemark.addressDictionary);
                      //NSLog(@"placemark %@",placemark.region);
                      //NSLog(@"placemark %@",placemark.country);  // Give Country Name
                      //NSLog(@"placemark %@",placemark.locality); // Extract the city name
                      //NSLog(@"location %@",placemark.name);
                      //NSLog(@"location %@",placemark.ocean);
                      //NSLog(@"location %@",placemark.postalCode);
                      //NSLog(@"location %@",placemark.subLocality);
                      //NSLog(@"location %@",placemark.location);
                      //Print the location to console
                      NSLog(@"I am currently at %@",locatedAt);
                      
                      
                     
                    JobActionPopUpViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"JobActionPopUpViewController"];
                      vc.strLocation = locatedAt;
                      vc.providesPresentationContextTransitionStyle = YES;
                      vc.definesPresentationContext = YES;
                      vc.latJobView = [NSString stringWithFormat:@"%f",coordinate.latitude];
                      vc.nav = self.navigationController;
                      vc.longiJobView= [NSString stringWithFormat:@"%f",coordinate.longitude];
                      
                      [vc setModalPresentationStyle:UIModalPresentationOverCurrentContext];
                      [self.tabBarController presentViewController:vc animated:NO completion:nil];
                   
                     
                   //   [self.navigationController presentViewController:vc animated:NO completion:nil];
                      
                      
                   /*   UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Add job at this location..." message:locatedAt preferredStyle:UIAlertControllerStyleAlert];
                      UIAlertAction *Cancle = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:nil];
                      UIAlertAction *JobHere = [UIAlertAction actionWithTitle:@"Post Job" style:UIAlertActionStyleDefault handler:^(UIAlertAction *Action){
                          JobDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobDetailsViewController"];
                          JobVC.latJobView = [NSString stringWithFormat:@"%f",coordinate.latitude];
                          JobVC.longiJobView= [NSString stringWithFormat:@"%f",coordinate.longitude];
                          JobVC.locationJobView = [NSString stringWithFormat:@"%@",locatedAt];
                          [self.navigationController pushViewController:JobVC animated:YES];
                      }];
                      [alert addAction:Cancle];[alert addAction:JobHere];
                      dispatch_async(dispatch_get_main_queue(), ^{
                          [self presentViewController:alert animated:YES completion:nil];
                      });*/
                  }
                  else {
                      NSLog(@"Could not locate");
                  }
            }
     ];
}

@end
