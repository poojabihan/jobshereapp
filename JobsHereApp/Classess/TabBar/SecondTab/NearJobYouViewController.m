//
//  NearJobYouViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 18/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "NearJobYouViewController.h"
//#import <MBProgressHUD.h>
#import "NearJobSYouTableViewCell.h"
#import "Webservice.h"
#import "Reachability.h"
#import "JobsDetailsViewController.h"
#import "UIImageView+WebCache.h"
#import <CoreLocation/CoreLocation.h>
#import "FindJobsHereViewController.h"
#import "DGActivityIndicatorView.h"
@interface NearJobYouViewController ()<CLLocationManagerDelegate>
{
   // MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;
    BOOL isList;
    NSArray *searchResults;UIRefreshControl *refreshController;
    BOOL searchEnabled;NSMutableArray *NearJobArray;
    NSString *longitude, *latitude;
    CLLocation *LocationFirst,*LocationSecond,*LocationFinal;
}
@property (weak, nonatomic) IBOutlet UIButton *btnToggleListGrid;
@property (strong, nonatomic) CLLocationManager *locationManager;
@end

@implementation NearJobYouViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    isList = YES;
    
    [self getCurrntLocation];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.TableView.backgroundColor = [UIColor clearColor];;
 
    refreshController = [[UIRefreshControl alloc] init];
    refreshController.tintColor=[UIColor darkGrayColor];
    [refreshController addTarget:self action:@selector(handleRefresh:) forControlEvents:UIControlEventValueChanged];
    //refreshController.attributedTitle = [[NSAttributedString alloc] initWithString:@"Refreshing Data...."];
    [self.TableView addSubview:refreshController];
}

-(void)viewWillAppear:(BOOL)animated{
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    dispatch_async(dispatch_get_main_queue(), ^{
         [self getCurrntLocation];
    });
}

-(void)getCurrntLocation{
    self.locationManager = [CLLocationManager new];
    self.locationManager.delegate = self;
    self.locationManager.distanceFilter = 80.0;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    if ([[[UIDevice currentDevice]systemVersion] intValue] >= 8){
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]){
            [self.locationManager requestWhenInUseAuthorization];
        }
    }
    [self.locationManager startUpdatingLocation];
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    
    if (status == kCLAuthorizationStatusDenied)
    {
        //location denied, handle accordingly
        NSLog(@"Dont allow");
        //[indicator stopAnimating];
        [self.locationManager stopUpdatingLocation];
        latitude = @"";
        longitude = @"";
        dispatch_async(dispatch_get_main_queue(), ^{
          //  [HUD hideAnimated:YES];
          [loader stopAnimating];
            
        });
        
    }
    else if (status == kCLAuthorizationStatusAuthorizedAlways || status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        NSLog(@"Allow");
        
        //hooray! begin startTracking
    }
    
}
-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLGeocoder *geocoder = [[CLGeocoder alloc] init] ;
    [geocoder cancelGeocode];
    
    [geocoder reverseGeocodeLocation:self.locationManager.location
                   completionHandler:^(NSArray *placemarks, NSError *error)
     {
         NSLog(@"reverseGeocodeLocation:completionHandler: Completion Handler called!");
         
         if (error)
         {
             NSLog(@"Geocode failed with error: %@", error);
             [self.locationManager stopUpdatingLocation];
             return;
         }
         CLPlacemark *placemark = [placemarks objectAtIndex:0];
         CLLocation *location = placemark.location;
         longitude = [[NSNumber numberWithDouble: location.coordinate.longitude] stringValue];
         latitude = [[NSNumber numberWithDouble: location.coordinate.latitude] stringValue];
         
         dispatch_async(dispatch_get_main_queue(), ^{
             
             [self LoadDataWebservices];
         });
         
     }];
    
    [self.locationManager stopUpdatingLocation];
}
-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"%@",error.localizedDescription);
   
    [self.locationManager stopUpdatingLocation];
    
    switch([error code])
    {
            
        case kCLErrorNetwork: // general, network-related error
        {
         
        }
            break;
        case kCLErrorDenied:{
            UIAlertController * alert=[UIAlertController alertControllerWithTitle:@"Background Location Access Disabled" message:@"In order to be notified, please open this app's settings and set location access to 'Always'." preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction * settingsAction=[UIAlertAction actionWithTitle:@"Settings" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                @try
                {
                    NSLog(@"tapped Settings");
                    BOOL canOpenSettings = (UIApplicationOpenSettingsURLString != NULL);
                    if (canOpenSettings)
                    {
                        NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                        [[UIApplication sharedApplication]openURL:url options:@{} completionHandler:nil];
                    }
                }
                @catch (NSException *exception)
                {
                    
                }
                
            }];
            UIAlertAction * cancelAction=[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:nil];
            
            [alert addAction:settingsAction];
            [alert addAction:cancelAction];
            alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

            [self presentViewController:alert animated:YES completion:nil];
        }
            break;
    }
}

- (IBAction)btnToggleListGridAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toMapView" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    FindJobsHereViewController *obj = [segue destinationViewController];
    obj.showListView = YES;
}

#pragma mark  Refresh Activity

-(void)handleRefresh : (id)sender{
    
    [self LoadDataWebservices];
}

#pragma mark  LoadDataWebServices

-(void)LoadDataWebservices{
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        NSUserDefaults *standardUserDefaults = [NSUserDefaults standardUserDefaults];
        NSString *LoginUserID = [standardUserDefaults stringForKey:@"UserID"];

        NSDictionary *dictParam = @{@"latitude":latitude,@"longitude":longitude,@"userId":LoginUserID};
        
        NSString *GetJobs=[NSString stringWithFormat:@"%@api/getJobsList",KBaseUrl];
        
        [Webservice requestPostUrl:GetJobs parameters:dictParam success:^(NSDictionary *response) {
            
            if (([[response objectForKey:@"status"]boolValue] == 1)) {
                
                NSLog(@"Response - %@",response);
                
                NearJobArray = [response valueForKey:@"jobs"];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.TableView reloadData];
                    //[HUD hideAnimated:YES];
                    [loader stopAnimating];
                    [refreshController endRefreshing];
                });
            }
            
        } failure:^(NSError *error) {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES];
                 [loader stopAnimating];
                 [refreshController endRefreshing];
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            });
            NSLog(@"Error %@",error);
        }];
        
    }else{
        
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        
    }
    
}

#pragma mark  Alert Delegate

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self presentViewController:alertCont animated:true completion:nil];
        
    });
    
}

#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (searchEnabled) {
        return [searchResults count];
    }
    else{
        return [NearJobArray count];
    }
}

-(UITableViewCell*)tableView:(UITableView*)tableView cellForRowAtIndexPath:(NSIndexPath*)indexPath{
    
    NearJobSYouTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NearJobSYouTableViewCell"];
    cell.selectionStyle=UITableViewCellSelectionStyleNone;
    cell.ProfileImage.layer.cornerRadius = 22.5;
    cell.ProfileImage.layer.masksToBounds=YES;
    cell.backgroundColor=[UIColor clearColor];
    if(cell==nil){
        NSArray*nib=[[NSBundle mainBundle]loadNibNamed:@"NearJobSYouTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    if (searchEnabled) {
        
        cell.JobTitle.text = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"jobTitle"]objectAtIndex:indexPath.row]];
        cell.jobLocation.text = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"location"]objectAtIndex:indexPath.row]];
        cell.PricePDay.text = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"cost"]objectAtIndex:indexPath.row]];
        cell.PostDate.text=[NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"postedDate"]objectAtIndex:indexPath.row]];
        NSString *FirstName = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"fname"]objectAtIndex:indexPath.row]];
        NSString *SecondName = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"lname"]objectAtIndex:indexPath.row]];
        cell.UserName.text = [NSString stringWithFormat:@"%@ %@",FirstName,SecondName];
        [cell.ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,[[searchResults valueForKey:@"profileImage"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];
        LocationFirst = [[CLLocation alloc] initWithLatitude:[@"22.7546" floatValue] longitude:[@"75.9059" floatValue]];
        LocationSecond = [[CLLocation alloc] initWithLatitude:[[[searchResults valueForKey:@"latitude"]objectAtIndex:indexPath.row]floatValue] longitude:[[[searchResults valueForKey:@"longitude"]objectAtIndex:indexPath.row]floatValue]];
        CLLocationDistance Meter = [LocationFirst distanceFromLocation:LocationSecond];
        CGFloat km = Meter/1000;
        cell.Distance.text = [NSString stringWithFormat:@"%f",km];
        cell.Distance.text = [NSString stringWithFormat:@"%@",[[searchResults valueForKey:@"distance"]objectAtIndex:indexPath.row]];

    }else{
        
        cell.JobTitle.text = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"jobTitle"]objectAtIndex:indexPath.row]];
        cell.jobLocation.text = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"location"]objectAtIndex:indexPath.row]];
        cell.PricePDay.text = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"cost"]objectAtIndex:indexPath.row]];
        cell.PostDate.text=[NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"postedDate"]objectAtIndex:indexPath.row]];
        NSString *FirstName = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"fname"]objectAtIndex:indexPath.row]];
        NSString *SecondName = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"lname"]objectAtIndex:indexPath.row]];
        cell.UserName.text = [NSString stringWithFormat:@"%@ %@",FirstName,SecondName];
        [cell.ProfileImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",ProfileImageUpload,[[NearJobArray valueForKey:@"profileImage"]objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"man"]];
        LocationFirst = [[CLLocation alloc] initWithLatitude:[@"22.7546" floatValue] longitude:[@"75.9059" floatValue]];
        LocationSecond = [[CLLocation alloc] initWithLatitude:[[[NearJobArray valueForKey:@"latitude"]objectAtIndex:indexPath.row]floatValue] longitude:[[[NearJobArray valueForKey:@"longitude"]objectAtIndex:indexPath.row]floatValue]];
        CLLocationDistance Meter = [LocationFirst distanceFromLocation:LocationSecond];
        CGFloat km = Meter/1000;
        cell.Distance.text = [NSString stringWithFormat:@"%f",km];
        cell.Distance.text = [NSString stringWithFormat:@"%@",[[NearJobArray valueForKey:@"distance"]objectAtIndex:indexPath.row]];

    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (searchEnabled)
    {
        JobsDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
        JobVC.JobData = [searchResults objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:JobVC animated:YES];
        
    }else{
        
        JobsDetailsViewController *JobsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobsDetailsViewController"];
        JobsVC.JobData = [NearJobArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:JobsVC animated:YES];
        
    }

}

#pragma mark  TableView Height

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 190;
    
}

#pragma mark  Search Bar Filter

-(void)updateFilteredContentForAirlineName:(NSString *)airlineName scope:(NSString *)scope{
    
    if (airlineName == nil) {
        // If empty the search results are the same as the original data
        searchResults = [NearJobArray mutableCopy];
    } else {
        //NSMutableArray *searchResults = [[NSMutableArray alloc] init];
        //BEGINSWITH
        if ([scope isEqualToString:@"0"]) {
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(jobTitle contains[cd] %@)", airlineName];
            NSArray * search = [NearJobArray filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }else if ([scope isEqualToString:@"1"]){
            NSPredicate *pred = [NSPredicate predicateWithFormat:@"(name contains[cd] %@)", airlineName];
            NSArray * search = [NearJobArray filteredArrayUsingPredicate:pred];
            NSLog(@"Search Array %@",search);
            searchResults = [NSMutableArray arrayWithArray:search];
            [self.TableView reloadData];
        }
    }
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    if(searchBar.text.length == 0) {
        searchEnabled = NO;
        [self.TableView reloadData];
    }else {
        searchEnabled = YES;
        [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}

#pragma mark  Search Bar Delegate

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    if (searchBar.text.length==0) {
    [searchBar resignFirstResponder];
    }else{
    [searchBar resignFirstResponder];
    searchEnabled = YES;
    [self updateFilteredContentForAirlineName:searchBar.text scope:[NSString stringWithFormat: @"%ld", (long)self.SearchBar.selectedScopeButtonIndex]];
    }
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
    [searchBar setText:@""];
    searchEnabled = NO;
    [self.TableView reloadData];
}

-(void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope{
    // NSNumber *scopeButtonPressedIndexNumber;
    // scopeButtonPressedIndexNumber = [NSNumber numberWithInt:selectedScope];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = YES;
    [UIView animateWithDuration:0.2f animations:^{
        [searchBar sizeToFit];
    }];
    [searchBar setShowsCancelButton:YES animated:YES];
    return YES;
}

- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar {
    searchBar.showsScopeBar = NO;
    [searchBar sizeToFit];
    [searchBar setShowsCancelButton:NO animated:YES];
    return YES;
}

@end
