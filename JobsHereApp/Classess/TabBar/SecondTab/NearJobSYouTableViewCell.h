//
//  NearJobSYouTableViewCell.h
//  JobsHereApp
//
//  Created by Alok Mishra on 23/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CardView.h"
@interface NearJobSYouTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *JobTitle;

@property (strong, nonatomic) IBOutlet UILabel *jobLocation;

@property (strong, nonatomic) IBOutlet UILabel *Distance;

@property (strong, nonatomic) IBOutlet UILabel *PricePDay;

@property (strong, nonatomic) IBOutlet UIImageView *ProfileImage;

@property (strong, nonatomic) IBOutlet UILabel *UserName;

@property (strong, nonatomic) IBOutlet UILabel *PostDate;

@property (strong, nonatomic) IBOutlet CardView *CardViewOutlet;

@end
