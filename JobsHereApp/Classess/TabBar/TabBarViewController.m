//
//  TabBarViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "TabBarViewController.h"

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
}

@end
