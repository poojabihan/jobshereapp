//
//  ViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+Hexadecimal.h"
//#import <SVProgressHUD.h>
#import "Reachability.h"
#import "Webservice.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
//#import <MBProgressHUD.h>
#import "DGActivityIndicatorView.h"
@import FirebaseInstanceID;
@import Firebase;
@import FirebaseMessaging;

@interface ViewController ()<UITextFieldDelegate,UIScrollViewDelegate>
{
    NSUserDefaults *Store;
  //  MBProgressHUD * HUD;
    DGActivityIndicatorView *loader;

}
@property (strong, nonatomic) IBOutlet UIButton *btnNotAMemberOutlet;


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"LinkedInConnect"];
    [[NSUserDefaults standardUserDefaults] setValue:@"No" forKey:@"FBConnect"];

    self.PasswordView.layer.borderWidth = 1;
    self.PasswordView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.EmailView.layer.borderWidth = 1;
    self.EmailView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.EmailSmallView.layer.borderWidth = 3;
    self.EmailSmallView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.PasswordSmallView.layer.borderWidth = 3;
    self.PasswordSmallView.layer.borderColor = [UIColor whiteColor].CGColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    
    UILabel *firstLineTestButton = [[UILabel alloc] initWithFrame:CGRectMake(35, 0, 120, 20)];
    firstLineTestButton.text = @"Not a member? ";
    
    firstLineTestButton.font = [UIFont fontWithName:@"Helvetica-light" size:16];
    [_btnNotAMemberOutlet addSubview:firstLineTestButton];
    
UILabel *secondLineTestButton = [[UILabel alloc] initWithFrame:CGRectMake(148, 0, 170, 20)];
    secondLineTestButton.text = @"Create a free account!";
    secondLineTestButton.font = [UIFont fontWithName:@"Helvetica-Medium" size:16];
    
    [_btnNotAMemberOutlet addSubview:secondLineTestButton];
    
    
}

#pragma mark  Hide navigation Bar

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
}

#pragma mark  Keyboard Delegates

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark  UITextfield Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        
        [nextResponder becomeFirstResponder];
        
    }else{
        
        [textField resignFirstResponder];
        
    }
}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}

#pragma mark  Login Button

- (IBAction)LoginButtonInstance:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
    
        if((([self.EmailTextfield.text isEqualToString:@""])) && ([self.PasswordTextField.text isEqualToString:@""])){
            
            [self validationErrorAlert:@"Please Fill all the details."];
            
        }
        else if(![self NSStringIsValidEmail:self.EmailTextfield.text]){
            
            [self validationErrorAlert:@"Invalid e-mail address!"];
            
        }else if ([self.PasswordTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Passwords do not match."];
            
        }else{
            
           // [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
            //[SVProgressHUD show];
            //[SVProgressHUD showWithStatus:@"Loading"];
            
            loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
            loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
            
            [self.view addSubview:loader];
            [loader startAnimating];
            NSString *FCMToken = [[FIRInstanceID instanceID]token];
            
            NSDictionary *dictParam = @{@"email":self.EmailTextfield.text,@"password":self.PasswordTextField.text,@"deviceToken":FCMToken};
            
            NSString *Login=[NSString stringWithFormat:@"%@api/userLogin",KBaseUrl];
            
            [Webservice requestPostUrl:Login parameters:dictParam success:^(NSDictionary *response){
                
                NSLog(@"Response %@",response);
                
                if (([[response objectForKey:@"status"]boolValue] == 1)) {
                 
                    [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"record"] valueForKey:@"email"] forKey:@"EmailID"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"record"] valueForKey:@"fname"] forKey:@"FName"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"record"] valueForKey:@"lname"] forKey:@"LName"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"record"] valueForKey:@"userId"] forKey:@"UserID"];
                    [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"record"] valueForKey:@"isVerified"] forKey:@"isVerified"];

                    [[NSUserDefaults standardUserDefaults]setValue:@"LoginSuccess" forKey:@"Login"];
                    [[NSUserDefaults standardUserDefaults]setValue:[[response valueForKey:@"record"]valueForKey:@"profileImage"] forKey:@"ProfileImage"];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        //[SVProgressHUD dismiss ];
                        [loader stopAnimating];

                    });
                    
                    UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
                    self.navigationController.navigationBarHidden=YES;
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.navigationController pushViewController:obj animated:YES];
                    });
                    
                }else if (response==NULL) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                       // [SVProgressHUD dismiss];
                        [loader stopAnimating];

                    });
                    
                    [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                    NSLog(@"response is null");
                    
                }else{
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        // [SVProgressHUD dismiss];
                        [loader stopAnimating];

                    });
                   [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                    
                }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[SVProgressHUD dismiss];
                    [loader stopAnimating];

                });
                NSLog(@"Error %@",error);
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

            }];
            
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    
}

#pragma mark  Forgot Password

- (IBAction)ForgotPasswordButton:(id)sender {
    
    UIAlertController * alertController = [UIAlertController alertControllerWithTitle:@"myOddJobs" message: @"Enter Your Email Address"preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder = @"Enter your E-mail";
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        [textField setKeyboardType:UIKeyboardTypeEmailAddress];
        //textField.borderStyle = UITextBorderStyleRoundedRect;
    }];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Done" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
        
    if (internetStatus != NotReachable){
        
        NSArray * textfields = alertController.textFields;
        UITextField * Email = textfields[0];
        
        if (![self NSStringIsValidEmail:Email.text]) {
            
            [self validationErrorAlert:@"Invalid e-mail address!"];
            
        }else if([Email.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Enter e-mail address!"];
            
        }else{
            
           // [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
           // [SVProgressHUD show];
            
            loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
            loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
            
            [self.view addSubview:loader];
            [loader startAnimating];

            //[SVProgressHUD showWithStatus:@"Loading"];
            
            NSDictionary *dictParam = @{@"email":Email.text};
            
            NSString *Login=[NSString stringWithFormat:@"%@api/forgotPassword",KBaseUrl];
            
            [Webservice requestPostUrl:Login parameters:dictParam success:^(NSDictionary *response){
                
            NSLog(@"Response _%@",response);
                
               if (([[response objectForKey:@"status"]boolValue] == 1)) {
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                      // [SVProgressHUD dismiss];
                       [loader stopAnimating];

                   });
                   [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"Done" actionStyle:UIAlertActionStyleCancel];
                   
               }else if (response==NULL) {
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                     //  [SVProgressHUD dismiss];
                       [loader stopAnimating];

                   });
                   
                   [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                   
                   NSLog(@"response is null");
                   
               }else{
                   
                   dispatch_async(dispatch_get_main_queue(), ^{
                      // [SVProgressHUD dismiss];
                       [loader stopAnimating];

                   });
                   [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
               }
            } failure:^(NSError *error) {
                dispatch_async(dispatch_get_main_queue(), ^{
                   // [SVProgressHUD dismiss];
                    [loader stopAnimating];

                });
                [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

                NSLog(@"Error %@",error);
            }];
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
    }
    }]];
    alertController.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertController animated:YES completion:nil];
    });
}

#pragma mark  Login With Facebook

- (IBAction)LoginWithFacebookButton:(id)sender {
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    [self fetchUserInfo];
}

-(void) facebookConnect
{
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {

         //[HUD hideAnimated:YES];
         [loader stopAnimating];
         if (error) {

             NSLog(@"Process error");
         } else if (result.isCancelled) {

             NSLog(@"Cancelled");
         } else {
             NSLog(@"Logged in %@",result);
//             if ([result.grantedPermissions containsObject:@"email"])
//             {
                 [self fetchUserInfo];
//             }
         }
     }];
}

-(void)fetchUserInfo
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
            // [HUD hideAnimated:YES];
             [loader stopAnimating];

         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             //[HUD hideAnimated:YES];
             [loader stopAnimating];

         } else {
             NSLog(@"Logged in");
             
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name"}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error) {
                      NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
                      [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"FBConnect"];
                      [self callWebServiceForSocialLogin:result];
                  }
                  else{
                    //  [HUD hideAnimated:YES];
                      [loader stopAnimating];

                  }
              }];
         }
     }];
}




#pragma mark  WebService Method
-(void) callWebServiceForSocialLogin:(id)result {
    
    NSString *str = [NSString stringWithFormat:@"%@api/socialLogin",KBaseUrl];
    
    NSString *strProfileImage = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[result valueForKey:@"id"]];

    NSString *FCMToken = [[FIRInstanceID instanceID]token];

    NSDictionary *dictParam = @{@"fname":[result valueForKeyPath:@"name"],@"lname":@"",@"profileImage":strProfileImage,@"socialId":[result valueForKeyPath:@"id"],@"deviceToken":FCMToken,@"email":[result valueForKeyPath:@"email"]};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        [loader stopAnimating];
        //[HUD hideAnimated:YES];
        NSLog(@"Response %@",response);
        
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
           // [HUD hideAnimated:YES];
            [loader stopAnimating];

            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"email"] forKey:@"EmailID"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"fname"] forKey:@"FName"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"lname"] forKey:@"LName"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"userId"] forKey:@"UserID"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"isVerified"] forKey:@"isVerified"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"LoginSuccess" forKey:@"Login"];
            [[NSUserDefaults standardUserDefaults]setValue:[[response valueForKey:@"result"]valueForKey:@"profileImage"] forKey:@"ProfileImage"];
            dispatch_async(dispatch_get_main_queue(), ^{
               // [SVProgressHUD dismiss ];
                [loader stopAnimating];

            });
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            self.navigationController.navigationBarHidden=YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:obj animated:YES];
            });
            
        }else if (response==NULL) {
           // [HUD hideAnimated:YES];
            [loader stopAnimating];

            dispatch_async(dispatch_get_main_queue(), ^{
                [loader stopAnimating];

                //[SVProgressHUD dismiss];
            });
            
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
            NSLog(@"response is null");
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [loader stopAnimating];

                // [SVProgressHUD dismiss];
            });
            [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
            
        }
    } failure:^(NSError *error) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];

        //NSLog(error);
        
    }];
    
}

@end
