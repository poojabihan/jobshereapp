//
//  AppTourViewController.m
//  JobsHereApp
//
//  Created by Apple on 22/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import "AppTourViewController.h"

@interface AppTourViewController (){
    int count;
}

@end

@implementation AppTourViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    count = count++;
    self.btnNext.layer.borderColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0].CGColor;
    self.btnNext.layer.borderWidth = 2;
    
    [_imgAppTour setImage:[UIImage imageNamed:@"Tour1"]];
    
}

-(IBAction)swiftPerformed:(id)sender {
    switch (count) {
        case 1:
            [_imgAppTour setImage:[UIImage imageNamed:@"Tour1"]];
            break;
        case 2:
            [_imgAppTour setImage:[UIImage imageNamed:@"Tour2"]];
            break;
        case 3:
            [_imgAppTour setImage:[UIImage imageNamed:@"Tour3"]];
            break;
        case 4:
            [_imgAppTour setImage:[UIImage imageNamed:@"Tour4"]];
            break;
        default:
            break;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
