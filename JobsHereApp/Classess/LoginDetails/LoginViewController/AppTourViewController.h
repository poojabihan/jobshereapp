//
//  AppTourViewController.h
//  JobsHereApp
//
//  Created by Apple on 22/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppTourViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imgAppTour;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;

@end

NS_ASSUME_NONNULL_END
