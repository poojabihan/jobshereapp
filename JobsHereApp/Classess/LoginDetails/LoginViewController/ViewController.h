//
//  ViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
@interface ViewController : UIViewController

#pragma mark  Outlet

@property (strong, nonatomic) IBOutlet ACFloatingTextField *EmailTextfield;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *PasswordTextField;
@property (strong, nonatomic) IBOutlet UIView *EmailView;
@property (strong, nonatomic) IBOutlet UIView *PasswordView;
@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (strong, nonatomic) IBOutlet UIView *EmailSmallView;
@property (strong, nonatomic) IBOutlet UIView *PasswordSmallView;




#pragma mark  Action

- (IBAction)LoginButtonInstance:(id)sender;
- (IBAction)ForgotPasswordButton:(id)sender;
- (IBAction)LoginWithFacebookButton:(id)sender;

@end

