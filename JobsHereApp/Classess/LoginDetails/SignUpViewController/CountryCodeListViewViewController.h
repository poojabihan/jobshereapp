//
//  CountryCodeListViewViewController.h
//  JobsHereApp
//
//  Created by kdstudio on 09/07/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SignUpViewController.h"

@protocol senddataProtocol <NSObject>

-(void)sendDataToA:(NSArray *)array; //I am thinking my data is NSArray, you can use another object for store your information.

@end


@interface CountryCodeListViewViewController : UIViewController

@property (strong, nonatomic) NSMutableArray *countryList;
@property(nonatomic,assign)id delegate;

@end





