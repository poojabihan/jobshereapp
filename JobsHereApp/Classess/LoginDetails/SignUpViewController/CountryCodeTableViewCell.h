//
//  CountryCodeTableViewCell.h
//  JobsHereApp
//
//  Created by kdstudio on 09/07/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryCodeTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblCountryName;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryCode;

@end
