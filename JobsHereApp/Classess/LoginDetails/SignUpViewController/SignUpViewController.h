//
//  SignUpViewController.h
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ACFloatingTextField.h"
#import "CountryCodeListViewViewController.h"


@interface SignUpViewController : UIViewController 
#pragma mark  Outlet
@property (strong, nonatomic) IBOutlet ACFloatingTextField *FirstNameTextField;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *LastNameTextField;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *EmailTextField;
@property (strong, nonatomic) IBOutlet ACFloatingTextField *ConatctTextField;

@property (strong, nonatomic) IBOutlet ACFloatingTextField *PasswordTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *Con_CodeTextField;
@property (weak, nonatomic) IBOutlet ACFloatingTextField *txtOTP;

@property (strong, nonatomic) IBOutlet UIScrollView *ScrollView;

@property (strong, nonatomic) IBOutlet UIView *FiestNameView;
@property (strong, nonatomic) IBOutlet UIView *LastNameView;
@property (strong, nonatomic) IBOutlet UIView *EmailView;
@property (strong, nonatomic) IBOutlet UIView *ContatctView;
@property (strong, nonatomic) IBOutlet UIView *PasswordView;
@property (weak, nonatomic) IBOutlet UIView *Con_CodeView;
@property (weak, nonatomic) IBOutlet UIView *OTPView;



#pragma mark  Action

- (IBAction)LoginWithFacebook:(id)sender;
- (IBAction)SignUpButtonInstance:(id)sender;

@end
