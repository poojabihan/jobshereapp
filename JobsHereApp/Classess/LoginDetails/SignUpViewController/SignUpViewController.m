//
//  SignUpViewController.m
//  JobsHereApp
//
//  Created by Alok Mishra on 17/01/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "SignUpViewController.h"
#import "UIColor+Hexadecimal.h"
#import <LGAlertView.h>
#import "TabBarViewController.h"
#import "Reachability.h"
#import "Webservice.h"
//#import <SVProgressHUD.h>
//#import <MBProgressHUD.h>
#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "CountryCodeListViewViewController.h"
#import "DGActivityIndicatorView.h"

@import FirebaseInstanceID;

@interface SignUpViewController ()<UITextFieldDelegate,UIScrollViewDelegate,LGAlertViewDelegate>
{
    //MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

    NSUserDefaults *Store;
    NSTimer *timer;
    int currMinute;
    int currSeconds;
    
}
@property (weak, nonatomic) IBOutlet UIButton *btnGenrateOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnTimer;
@end

@implementation SignUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:self.navigationItem.backBarButtonItem.style target:nil action:nil];
    
    self.FiestNameView.layer.borderWidth = 1;
    self.FiestNameView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.LastNameView.layer.borderWidth = 1;
    self.LastNameView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.EmailView.layer.borderWidth = 1;
    self.EmailView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.ContatctView.layer.borderWidth = 1;
    self.ContatctView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.PasswordView.layer.borderWidth = 1;
    self.PasswordView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    self.Con_CodeView.layer.borderWidth = 1;
    self.Con_CodeView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    self.OTPView.layer.borderWidth = 1;
    self.OTPView.layer.borderColor = [UIColor colorWithHex:@"00b050"].CGColor;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWasShown:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

#pragma mark  Show navigation Bar

-(void)viewWillAppear:(BOOL)animated{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    [_btnGenrateOTP setTitle:@"Generate OTP" forState:normal];
    
}


#pragma mark  Keyboard Delegates

-(void)dealloc{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardDidShowNotification
                                                  object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
}
- (void)keyboardWasShown:(NSNotification*)notification
{
    NSDictionary* info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(self.view.frame.origin.x,self.view.frame.origin.y, kbSize.height, 0);
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
    
}

- (void)keyboardWillBeHidden:(NSNotification*)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    self.ScrollView.contentInset = contentInsets;
    self.ScrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark  UITextfield Delegates

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    NSInteger nextTag = textField.tag + 1;
    [self jumpToNextTextField:textField withTag:nextTag];
    
    return NO;
}

- (void)jumpToNextTextField:(UITextField *)textField withTag:(NSInteger)tag{
    
    UIResponder *nextResponder = [self.view viewWithTag:tag];
    
    if ([nextResponder isKindOfClass:[UITextField class]]) {
        
        [nextResponder becomeFirstResponder];
        
    }else{
        
        [textField resignFirstResponder];
        
    }
}

#pragma mark  Email Validation Method

-(BOOL)NSStringIsValidEmail:(NSString *)checkString{
    
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
    
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName goBack:(BOOL)goBack{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    if(goBack){
        UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:^(UIAlertAction * _Nonnull action) {
            [self btnActionGOBack:self];
        }];
        [alertCont addAction:okAction];
        alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

        dispatch_async(dispatch_get_main_queue(), ^{
            [self presentViewController:alertCont animated:true completion:nil];
        });
    }
    else{
        {
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
            [alertCont addAction:okAction];
            alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

            dispatch_async(dispatch_get_main_queue(), ^{
                [self presentViewController:alertCont animated:true completion:nil];
                
            });
        }
    }
    
}

#pragma mark  Login With Facebbok

- (IBAction)LoginWithFacebook:(id)sender {
//    HUD = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];
    
    
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    [self fetchUserInfo];

}

-(void)fetchUserInfo
{
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login
     logInWithReadPermissions: @[@"public_profile", @"email", @"user_friends"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
           //  [HUD hideAnimated:YES];
             [loader stopAnimating];
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
            // [HUD hideAnimated:YES];
             [loader stopAnimating];

         } else {
             NSLog(@"Logged in");
             
             [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"email,name"}]
              startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                  if (!error) {
                      NSLog(@"fetched user:%@  and Email : %@", result,result[@"email"]);
                      [self callWebServiceForSocialLogin:result];
                  }
                  else{
                     // [HUD hideAnimated:YES];
                      [loader stopAnimating];

                  }
              }];
         }
     }];
}
- (IBAction)btnGenerateAction:(id)sender {
    
    if ([self.Con_CodeTextField.text isEqualToString:@""]){
        
        [self validationErrorAlert:@"Enter country code"];
        
    }else if ([self.ConatctTextField.text isEqualToString:@""]){
        
        [self validationErrorAlert:@"Enter Contact Number!"];
        
    }
    else if ([self.ConatctTextField.text length] != 10){
        
        [self validationErrorAlert:@"Please enter valid 10 digit Contact Number"];
        
    }
    else{
    
    [self callWebServiceForGenrateOTP];
    }
}


-(void)timerFired
{
    if((currMinute>0 || currSeconds>=0) && currMinute>=0)
    {
        if(currSeconds==0)
        {
            currMinute-=1;
            currSeconds=59;
        }
        else if(currSeconds>0)
        {
            currSeconds-=1;
        }
        if(currMinute>-1)
            
            [_btnTimer setTitle:[NSString stringWithFormat:@"%d%@%02d", currMinute,@":",currSeconds] forState: normal];
           
    }
    else
    {
        [timer invalidate];
        _btnGenrateOTP.userInteractionEnabled = true;
        [_btnGenrateOTP setTitle:@"Resend OTP" forState:normal];
    }
}

/*- (void)timer {
    totalSeconds-- ;
    
    _btnTimer.titleLabel.text = [NSString stringWithFormat:@"%i", totalSeconds];
    if ( totalSeconds == 0 ) {
        [timer invalidate];
    }
}*/

#pragma mark  WebService Method

-(void) callWebServiceForGenrateOTP{
    
    NSString *str = [NSString stringWithFormat:@"%@api/generateOtp",KBaseUrl];
    
    NSString *contact = @"";
    NSString *contactNoFirstString = @"";
    contactNoFirstString = [self.ConatctTextField.text substringToIndex:1];
    if ([contactNoFirstString isEqualToString:@"0"]) {
        contact = [self.ConatctTextField.text substringFromIndex:1];
    }
    else {
        contact = self.ConatctTextField.text;
    }
    
    NSDictionary *dictParam = @{ @"mobile": contact, @"code" : self.Con_CodeTextField.text };
    
    NSLog(@"strDict %@",dictParam);
    
   // HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        NSLog(@"response+++ %@",response);
        
        if (([[response objectForKey:@"status"] boolValue] == 1)) {
            [loader stopAnimating];

            /*{
                "status": true,
                "message": "success"
            }*/
           
            [_btnTimer setTitle:@"2:00" forState:normal];
            currMinute=2;
            currSeconds=00;
            
            timer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                     target:self
                                                   selector:@selector(timerFired)
                                                   userInfo:nil
                                                    repeats:YES];
            
            _btnGenrateOTP.userInteractionEnabled = false;
        

            NSString *successfullySent = [response objectForKey:@"message"];
            if ( [successfullySent isEqualToString:@"success"]){
                [self validationErrorAlert:@"OTP has been sent successfully!"];

               // [self alertWithTitle:@"OTP Sent" message:@"OTP has been sent successfully!" actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
                
            }
        }
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        //[HUD hideAnimated:YES ];
        [loader stopAnimating];
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
        
    }];
    
}

-(void) callWebServiceForSocialLogin:(id)result {
    
    NSString *str = [NSString stringWithFormat:@"%@api/socialLogin",KBaseUrl];
    
    NSString *strProfileImage = [NSString stringWithFormat:@"http://graph.facebook.com/%@/picture?type=large",[result valueForKey:@"id"]];
    
    NSString *FCMToken = [[FIRInstanceID instanceID]token];
    
    NSDictionary *dictParam = @{@"fname":[result valueForKeyPath:@"name"],@"lname":@"",@"profileImage":strProfileImage,@"socialId":[result valueForKeyPath:@"id"],@"deviceToken":FCMToken,@"email":[result valueForKeyPath:@"email"]};
    
    [Webservice requestPostUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        [loader stopAnimating];

        //[HUD hideAnimated:YES];
        NSLog(@"Response %@",response);
        
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
          //  [HUD hideAnimated:YES];
           
            [loader stopAnimating];
            
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"email"] forKey:@"EmailID"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"fname"] forKey:@"FName"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"lname"] forKey:@"LName"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"userId"] forKey:@"UserID"];
            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"isVerified"] forKey:@"isVerified"];
            
            [[NSUserDefaults standardUserDefaults]setValue:@"LoginSuccess" forKey:@"Login"];
            [[NSUserDefaults standardUserDefaults]setValue:[[response valueForKey:@"result"]valueForKey:@"profileImage"] forKey:@"ProfileImage"];
            dispatch_async(dispatch_get_main_queue(), ^{
               // [SVProgressHUD dismiss ];
                [loader stopAnimating];

            });
            
            UIStoryboard * storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            UITabBarController *obj=[storyboard instantiateViewControllerWithIdentifier:@"TabBarViewController"];
            self.navigationController.navigationBarHidden=YES;
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:obj animated:YES];
            });
            
        }else if (response==NULL) {
         //   [HUD hideAnimated:YES];
            [loader stopAnimating];

            dispatch_async(dispatch_get_main_queue(), ^{
              //  [SVProgressHUD dismiss];
                [loader stopAnimating];

            });
            
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
            
            NSLog(@"response is null");
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
              //  [SVProgressHUD dismiss];
                [loader stopAnimating];

            });
            
            if ([[response valueForKey:@"message"] isEqualToString:@"User already exists"]) {
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:YES];
            }
            else{
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
            }

            
        }
    } failure:^(NSError *error) {
        
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];

        //NSLog(error);
        
    }];
    
}




#pragma mark  SignUp button

- (IBAction)btnActionGOBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnActionGO:(id)sender {
    
}

    
    - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
    {
        // Make sure your segue name in storyboard is the same as this line
        if ([[segue identifier] isEqualToString:@"ShowCodeList"])
        {
            // Get reference to the destination view controller
            CountryCodeListViewViewController *vc = [segue destinationViewController];
            
            vc.delegate = self;
            
            vc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
            
            
            [self.navigationController presentViewController:vc animated:NO completion:nil];
            
            
        }
    }

   

-(void)sendDataToA:(NSArray *)array
{
    NSLog(@"##############%@", array);
    
    self.Con_CodeTextField.text = [NSString stringWithFormat:@"%@", array] ;

    // data will come here inside of ViewControllerA
}

- (IBAction)SignUpButtonInstance:(id)sender {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reachability currentReachabilityStatus];
    
    if (internetStatus != NotReachable){
        
        if ([self.FirstNameTextField.text isEqualToString:@""] && [self.LastNameTextField.text isEqualToString:@""]  && [self.EmailTextField.text isEqualToString:@""]  && [self.ConatctTextField.text isEqualToString:@""] && [self.PasswordTextField.text isEqualToString:@""]) {
            
            [self validationErrorAlert:@"Please, Fill all the details."];
            
        }else if([self.FirstNameTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter first name."];
            
        }else if ([self.LastNameTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter email."];
            
        }else if(![self NSStringIsValidEmail:self.EmailTextField.text]){
            
            [self validationErrorAlert:@"Please enter valid email."];
            
        }else if ([self.Con_CodeTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please select country code."];
            
        }else if ([self.ConatctTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter mobile number"];
            
        }else if ([self.PasswordTextField.text isEqualToString:@""]){
            
            [self validationErrorAlert:@"Please enter password."];
            
        }
        else if ([self.ConatctTextField.text length] != 10){
            
            [self validationErrorAlert:@"Please enter valid 10 digit Contact Number"];
            
        }else{
     
            
            UIScrollView *scrollView = [UIScrollView new];
            
            UILabel *label = [UILabel new];
            label.numberOfLines = 0;
            label.text = @"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce bibendum sagittis magna, at pulvinar leo. Vestibulum eu ex ut ligula mollis tempus in sit amet lacus. Nullam fermentum tortor sit amet arcu ornare, a fringilla arcu vulputate. Pellentesque accumsan imperdiet viverra. Praesent et bibendum turpis. Morbi condimentum risus non neque vehicula, a laoreet neque lacinia. Nullam et lorem non magna pharetra ullamcorper. Duis malesuada sem quis venenatis pulvinar. Pellentesque consectetur dolor non elit pretium laoreet. Praesent condimentum tristique sapien, non lacinia erat ullamcorper eu. Pellentesque turpis nisl, mollis id arcu eget, commodo commodo massa. Duis pretium et libero sed fringilla. Donec a nunc sem. Phasellus eget est eget quam commodo egestas.\n\nNam rhoncus vehicula interdum. Praesent urna lorem, iaculis id sem eu, varius gravida sapien. Sed et purus bibendum ipsum feugiat placerat. Donec iaculis urna nisl, vel condimentum elit auctor quis. Phasellus mollis vehicula facilisis. Vivamus commodo arcu sed justo consectetur interdum. Suspendisse pharetra malesuada sem eleifend bibendum. Suspendisse quis tempus lorem. Cras viverra congue nisi, sed molestie sem. Etiam accumsan tincidunt arcu, quis feugiat mi lacinia quis. Curabitur sit amet tincidunt nisl.\n\nProin sit amet tempor augue. Proin eget arcu quis ipsum semper condimentum nec ac purus.";
            [scrollView addSubview:label];
            
            LGAlertView *alertView = [[LGAlertView alloc] initWithViewAndTitle:@"myOddJobs"
                                                                       message:@"Term And Condition"
                                                                         style:LGAlertViewStyleAlert
                                                                          view:scrollView
                                                                  buttonTitles:@[@"Agree"]
                                                             cancelButtonTitle:@"Cancel"
                                                        destructiveButtonTitle:nil
                                                                      delegate:self];
            alertView.messageTextColor=[UIColor redColor];
            alertView.titleTextColor=[UIColor greenColor];
            alertView.buttonsTitleColor=[UIColor redColor];
            scrollView.frame = CGRectMake(0.0, 0.0, alertView.width, 250.0);
            label.textColor = [UIColor blackColor];
            label.font = alertView.messageFont;
            CGSize labelSize = [label sizeThatFits:CGSizeMake(alertView.width - 15.0, CGFLOAT_MAX)];
            label.frame = CGRectMake(8.0, 0.0, labelSize.width, labelSize.height);
            scrollView.contentSize = CGSizeMake(alertView.width, labelSize.height);
            [alertView showAnimated:YES completionHandler:nil];
            
        }
    }else{
        [self alertWithTitle:kAppNameAlert message:kInternetOff actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
    }
}


#pragma mark - LGAlertViewDelegate

- (void)alertView:(LGAlertView *)alertView clickedButtonAtIndex:(NSUInteger)index title:(nullable NSString *)title {
    NSLog(@"action {title: %@, index: %lu}", title, (long unsigned)index);
  
//    HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//    HUD.bezelView.color = [UIColor blackColor];
//    HUD.bezelView.style = MBProgressHUDModeIndeterminate;
//    HUD.contentColor = [UIColor whiteColor];

    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    NSDictionary *dictParam = @{@"fname":self.FirstNameTextField.text,
                                @"lname":self.LastNameTextField.text,
                                @"email":self.EmailTextField.text,
                                @"contact":self.ConatctTextField.text,
                                @"password":self.PasswordTextField.text,
                                @"code":_Con_CodeTextField.text,
                                @"otp":_txtOTP.text,
                                @"deviceToken":@""};
    
    NSString *SignUP=[NSString stringWithFormat:@"%@api/userRegister",KBaseUrl];
    
    [Webservice requestPostUrl:SignUP parameters:dictParam success:^(NSDictionary *response){
        
        NSLog(@"Response %@",response);
    
        if (([[response objectForKey:@"status"]boolValue] == 1)) {
            
//            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"email"] forKey:@"EmailID"];
//            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"fname"] forKey:@"FName"];
//            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"lname"] forKey:@"LName"];
//            [[NSUserDefaults standardUserDefaults] setValue:[[response objectForKey:@"result"] valueForKey:@"userId"] forKey:@"UserID"];
//            [[NSUserDefaults standardUserDefaults]setValue:@"LoginSuccess" forKey:@"Login"];
//
           // dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES ];
            
            [loader stopAnimating];
            //});
            
            ViewController *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.navigationController pushViewController:VC animated:YES];
            });
            
        }else if (response==NULL) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[HUD hideAnimated:YES];
                [loader stopAnimating];

            });
            
            [self alertWithTitle:@"Something went wrong!" message:@"Please try again later." actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
            
            NSLog(@"response is null");
            
        }else{
            
            dispatch_async(dispatch_get_main_queue(), ^{
               // [HUD hideAnimated:YES];
                [loader stopAnimating];

            });
            
            if ([[response valueForKey:@"message"] isEqualToString:@"User already exists"]) {
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:YES];
            }
            else{
                [self alertWithTitle:kAppNameAlert message:[response valueForKey:@"message"] actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];
            }
            
        }
    } failure:^(NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            //[HUD hideAnimated:YES];
            [loader stopAnimating];
 });
        NSLog(@"Error %@",error);
        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel goBack:NO];

    }];
    
}
@end
