//
//  CountryCodeListViewViewController.m
//  JobsHereApp
//
//  Created by kdstudio on 09/07/18.
//  Copyright © 2018 Alok Mishra. All rights reserved.
//

#import "CountryCodeListViewViewController.h"
#import "CountryCodeTableViewCell.h"
#import "Reachability.h"
#import "Webservice.h"
//#import <SVProgressHUD.h>
//#import <MBProgressHUD.h>
#import "SignUpViewController.h"
#import "DGActivityIndicatorView.h"

@interface CountryCodeListViewViewController ()
{
   // MBProgressHUD *HUD;
    DGActivityIndicatorView *loader;

}
@property (weak, nonatomic) IBOutlet UITableView *tblView;


@end

@implementation CountryCodeListViewViewController
@synthesize delegate;

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    [self callWebServiceForCountryCodeList];
    // Do any additional setup after loading the view.
}



#pragma mark  TableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return _countryList.count;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath//populates each cell
{
    CountryCodeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CountryCodeTableViewCell"];
    
    
   

    cell.lblCountryName.text = [NSString stringWithFormat:@"%@",[[_countryList valueForKey:@"name"]objectAtIndex:indexPath.row]];
    
    cell.lblCountryCode.text = [NSString stringWithFormat:@"%@",[[_countryList valueForKey:@"phonecode"]objectAtIndex:indexPath.row]];
    
    return cell;
}


#pragma mark  TableView Height

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    [delegate sendDataToA:[[_countryList valueForKey:@"phonecode"]objectAtIndex:indexPath.row]];

    
//[self.delegate didFinishWithInputView: [[_countryList valueForKey:@"phonecode"]objectAtIndex:indexPath.row]];
    
    [self dismissViewControllerAnimated:NO completion:nil];
}

    

#pragma mark  ButtonAction

- (IBAction)dismissView:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark  Web Service
-(void) callWebServiceForCountryCodeList{
    
    NSString *str = [NSString stringWithFormat:@"%@/api/countryCode",KBaseUrl];
    
     NSDictionary *dictParam = @{ };
    //HUD = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    loader = [[DGActivityIndicatorView alloc] initWithType:DGActivityIndicatorAnimationTypeBallPulse tintColor:[UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0] size:50.0f];
    loader.frame = CGRectMake(self.view.frame.size.width/2 - 50.0, self.view.frame.size.height/2 - 50.0, 110.0f, 110.0f);
    
    [self.view addSubview:loader];
    [loader startAnimating];
    
    
    [Webservice requestGetUrl:str parameters:dictParam success:^(NSDictionary *response) {
        
        NSLog(@"response+++ %@",response);
        
        if (([[response objectForKey:@"status"] boolValue] == 1)) {
            //[HUD hideAnimated:YES];
            [loader stopAnimating];
           _countryList  =[response objectForKey:@"result"];
            NSLog(@" HELLO COUNTRY LIST %@",_countryList);
            
            [_tblView reloadData];
            
        }
        
    } failure:^(NSError *error) {
        
        //NSLog(error);
        //[HUD hideAnimated:YES];
        [loader stopAnimating];

        [self alertWithTitle:@"Something went wrong!" message:error.localizedDescription actionTitle:@"OK" actionStyle:UIAlertActionStyleCancel];
        
    }];
    
}

#pragma mark  Alert Prompt Method

-(void)validationErrorAlert:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    alert.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 1; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:nil];
    });
    
}

-(void)alertWithTitle:(NSString *)titleName message:(NSString *)messageName actionTitle:(NSString *)actionName actionStyle:(UIAlertActionStyle)actionStyleName{
    
    UIAlertController *alertCont = [UIAlertController alertControllerWithTitle:titleName message:messageName preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:actionName style:actionStyleName handler:nil];
    [alertCont addAction:okAction];
    alertCont.view.tintColor = [UIColor colorWithRed: 0.0/ 255.0 green: 176.0 / 255.0 blue: 80.0 / 255.0 alpha:1.0];

    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:alertCont animated:true completion:nil];
    });
    
}


@end
