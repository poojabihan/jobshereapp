//
//  JobActionPopUpViewController.m
//  JobsHereApp
//
//  Created by Apple on 28/01/19.
//  Copyright © 2019 Alok Mishra. All rights reserved.
//

#import "JobActionPopUpViewController.h"
#import "JobDetailsViewController.h"
@interface JobActionPopUpViewController ()

@end

@implementation JobActionPopUpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.lblLocation.text = self.strLocation;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCancelAction:(id)sender {
    
    [self dismissViewControllerAnimated:NO completion:nil];

}

- (IBAction)btnPosJobsAction:(id)sender {
    
    JobDetailsViewController *JobVC = [self.storyboard instantiateViewControllerWithIdentifier:@"JobDetailsViewController"];
    JobVC.latJobView = _latJobView;
    JobVC.locationJobView = self.strLocation;
    JobVC.longiJobView = _longiJobView;
    [_nav pushViewController:JobVC animated:YES];
    [self.view.window.rootViewController dismissViewControllerAnimated:NO completion:nil];
    [self dismissViewControllerAnimated:NO completion:nil];
    
   // [self.navigationController pushViewController:JobVC animated:YES];
}


@end
